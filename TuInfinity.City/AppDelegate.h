//
//  AppDelegate.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
