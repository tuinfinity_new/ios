//
//  AppDelegate.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "RFRateMe.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;

    [RFRateMe showRateAlertAfterTimesOpened:5];
    [RFRateMe showRateAlertAfterDays:7];
    
    DatosAplicacionSingleton *datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    datosAplicacion.locationManager.delegate = self;
    datosAplicacion.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ( IS_OS_8_OR_LATER )
        [datosAplicacion.locationManager requestAlwaysAuthorization];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    if ( [userDefaults objectForKey:@"tutorialActivo"] == nil )
    {
        [userDefaults setValue:[NSNumber numberWithBool:YES] forKeyPath:@"tutorialActivo"];
        [userDefaults synchronize];
    }
    
    if ( [userDefaults objectForKey:@"gelocalizacionActiva"] == nil )
    {
        [userDefaults setValue:[NSNumber numberWithBool:YES] forKeyPath:@"gelocalizacionActiva"];
        [userDefaults synchronize];
        
        [datosAplicacion.locationManager startUpdatingLocation];
        datosAplicacion.geolocalizacionActiva = YES;
    }
    else
    {
        datosAplicacion.geolocalizacionActiva = [[userDefaults objectForKey:@"gelocalizacionActiva"] boolValue];
        if ( datosAplicacion.geolocalizacionActiva )
            [datosAplicacion.locationManager startUpdatingLocation];
    }

    NSDictionary *attributedStringSettings = @{ NSFontAttributeName:[UIFont fontWithName:@"Asenine" size:30],
                                                NSForegroundColorAttributeName:COLOR_BLANCO };
    [[UINavigationBar appearance] setTitleTextAttributes:attributedStringSettings];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    
    if ( [[userDefaults objectForKey:@"usrInvitado"] boolValue] )
        [userDefaults removePersistentDomainForName:appDomain];
    else if ( ![[userDefaults objectForKey:@"usrRecordarDatos"] boolValue] )
        [userDefaults removePersistentDomainForName:appDomain];
}

#pragma mark -
#pragma mark - CLLocationManagerDelegate

- (IBAction)locationManagerTimeout:(id)sender
{
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    DatosAplicacionSingleton *datosAplicacion = [DatosAplicacionSingleton sharedSingleton];

    if ( [[userDefaults objectForKey:@"gelocalizacionActiva"] boolValue] )
    {
        datosAplicacion.location = [locations lastObject];
        [datosAplicacion updateLocation];

        [userDefaults setValue:[NSNumber numberWithDouble:datosAplicacion.location.coordinate.latitude] forKeyPath:@"locLatitud"];
        [userDefaults setValue:[NSNumber numberWithDouble:datosAplicacion.location.coordinate.longitude] forKeyPath:@"locLongitud"];
        [userDefaults synchronize];

        if ( [userDefaults objectForKey:@"usrIdUsuario"] )
        {
            CLLocationDistance distance = [datosAplicacion.location distanceFromLocation:datosAplicacion.lastLocation];
            
            NSDateComponents *dateDifference = [[NSCalendar currentCalendar] components:NSSecondCalendarUnit fromDate:datosAplicacion.lastUpdateLocation toDate:[[NSDate alloc] init] options:0];
            
            if ( ( dateDifference.second > 10 ) && ( fabs(distance) > 30.0 ) )
            {
                datosAplicacion.lastLocation = datosAplicacion.location;
                [datosAplicacion updateLocation];
                
                NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/usuario/actualizarLocalizacion", URL_SERVIDOR_HTTPS]];
                
                AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
                [operationManager.securityPolicy setAllowInvalidCertificates:YES];
                [operationManager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
                [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
                [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
                
                [operationManager PUT:[NSString stringWithFormat:@"%f/%f", datosAplicacion.location.coordinate.latitude, datosAplicacion.location.coordinate.longitude]
                           parameters:nil
                              success:^(AFHTTPRequestOperation *operation, id responseObject){
                              }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error){
                              }];
            }
        }
    }
}

@end