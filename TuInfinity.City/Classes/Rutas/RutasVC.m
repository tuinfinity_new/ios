//
//  RutasVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 10/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "BTSimpleSideMenu.h"
#import "HMSegmentedControl.h"

#import "Globales.h"

#import "Rutas_RutasListadoVC.h"
#import "Rutas_MisRutasListadoVC.h"
#import "Rutas_RutasGuardadasListadoVC.h"

#import "RutasVC.h"

@interface RutasVC ()

@property (nonatomic) BTSimpleSideMenu *sideMenu;
@property (strong, nonatomic) IBOutlet HMSegmentedControl *segmentedControl;

@property (nonatomic, weak) IBOutlet UIView *viewMenu;
@property (nonatomic, weak) IBOutlet UIScrollView *viewContainer;

@property UIViewController *currentDetailViewController;

@property (nonatomic, strong) IBOutlet Rutas_RutasListadoVC *rutas_RutasListadoVC;
@property (nonatomic, strong) IBOutlet Rutas_MisRutasListadoVC *rutas_MisRutasListadoVC;
@property (nonatomic, strong) IBOutlet Rutas_RutasGuardadasListadoVC *rutas_RutasGuardadasListadoVC;

@end

@implementation RutasVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ( [[userDefaults objectForKey:@"tutorialActivo"] boolValue] )
    {
    }

    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    _rutas_RutasListadoVC = [storyboard instantiateViewControllerWithIdentifier:@"Rutas_RutasListado"];
    _rutas_MisRutasListadoVC = [storyboard instantiateViewControllerWithIdentifier:@"Rutas_MisRutasListado"];
    _rutas_RutasGuardadasListadoVC = [storyboard instantiateViewControllerWithIdentifier:@"Rutas_RutasGuardadasListado"];

    [_rutas_RutasListadoVC setOriginView:self];
    
    [self configuraNavigationBar:1];
    //[self configuraMenu];
    [self configuraMenuLateral];
    
//    [_segmentedControl setSelectedSegmentIndex:0 animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = COLOR_AZUL1;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar:(int)typeRightButtonItem
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *btnR1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_MenuPrincipal"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnMenuPrincipal_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    switch ( typeRightButtonItem )
    {
        case 0:
            break;
        case 1:
            [btnR1 setFrame:CGRectMake(0, 0, 22, 22)];
            [btnR1 setImage:[UIImage imageNamed:@"Btn_Filtrar"] forState:UIControlStateNormal];
            [btnR1 addTarget:self action:@selector(btnFiltrar_Pressed:) forControlEvents:UIControlEventTouchUpInside];
            break;
        case 2:
            [btnR1 setFrame:CGRectMake(0, 0, 22, 22)];
            [btnR1 setImage:[UIImage imageNamed:@"Btn_Mas"] forState:UIControlStateNormal];
            [btnR1 addTarget:self action:@selector(btnMas_Pressed:) forControlEvents:UIControlEventTouchUpInside];
            break;
    }
    btnL1.selected = NO;
    btnR1.selected = NO;
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    switch ( typeRightButtonItem )
    {
        case 0:
            self.navigationItem.rightBarButtonItems = nil;
            break;
        default:
            self.navigationItem.rightBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnR1], nil];
            break;
    }
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - Misc

- (void)configuraMenu
{
    NSArray *menuTitulos = [NSArray arrayWithObjects:[NSLocalizedString(@"MENU_OPC_2_1", @"") uppercaseString], [NSLocalizedString(@"MENU_OPC_2_2", @"") uppercaseString], [NSLocalizedString(@"MENU_OPC_2_3", @"") uppercaseString], nil];
    NSArray *menuImagenes1 = [NSArray arrayWithObjects:[UIImage imageNamed:@"Img_Logo2"], [UIImage imageNamed:@"Img_Logo2"], [UIImage imageNamed:@"Img_Logo2"], nil];
    NSArray *menuImagenes2 = [NSArray arrayWithObjects:[UIImage imageNamed:@"Img_Logo2"], [UIImage imageNamed:@"Img_Logo2"], [UIImage imageNamed:@"Img_Logo2"], nil];

    _segmentedControl = [[HMSegmentedControl alloc] initWithSectionImages:menuImagenes1 sectionSelectedImages:menuImagenes2 titlesForSections:menuTitulos];

    _segmentedControl.frame = CGRectMake(0, 0, _viewMenu.frame.size.width, _viewMenu.frame.size.height);
    _segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    _segmentedControl.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10);
    _segmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleDynamic;
    _segmentedControl.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:( DEVICE_IPHONE ) ? 16 : 30];
    _segmentedControl.textColor = COLOR_NEGRO;
    _segmentedControl.selectedTextColor = COLOR_AZUL1;
    _segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    _segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    _segmentedControl.selectionIndicatorColor = COLOR_AZUL1;
    [_segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
    [_segmentedControl setSelectedSegmentIndex:0 animated:YES];
    
    [_viewMenu addSubview:_segmentedControl];
}

- (void)configuraMenuLateral
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;

    BTSimpleMenuItem *menuItem1 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_1", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Cupones"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Cupones"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem2 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_2", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Ofertas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Ofertas"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem3 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_3", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Empresas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Empresas"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem4 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_4", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Rutas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 _viewContainer.userInteractionEnabled = YES;
                                                                 _viewMenu.userInteractionEnabled = YES;
                                                                 [_segmentedControl setSelectedSegmentIndex:0 animated:YES];
                                                             }];
    BTSimpleMenuItem *menuItem5 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_5", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Lugares"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Lugares"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem6 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_6", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Wifi"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Wifi"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem0 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_0", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Ajustes"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Ajustes"] animated:NO];
                                                             }];

    _sideMenu = [[BTSimpleSideMenu alloc] initWithItem:@[menuItem1, menuItem2, menuItem3, menuItem4, menuItem5, menuItem6, menuItem0] addToViewController:self];
}

#pragma mark -
#pragma mark - View Controller

- (void)presentDetailController:(UIViewController*)detailVC
{
    if ( _currentDetailViewController )
        [self removeCurrentDetailViewController];
    
    _currentDetailViewController = detailVC;
    _currentDetailViewController.view.frame = _viewContainer.bounds;
    [_viewContainer addSubview:_currentDetailViewController.view];
}

- (void)removeCurrentDetailViewController
{
    [_currentDetailViewController.view removeFromSuperview];
    _currentDetailViewController = nil;
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnMenuPrincipal_Pressed:(id)sender
{
    [_sideMenu toggleMenu];
    _viewMenu.userInteractionEnabled = ![_sideMenu isOpen];
    _viewContainer.userInteractionEnabled = ![_sideMenu isOpen];
}

- (IBAction)btnFiltrar_Pressed:(id)sender
{
}

- (IBAction)btnMas_Pressed:(id)sender
{
}

#pragma mark -
#pragma mark - HMSegmentedControl

- (void)segmentedControlChangedValue:(HMSegmentedControl*)segmentedControl
{
    switch ( segmentedControl.selectedSegmentIndex )
    {
        case 0:
            [self configuraNavigationBar:1];
            [self presentDetailController:_rutas_RutasListadoVC];
            break;
        case 1:
            [self configuraNavigationBar:0];
            [self presentDetailController:_rutas_MisRutasListadoVC];
            break;
        case 2:
            [self configuraNavigationBar:2];
            [self presentDetailController:_rutas_RutasGuardadasListadoVC];
            break;
        default:
            break;
    }
}

@end