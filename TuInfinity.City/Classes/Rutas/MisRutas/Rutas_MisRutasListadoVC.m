//
//  Rutas_MisRutasListadoVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"

#import "Rutas_RutasListadoCell.h"

#import "Rutas_MisRutasListadoVC.h"

@interface Rutas_MisRutasListadoVC () <UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *listadoDatos;

@end

@implementation Rutas_MisRutasListadoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _originView.navigationController.delegate = self;
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _listadoDatos = [NSMutableArray array];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [self viewWillAppear:animated];
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ( DEVICE_IPHONE ) ? 100 : 200;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_listadoDatos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Rutas_RutasListadoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellRutas_MisRutasListado"];
    [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
    return cell;
}

@end