//
//  Rutas_RutasGuardadasListadoVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RutasVC.h"

@interface Rutas_RutasGuardadasListadoVC : UIViewController

@property (nonatomic, weak) IBOutlet RutasVC *originView;

@end
