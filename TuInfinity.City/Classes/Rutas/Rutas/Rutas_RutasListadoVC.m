//
//  Rutas_RutasListadoVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"

#import "Rutas_RutasListadoCell.h"

#import "Rutas_RutasListadoVC.h"

@interface Rutas_RutasListadoVC () <UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *listadoDatos1;
@property (nonatomic, strong) NSMutableArray *listadoDatos2;

@end

@implementation Rutas_RutasListadoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _originView.navigationController.delegate = self;
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _listadoDatos1 = [NSMutableArray array];
    _listadoDatos2 = [NSMutableArray array];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [self viewWillAppear:animated];
}

#pragma mark -
#pragma mark - TableView

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return ( DEVICE_IPHONE ) ? 25 : 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lblTitulo = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, tableView.frame.size.width - 10, ( DEVICE_IPHONE ) ? 25 : 50)];

    [lblTitulo setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:( DEVICE_IPHONE ) ? 15 : 30]];
    [lblTitulo setTextColor:COLOR_BLANCO];

    switch ( section )
    {
        case 0: [lblTitulo setText:NSLocalizedString(@"RUTAS_RUTASRECOMENDADAS", @"")]; break;
        case 1: [lblTitulo setText:NSLocalizedString(@"RUTAS_OTRASRUTAS", @"")]; break;
        default: break;
    }

    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = COLOR_GRIS1;
    [headerView addSubview:lblTitulo];
    
    return headerView;
}

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ( indexPath.section )
    {
        case 0: return ( DEVICE_IPHONE ) ? 100 : 200; break;
        case 1: return ( DEVICE_IPHONE ) ? 100 : 200; break;
        default: break;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ( section )
    {
        case 0: return [_listadoDatos1 count]; break;
        case 1: return [_listadoDatos2 count]; break;
        default: break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Rutas_RutasListadoCell *cell;
    
    switch ( indexPath.section )
    {
        case 0: cell = [tableView dequeueReusableCellWithIdentifier:@"CellRutas_RutasListado1"]; break;
        case 1: cell = [tableView dequeueReusableCellWithIdentifier:@"CellRutas_RutasListado2"]; break;
        default: break;
    }
    [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
    return cell;
}

@end
