//
//  UIImage_Blur.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 20/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView (bt_screenshot)

-(UIImage *)screenshotWitWidth:(CGFloat)width andHeight:(CGFloat)height;

@end

@interface UIImage (bt_blurrEffect)

- (UIImage *)blurredImageWithRadius:(CGFloat)radius iterations:(NSUInteger)iterations tintColor:(UIColor *)tintColor;

@end
