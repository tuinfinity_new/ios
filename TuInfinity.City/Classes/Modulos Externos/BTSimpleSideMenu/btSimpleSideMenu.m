//
//  BTSimpleSideMenu.m
//  BTSimpleSideMenuDemo
//  Created by Balram on 29/05/14.
//  Copyright (c) 2014 Balram Tiwari. All rights reserved.
//

//#import <QuartzCore/QuartzCore.h>
//#import <Accelerate/Accelerate.h>

#import "Globales.h"

#import "BTSimpleSideMenu.h"

#define BACKGROUND_COLOR [UIColor colorWithWhite:1 alpha:0.2]
#define GENERIC_IMAGE_FRAME CGRectMake(0, 0, 40, 40)
#define MENU_WIDTH [UIScreen mainScreen].bounds.size.width / 1.3

@implementation BTSimpleSideMenu

#pragma -mark public methods

-(instancetype)initWithItem:(NSArray *)items addToViewController:(id)sender
{
    if ((self = [super init]))
    {
        viewOrigin = sender;

        // perform the other initialization of items.
        [self commonInit:sender];
        itemsArray = items;
    }
    return self;
}

-(instancetype)initWithItemTitles:(NSArray *)itemsTitle addToViewController:(UIViewController *)sender
{
    if ((self = [super init]))
    {
        viewOrigin = sender;
        
        // perform the other initialization of items.
        [self commonInit:sender];
        NSMutableArray *tempArray = [[NSMutableArray alloc]init];
        for(int i = 0;i<[itemsTitle count]; i++)
        {
            BTSimpleMenuItem *temp = [[BTSimpleMenuItem alloc]initWithTitle:[itemsTitle objectAtIndex:i]
                                                                      image:nil
                                                               onCompletion:nil];
            [tempArray addObject:temp];
        }
        itemsArray = tempArray;
    }
    return self;
}

-(instancetype)initWithItemTitles:(NSArray *)itemsTitle andItemImages:(NSArray *)itemsImage addToViewController:(UIViewController *)sender
{
    if ((self = [super init]))
    {
        viewOrigin = sender;

        // perform the other initialization of items.
        [self commonInit:sender];
        NSMutableArray *tempArray = [[NSMutableArray alloc]init];
        for(int i = 0;i<[itemsTitle count]; i++)
        {
            BTSimpleMenuItem *temp = [[BTSimpleMenuItem alloc]initWithTitle:[itemsTitle objectAtIndex:i]
                                                                      image:[itemsImage objectAtIndex:i]
                                                               onCompletion:nil];
            [tempArray addObject:temp];
        }
        itemsArray = tempArray;
    }
    return self;
}

-(void)toggleMenu
{
    if ( !_isOpen )
        [self show];
    else
        [self hide];
}

-(void)show
{
    if ( !_isOpen )
    {
        [self setUserInteractionEnabled:YES];

        backGroundImageMenu1.image = [[viewOrigin.view screenshotWitWidth:MENU_WIDTH andHeight:[UIScreen mainScreen].bounds.size.height] blurredImageWithRadius:5.0f iterations:5 tintColor:nil];
        backGroundImageMenu2.image = [UIImage imageNamed:@"Img_FondoBlanco"];
        
        self.frame = CGRectMake(xAxis, yAxis, width, height);

        [UIView animateWithDuration:0.5 animations:^{
            menuTable.frame = CGRectMake(menuTable.frame.origin.x, menuTable.frame.origin.y+15, width, height);
            menuTable.alpha = 1;
            backGroundImageMenu1.alpha = 1;
            backGroundImageMenu2.alpha = 0.5;
        } completion:^(BOOL finished) {
        }];
        _isOpen = YES;
    }
}

-(void)hide
{
    if ( _isOpen )
    {
        [UIView animateWithDuration:0.5 animations:^{
            menuTable.frame = CGRectMake(-menuTable.frame.origin.x, menuTable.frame.origin.y-15, width, height);
            menuTable.alpha = 0;
            backGroundImageMenu1.frame = CGRectMake(0, 0, width, height);
            backGroundImageMenu2.frame = CGRectMake(0, 0, width, height);
        } completion:^(BOOL finished) {
            self.frame = CGRectMake(-width, yAxis, width, height);
        }];
        _isOpen = NO;
    }
}

#pragma -mark tableView Delegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemsArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //return 60;
    return 55;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate BTSimpleSideMenu:self didSelectItemAtIndex:indexPath.row];
    [self.delegate BTSimpleSideMenu:self selectedItemTitle:[[itemsArray objectAtIndex:indexPath.row] title]];
    _selectedItem = [itemsArray objectAtIndex:indexPath.row];
    [self hide];
    if (_selectedItem.block)
    {
        BOOL success = YES;
        _selectedItem.block(success, _selectedItem);
    }
    [menuTable deselectRowAtIndexPath:indexPath animated:YES];
}

#define IMAGE_VIEW_TAG 1
#define TITLE_LABLE_TAG 2

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"cell";
    UILabel *titleLabel;
    UIImageView *imageView;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    BTSimpleMenuItem *item = [itemsArray objectAtIndex:indexPath.row];
    
    if ( cell == nil )
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //imageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 15, 30, 30)];
        imageView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 30, 30)];
        imageView.tag = IMAGE_VIEW_TAG;
        [cell.contentView addSubview:imageView];

        //titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 0, 130, 60)];
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 0, 130, 50)];
        titleLabel.tag = TITLE_LABLE_TAG;
        titleLabel.textColor = [UIColor colorWithWhite:0.2 alpha:1];
        titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:30];
        [cell.contentView addSubview:titleLabel];
    }
    else
    {
        titleLabel = (UILabel *)[cell.contentView viewWithTag:TITLE_LABLE_TAG];
        imageView = (UIImageView *)[cell.contentView viewWithTag:IMAGE_VIEW_TAG];
    }
    
    titleLabel.text = item.title;
    imageView.image = item.imageView.image;
    return cell;
}

#pragma -mark Private helpers
-(void)commonInit:(UIViewController *)sender
{
    CGRect screenSize = [UIScreen mainScreen].bounds;
    xAxis = 0;
    yAxis = 0;
    height = screenSize.size.height - 64;
    width = MENU_WIDTH;
    
    self.frame = CGRectMake(-width, yAxis, width, height);
    self.backgroundColor = BACKGROUND_COLOR;
    
    menuTable = [[UITableView alloc]initWithFrame:CGRectMake(xAxis, yAxis, width, height) style:UITableViewStylePlain];
    
    screenShotImage = [sender.view screenshotWitWidth:MENU_WIDTH andHeight:[UIScreen mainScreen].bounds.size.height];
    blurredImage = [screenShotImage blurredImageWithRadius:10.0f iterations:5 tintColor:nil];

    backGroundImageMenu1 = [[UIImageView alloc] init];
    backGroundImageMenu1.frame = CGRectMake(0, 0, width, height);
    backGroundImageMenu1.alpha = 0;
    [self addSubview:backGroundImageMenu1];

    backGroundImageMenu2 = [[UIImageView alloc] init];
    backGroundImageMenu2.frame = CGRectMake(0, 0, width, height);
    backGroundImageMenu2.alpha = 0;
    [self addSubview:backGroundImageMenu2];

    [menuTable setBackgroundColor:[UIColor clearColor]];
    [menuTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [menuTable setShowsVerticalScrollIndicator:NO];
    
    menuTable.delegate = self;
    menuTable.dataSource = self;
    menuTable.alpha = 0;
    menuTable.bounces = NO;
    _isOpen = NO;
    [self addSubview:menuTable];
    
    [sender.view addSubview:self];
}

-(UIImage *)reducedImage:(UIImage *)srcImage
{
    UIImage *image = srcImage;
    UIImage *tempImage = nil;
    CGSize targetSize = CGSizeMake(20,20);
    UIGraphicsBeginImageContext(targetSize);
    
    CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
    thumbnailRect.origin = CGPointMake(0.0,0.0);
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [image drawInRect:thumbnailRect];
    
    tempImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return tempImage;
}

@end