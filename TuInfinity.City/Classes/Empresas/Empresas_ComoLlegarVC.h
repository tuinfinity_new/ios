//
//  Empresas_ComoLlegarVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 14/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Empresas_ComoLlegarVC : UIViewController

@property (nonatomic, readwrite) double latitud;
@property (nonatomic, readwrite) double longitud;

@property (nonatomic, strong) NSMutableDictionary *itemImagen;

@end
