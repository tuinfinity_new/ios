//
//  Empresas_ValoracionesVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 21/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"
#import "EDStarRating.h"
#import "UIAlertView+Blocks.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"

#import "Cupones_TituloCabeceraCell.h"
#import "Empresas_ValoracionesCell.h"
#import "Empresas_DetalleCell.h"

#import "Empresas_ValoracionesVC.h"

@interface Empresas_ValoracionesVC ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) Empresas_ValoracionesCell *prototypeCellValoracion1;
@property (nonatomic, strong) Empresas_ValoracionesCell *prototypeCellValoracion2;

@property (nonatomic, strong) NSMutableArray *listadoDatos;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;

@property (nonatomic, readwrite) float valoracion;
@property (nonatomic, readwrite) float valoracionTotal;

@end

@implementation Empresas_ValoracionesVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _listadoDatos = [NSMutableArray array];

    [self configuraNavigationBar];
    
    _tapGesture.enabled = NO;
    
    if ( [_itemEmpresa objectForKey:@"miValoracion"] != nil )
        _valoracion = [[[_itemEmpresa objectForKey:@"miValoracion"] objectForKey:@"valoracion"] floatValue];
    else
        _valoracion = 0;
    _valoracionTotal = 0;
    
    [self downloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Prototype cells

- (Empresas_ValoracionesCell *)prototypeCellValoracion1
{
    if ( !_prototypeCellValoracion1 )
        _prototypeCellValoracion1 = [_tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Valoracion1"];
    return _prototypeCellValoracion1;
}

- (Empresas_ValoracionesCell *)prototypeCellValoracion2
{
    if ( !_prototypeCellValoracion2 )
        _prototypeCellValoracion2 = [_tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Valoracion2"];
    return _prototypeCellValoracion2;
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)downloadData
{
    [_activityIndicator startAnimating];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
    
    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager.securityPolicy setAllowInvalidCertificates:YES];
    operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
    [operationManager GET:[NSString stringWithFormat:@"empresa/%@/valoraciones", [_itemEmpresa objectForKey:@"id_empresa"]]
                parameters:@""
                   success:^(AFHTTPRequestOperation *operation, id responseObject){
                       [_activityIndicator stopAnimating];
                       
                       NSMutableArray *receivedData = [[NSJSONSerialization JSONObjectWithData:[[operation responseString] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] mutableCopy];

                       for ( NSDictionary *itemValoracion in receivedData )
                       {
                           _valoracionTotal += [[itemValoracion objectForKey:@"valoracion"] floatValue];
                       }
                       
                       [_listadoDatos addObjectsFromArray:receivedData];
                       [_tableView reloadData];
                   }
                   failure:^(AFHTTPRequestOperation *operation, NSError *error){
                       [_activityIndicator stopAnimating];
                   }];
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnEnviarValoracion_Pressed:(id)sender
{
    Empresas_ValoracionesCell *cell = (Empresas_ValoracionesCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
    [itemData setValue:[NSNumber numberWithFloat:_valoracion] forKey:@"valoracion"];
    [itemData setValue:cell.txvDescripcion.text forKey:@"texto"];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
    
    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager.securityPolicy setAllowInvalidCertificates:YES];
    operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
    [operationManager POST:[NSString stringWithFormat:@"empresa/%@/valoracion", [_itemEmpresa objectForKey:@"id_empresa"]]
                parameters:itemData
                   success:^(AFHTTPRequestOperation *operation, id responseObject){
                       [UIAlertView showWithTitle:@""
                                          message:NSLocalizedString(@"EMPRESAS_VALORACIONENVIADA", @"")
                                          handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                              if ( [_itemEmpresa objectForKey:@"miValoracion"] == nil )
                                              {
                                                  _valoracionTotal += _valoracion;
                                                  _originView.numValoraciones += 1;
                                                  _originView.valoracion = _valoracion;
                                                  _originView.valoracionMedia = _valoracionTotal / (float)_originView.numValoraciones;
                                              }
                                              else
                                              {
                                                  _valoracionTotal -= _originView.valoracion;
                                                  _valoracionTotal += _valoracion;
                                                  _originView.valoracion = _valoracion;
                                                  _originView.valoracionMedia = _valoracionTotal / (float)_originView.numValoraciones;
                                              }
                                              [_originView.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];

                                              [self.navigationController popViewControllerAnimated:NO];
                                          }];
                   }
                   failure:^(AFHTTPRequestOperation *operation, NSError *error){
                   }];
}

#pragma mark -
#pragma mark - UIGestureRecognizerDelegate

- (IBAction)singleTapGesture:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark - UITextViewdDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ( [text isEqualToString:@"\n"] )
        [textView resignFirstResponder];
    
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    _tapGesture.enabled = YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    _tapGesture.enabled = NO;
}

#pragma mark -
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _tapGesture.enabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _tapGesture.enabled = NO;
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Empresas_ValoracionesCell class]] )
    {
        Empresas_ValoracionesCell *cellToConfigure = (Empresas_ValoracionesCell *)cell;
        
        cellToConfigure.lblTitulo.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
        cellToConfigure.lblValoracionUsuario.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:14];
        cellToConfigure.lblValoracionFecha.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
        cellToConfigure.lblValoracionDescripcion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
        
        cellToConfigure.btnEnviarValoracion.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:14];

        if ( indexPath.section == 0 )
        {
            cellToConfigure.edsValoracion1.tintColor = COLOR_ORO;
            cellToConfigure.edsValoracion1.starImage = [[UIImage imageNamed:@"Img_Valoracion_iPhone2OFF"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cellToConfigure.edsValoracion1.starHighlightedImage = [[UIImage imageNamed:@"Img_Valoracion_iPhone2ON"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cellToConfigure.edsValoracion1.backgroundColor  = [UIColor clearColor];
            cellToConfigure.edsValoracion1.maxRating = 5.0;
            cellToConfigure.edsValoracion1.horizontalMargin = 0.0;
            cellToConfigure.edsValoracion1.editable = YES;
            cellToConfigure.edsValoracion1.rating = _valoracion;
            cellToConfigure.edsValoracion1.displayMode = EDStarRatingDisplayAccurate;
            [cellToConfigure.edsValoracion1 setNeedsDisplay];
            cellToConfigure.edsValoracion1.returnBlock = ^(float rating) {
                _valoracion = rating;
            };
            
            cellToConfigure.txvDescripcion.text = @"";
        }
        else if ( indexPath.section == 2 )
        {
            NSMutableDictionary *itemValoracion = [_listadoDatos objectAtIndex:indexPath.row];
            
            cellToConfigure.edsValoracion2.tintColor = COLOR_GRIS1;
            cellToConfigure.edsValoracion2.starImage = [[UIImage imageNamed:@"Img_Valoracion_iPhone1OFF"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cellToConfigure.edsValoracion2.starHighlightedImage = [[UIImage imageNamed:@"Img_Valoracion_iPhone1ON"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cellToConfigure.edsValoracion2.backgroundColor  = [UIColor clearColor];
            cellToConfigure.edsValoracion2.maxRating = 5.0;
            cellToConfigure.edsValoracion2.horizontalMargin = 0.0;
            cellToConfigure.edsValoracion2.editable = NO;
            cellToConfigure.edsValoracion2.rating = [[itemValoracion objectForKey:@"valoracion"] floatValue];
            cellToConfigure.edsValoracion2.displayMode = EDStarRatingDisplayAccurate;
            [cellToConfigure.edsValoracion2 setNeedsDisplay];
            
            NSDateFormatter *formatoFecha1 = [[NSDateFormatter alloc] init];
            NSDateFormatter *formatoFecha2 = [[NSDateFormatter alloc] init];
            [formatoFecha1 setDateFormat:@"dd MM yyyy HH:mm:ss"];
            [formatoFecha2 setDateStyle:NSDateFormatterMediumStyle];

            cellToConfigure.lblValoracionFecha.text = [formatoFecha2 stringFromDate:[formatoFecha1 dateFromString:[itemValoracion objectForKey:@"fecha"]]];
            cellToConfigure.lblValoracionUsuario.text = [itemValoracion objectForKey:@"nombreUsuario"];
            cellToConfigure.lblValoracionDescripcion.text = [itemValoracion objectForKey:@"texto"];
        }
    }
    else if ( [cell isKindOfClass:[Cupones_TituloCabeceraCell class]] )
    {
        Cupones_TituloCabeceraCell *cellToConfigure = (Cupones_TituloCabeceraCell *)cell;
        
        cellToConfigure.lblTitulo.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
        return 265;
    else if ( indexPath.section == 1 )
        return 40;
    else if ( indexPath.section == 2 )
    {
        [self configureCell:self.prototypeCellValoracion2 inTableView:tableView forRowAtIndexPath:indexPath];
        _prototypeCellValoracion2.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellValoracion2.bounds));
        [_prototypeCellValoracion2 layoutIfNeeded];
        
        CGSize size = [_prototypeCellValoracion2.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ( section )
    {
        case 0: return 1;
        case 1: return 1;
        case 2: return [_listadoDatos count];
        default: return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        Empresas_ValoracionesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Valoracion1"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    else if ( indexPath.section == 1 )
    {
        Cupones_TituloCabeceraCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Valoracion_Cabecera"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    else if ( indexPath.section == 2 )
    {
        Empresas_ValoracionesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Valoracion2"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    
    return nil;
}

@end