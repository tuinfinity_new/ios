//
//  EmpresasVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 02/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "BTSimpleSideMenu.h"
#import "HMSegmentedControl.h"
#import "UIImage_Blur.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"

#import "Ajustes_LocalizacionVC.h"
#import "Empresas_ListadoVC.h"

#import "EmpresasVC.h"

@interface EmpresasVC () <BTSimpleSideMenuDelegate>

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic) BTSimpleSideMenu *sideMenu;

@property (nonatomic, weak) IBOutlet UIScrollView *viewContainer;
@property (nonatomic, weak) IBOutlet UIScrollView *viewOpcionesBusqueda;
@property (weak, nonatomic) IBOutlet UIView *viewModoVista;

@property (nonatomic, weak) IBOutlet UIImageView *imgBlur;
@property (nonatomic, weak) IBOutlet UIImageView *imgFondo;

@property UIViewController *currentDetailViewController;

@property (nonatomic, strong) IBOutlet Empresas_ListadoVC *Empresas_ListadoVC;

@property (nonatomic, weak) IBOutlet UILabel *lblTituloCategoria;
@property (nonatomic, weak) IBOutlet UILabel *lblTituloDistancia;

@property (nonatomic, weak) IBOutlet UILabel *lblCategoria1;
@property (nonatomic, weak) IBOutlet UILabel *lblCategoria2;
@property (nonatomic, weak) IBOutlet UILabel *lblCategoria3;
@property (nonatomic, weak) IBOutlet UILabel *lblCategoria4;
@property (nonatomic, weak) IBOutlet UILabel *lblCategoria5;
@property (nonatomic, weak) IBOutlet UILabel *lblCategoria6;

@property (nonatomic, weak) IBOutlet UIImageView *imgCategoria1;
@property (nonatomic, weak) IBOutlet UIImageView *imgCategoria2;
@property (nonatomic, weak) IBOutlet UIImageView *imgCategoria3;
@property (nonatomic, weak) IBOutlet UIImageView *imgCategoria4;
@property (nonatomic, weak) IBOutlet UIImageView *imgCategoria5;
@property (nonatomic, weak) IBOutlet UIImageView *imgCategoria6;

@property (nonatomic, weak) IBOutlet UIButton *btnCategoria1;
@property (nonatomic, weak) IBOutlet UIButton *btnCategoria2;
@property (nonatomic, weak) IBOutlet UIButton *btnCategoria3;
@property (nonatomic, weak) IBOutlet UIButton *btnCategoria4;
@property (nonatomic, weak) IBOutlet UIButton *btnCategoria5;
@property (nonatomic, weak) IBOutlet UIButton *btnCategoria6;

@property (nonatomic, weak) IBOutlet UISearchBar *scbTextoBusqueda;

@property (nonatomic, weak) IBOutlet UISlider *sldRadioBusqueda;
@property (nonatomic, weak) IBOutlet UILabel *lblRadioBusqueda;

@property (nonatomic, weak) IBOutlet UIButton *btnBuscar;
@property (nonatomic, weak) IBOutlet UIButton *btnCancelar;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;

@property (nonatomic, retain) NSString *lastOpcionesBusqueda_Texto;

@end

@implementation EmpresasVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ( [[userDefaults objectForKey:@"tutorialActivo"] boolValue] )
    {
    }
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _viewOpcionesBusqueda.hidden = YES;
    _viewModoVista.hidden = YES;

    [self menuBusquedaVisible:NO];
    _scbTextoBusqueda.text = @"";
    _lastTextoBusqueda = @"";
    _lastCategoria = 0;
    
    [_lblTituloCategoria setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:18]];
    [_lblTituloDistancia setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:18]];
    
    [_lblCategoria1 setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14]];
    [_lblCategoria2 setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14]];
    [_lblCategoria3 setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14]];
    [_lblCategoria4 setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14]];
    [_lblCategoria5 setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14]];
    [_lblCategoria6 setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14]];
    
    _btnBuscar.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
    _btnCancelar.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    _Empresas_ListadoVC = [storyboard instantiateViewControllerWithIdentifier:@"Empresas_Listado"];
    
    [_Empresas_ListadoVC setOriginView:self];
    
    [self configuraNavigationBarWithBtnModoVista:YES andBtnFiltro:YES andBtnGeolocalizacion:YES];
    [self configuraMenuLateral];
    
    [self presentDetailController:_Empresas_ListadoVC];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = COLOR_AZUL1;
    
    [self menuBusquedaVisible:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self presentDetailController:_Empresas_ListadoVC];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBarWithBtnModoVista:(BOOL)btnModoVistaVisible andBtnFiltro:(BOOL)btnFiltroVisible andBtnGeolocalizacion:(BOOL)btnGeolocalizacionVisible
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *btnL2 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *btnR1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *btnR2 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_MenuPrincipal"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnMenuPrincipal_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnL2 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL2 setImage:[UIImage imageNamed:@"Btn_Geolocalizacion"] forState:UIControlStateNormal];
    [btnL2 addTarget:self action:@selector(btnGeolocalizacion_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnR1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnR1 setImage:[UIImage imageNamed:@"Btn_ModosVista"] forState:UIControlStateNormal];
    [btnR1 addTarget:self action:@selector(btnModosVista_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnR2 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnR2 setImage:[UIImage imageNamed:@"Btn_Filtrar"] forState:UIControlStateNormal];
    [btnR2 addTarget:self action:@selector(btnFiltrar_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnL1.selected = NO;
    btnL2.selected = NO;
    btnR1.selected = NO;
    btnR2.selected = NO;
    
    NSMutableArray *buttonItemsL = [[NSMutableArray alloc] init];
    NSMutableArray *buttonItemsR = [[NSMutableArray alloc] init];
    
    [buttonItemsL addObject:[[UIBarButtonItem alloc] initWithCustomView:btnL1]];
    if ( btnGeolocalizacionVisible ) [buttonItemsL addObject:[[UIBarButtonItem alloc] initWithCustomView:btnL2]];
    
    if ( btnModoVistaVisible ) [buttonItemsR addObject:[[UIBarButtonItem alloc] initWithCustomView:btnR1]];
    if ( btnFiltroVisible ) [buttonItemsR addObject:[[UIBarButtonItem alloc] initWithCustomView:btnR2]];
    
    self.navigationItem.leftBarButtonItems = buttonItemsL;
    self.navigationItem.rightBarButtonItems = buttonItemsR;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - Misc

- (void)configuraMenuLateral
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    BTSimpleMenuItem *menuItem1 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_1", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Cupones"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Cupones"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem2 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_2", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Ofertas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Ofertas"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem3 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_3", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Empresas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 _viewContainer.userInteractionEnabled = YES;
                                                             }];
    BTSimpleMenuItem *menuItem4 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_4", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Rutas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Rutas"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem5 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_5", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Lugares"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Lugares"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem6 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_6", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Wifi"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Wifi"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem0 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_0", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Ajustes"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Ajustes"] animated:NO];
                                                             }];
    
    _sideMenu = [[BTSimpleSideMenu alloc] initWithItem:@[menuItem1, menuItem2, menuItem3, menuItem4, menuItem5, menuItem6, menuItem0] addToViewController:self];
}

- (void)menuBusquedaVisible:(BOOL)stateVisible
{
    _imgBlur.hidden = !stateVisible;
    _imgFondo.hidden = !stateVisible;
    _viewOpcionesBusqueda.hidden = !stateVisible;
}

#pragma mark -
#pragma mark - View Controller

- (void)presentDetailController:(UIViewController*)detailVC
{
    if ( _currentDetailViewController )
        [self removeCurrentDetailViewController];
    
    _currentDetailViewController = detailVC;
    _currentDetailViewController.view.frame = _viewContainer.bounds;
    [_viewContainer addSubview:_currentDetailViewController.view];
}

- (void)removeCurrentDetailViewController
{
    [_currentDetailViewController.view removeFromSuperview];
    _currentDetailViewController = nil;
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnMenuPrincipal_Pressed:(id)sender
{
    [self menuBusquedaVisible:NO];
    [_sideMenu toggleMenu];
    _viewContainer.userInteractionEnabled = ![_sideMenu isOpen];
}

- (IBAction)btnModosVista_Pressed:(id)sender
{
    CGRect frameShown = CGRectMake([[UIScreen mainScreen] bounds].size.width - _viewModoVista.frame.size.width, 0, _viewModoVista.frame.size.width, _viewModoVista.frame.size.height);
    CGRect frameHidden = CGRectMake([[UIScreen mainScreen] bounds].size.width - _viewModoVista.frame.size.width, 0 - _viewModoVista.frame.size.height, _viewModoVista.frame.size.width, _viewModoVista.frame.size.height);
    
    if ( _viewModoVista.hidden )
    {
        [_viewModoVista setHidden:!_viewModoVista.hidden];
        [_viewModoVista setFrame:frameHidden];
        [UIView animateWithDuration:0.5 animations:^{ [_viewModoVista setFrame:frameShown]; }];
    }
    else
        [_viewModoVista setHidden:!_viewModoVista.hidden];
    
    _viewContainer.userInteractionEnabled = _viewModoVista.hidden;
}

- (IBAction)btnGeolocalizacion_Pressed:(id)sender
{
    [self menuBusquedaVisible:NO];
    [_sideMenu hide];
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Ajustes_LocalizacionVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Ajustes_Localizacion"];
    [pantallaDestino setLatitud:_datosAplicacion.latitud];
    [pantallaDestino setLongitud:_datosAplicacion.longitud];
    [self.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnFiltrar_Pressed:(id)sender
{
    if ( [_sideMenu isOpen] )
        [_sideMenu hide];
    
    if ( !_viewOpcionesBusqueda.hidden )
        [self menuBusquedaVisible:NO];
    else
    {
        [_btnCategoria1 setSelected:NO];
        [_btnCategoria2 setSelected:NO];
        [_btnCategoria3 setSelected:NO];
        [_btnCategoria4 setSelected:NO];
        [_btnCategoria5 setSelected:NO];
        [_btnCategoria6 setSelected:NO];
        
        if ( _lastCategoria == 1 ) _btnCategoria1.selected = YES;
        else if ( _lastCategoria == 2 ) _btnCategoria2.selected = YES;
        else if ( _lastCategoria == 3 ) _btnCategoria3.selected = YES;
        else if ( _lastCategoria == 4 ) _btnCategoria4.selected = YES;
        else if ( _lastCategoria == 5 ) _btnCategoria5.selected = YES;
        else if ( _lastCategoria == 6 ) _btnCategoria6.selected = YES;
        
        [_imgCategoria1 setImage:[UIImage imageNamed:( _btnCategoria1.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
        [_imgCategoria2 setImage:[UIImage imageNamed:( _btnCategoria2.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
        [_imgCategoria3 setImage:[UIImage imageNamed:( _btnCategoria3.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
        [_imgCategoria4 setImage:[UIImage imageNamed:( _btnCategoria4.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
        [_imgCategoria5 setImage:[UIImage imageNamed:( _btnCategoria5.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
        [_imgCategoria6 setImage:[UIImage imageNamed:( _btnCategoria6.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
        
        _scbTextoBusqueda.text = _lastTextoBusqueda;
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        if ( ![userDefaults objectForKey:@"radioBusqueda"] )
            _sldRadioBusqueda.value = 0.0;
        else
            _sldRadioBusqueda.value = [[userDefaults objectForKey:@"radioBusqueda"] doubleValue];
        
        if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
        {
            if ( _sldRadioBusqueda.value < 1000 )
                _lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (int)_sldRadioBusqueda.value];
            else
                _lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), (int)_sldRadioBusqueda.value / 1000.0];
        }
        else
        {
            if ( _sldRadioBusqueda.value < POINT_ONE_MILE_METERS )
                _lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), _sldRadioBusqueda.value * METERS_TO_FEET];
            else
                _lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), _sldRadioBusqueda.value * METERS_TO_MILES];
        }
        
        UIImage *screenShotImage = [self.view screenshotWitWidth:self.view.frame.size.width andHeight:self.view.frame.size.height];
        UIImage *blurredImage = [screenShotImage blurredImageWithRadius:10.0f iterations:5 tintColor:nil];
        
        _imgBlur.image = blurredImage;
        
        [self menuBusquedaVisible:YES];
    }
}

- (IBAction)btn_OpcionesBusqueda_Categoria_Pressed:(id)sender
{
    int categoria = (int)[(UIButton*)sender tag];
    
    _btnCategoria1.selected = NO;
    _btnCategoria2.selected = NO;
    _btnCategoria3.selected = NO;
    _btnCategoria4.selected = NO;
    _btnCategoria5.selected = NO;
    _btnCategoria6.selected = NO;
    
    if ( categoria == _lastCategoria )
        _lastCategoria = 0;
    else
    {
        switch ( categoria )
        {
            case 1: _btnCategoria1.selected = YES; break;
            case 2: _btnCategoria2.selected = YES; break;
            case 3: _btnCategoria3.selected = YES; break;
            case 4: _btnCategoria4.selected = YES; break;
            case 5: _btnCategoria5.selected = YES; break;
            case 6: _btnCategoria6.selected = YES; break;
        }
        _lastCategoria = categoria;
    }
    
    [_imgCategoria1 setImage:[UIImage imageNamed:( _btnCategoria1.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
    [_imgCategoria2 setImage:[UIImage imageNamed:( _btnCategoria2.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
    [_imgCategoria3 setImage:[UIImage imageNamed:( _btnCategoria3.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
    [_imgCategoria4 setImage:[UIImage imageNamed:( _btnCategoria4.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
    [_imgCategoria5 setImage:[UIImage imageNamed:( _btnCategoria5.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
    [_imgCategoria6 setImage:[UIImage imageNamed:( _btnCategoria6.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
}

- (IBAction)btn_OpcionesBusqueda_Buscar_Pressed:(id)sender
{
    _lastOpcionesBusqueda_Texto = _scbTextoBusqueda.text;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:[NSNumber numberWithDouble:_sldRadioBusqueda.value] forKey:@"radioBusqueda"];
    [userDefaults synchronize];
    
    int numOpcionesBusqueda = 0;
    
    NSMutableString *opcionesBusqueda = [[NSMutableString alloc] initWithCapacity:200];
    
    if ( [_scbTextoBusqueda.text length] > 0 )
    {
        if ( numOpcionesBusqueda == 0 )
            [opcionesBusqueda appendString:@"?"];
        else
            [opcionesBusqueda appendString:@"&"];
        
        numOpcionesBusqueda += 1;
        
        [opcionesBusqueda appendString:[NSString stringWithFormat:@"search=%@", _scbTextoBusqueda.text]];
    }
    
    //    if ( _sldRadioBusqueda.value > 0.0 )
    //    {
    //        if ( numOpcionesBusqueda == 0 )
    //            [opcionesBusqueda appendString:@"?"];
    //        else
    //            [opcionesBusqueda appendString:@"&"];
    //
    //        numOpcionesBusqueda += 1;
    //
    //        [opcionesBusqueda appendString:[NSString stringWithFormat:@"radio=%.2f", _sldRadioBusqueda.value]];
    //    }
    
    if ( _btnCategoria1.selected || _btnCategoria2.selected || _btnCategoria3.selected || _btnCategoria4.selected || _btnCategoria5.selected || _btnCategoria6.selected )
    {
        if ( numOpcionesBusqueda == 0 )
            [opcionesBusqueda appendString:@"?"];
        else
            [opcionesBusqueda appendString:@"&"];
        
        numOpcionesBusqueda += 1;
        
        if ( _btnCategoria1.selected ) [opcionesBusqueda appendString:@"categoria=1"];
        if ( _btnCategoria2.selected ) [opcionesBusqueda appendString:@"categoria=2"];
        if ( _btnCategoria3.selected ) [opcionesBusqueda appendString:@"categoria=3"];
        if ( _btnCategoria4.selected ) [opcionesBusqueda appendString:@"categoria=4"];
        if ( _btnCategoria5.selected ) [opcionesBusqueda appendString:@"categoria=5"];
        if ( _btnCategoria6.selected ) [opcionesBusqueda appendString:@"categoria=6"];
    }
    
    [_Empresas_ListadoVC setItemsInicio:0];
    [_Empresas_ListadoVC setOpcionesBusqueda_Parametros:opcionesBusqueda];
    [_Empresas_ListadoVC.listadoDatos removeAllObjects];
    [_Empresas_ListadoVC.listadoDatos_Imagenes removeAllObjects];
    [_Empresas_ListadoVC.listadoDatos_CoordenadasMapa removeAllObjects];
    [_Empresas_ListadoVC.listadoDatos_CoordenadasAR removeAllObjects];
    [_Empresas_ListadoVC.tableView reloadData];
    [_Empresas_ListadoVC downloadData];

    [self menuBusquedaVisible:NO];
}

- (IBAction)btn_OpcionesBusqueda_Cancelar_Pressed:(id)sender
{
    [self menuBusquedaVisible:NO];
}

#pragma mark -
#pragma mark - UIGestureRecognizerDelegate

- (IBAction)singleTapGesture:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark - UISlider

- (IBAction)sldRadioBusqueda_Radio_ValueChanged:(id)sender
{
    if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
    {
        if ( _sldRadioBusqueda.value < 1000 )
            _lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (int)_sldRadioBusqueda.value];
        else
            _lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), (int)_sldRadioBusqueda.value / 1000.0];
    }
    else
    {
        if ( _sldRadioBusqueda.value < POINT_ONE_MILE_METERS )
            _lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), _sldRadioBusqueda.value * METERS_TO_FEET];
        else
            _lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), _sldRadioBusqueda.value * METERS_TO_MILES];
    }
}

#pragma mark -
#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    _tapGesture.enabled = YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    _tapGesture.enabled = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    _lastTextoBusqueda = searchBar.text;
    [searchBar resignFirstResponder];
}

#pragma mark -
#pragma mark - TableView


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = [NSString stringWithFormat:@"CellTipoVista%d", (int)indexPath.row + 1];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [_viewModoVista setHidden:!_viewModoVista.hidden];
    _viewContainer.userInteractionEnabled = YES;
    
    if ( ( indexPath.row == 2 ) && ( !_datosAplicacion.geolocalizacionActiva ) )
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:NSLocalizedString(@"GEOLOCALIZACION_OFF_ERROR_RA", @"")
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    else
    {
        if ( !_Empresas_ListadoVC.itemsDownload )
        {
            _Empresas_ListadoVC.previousSelectedView = _Empresas_ListadoVC.selectedView;
            _Empresas_ListadoVC.selectedView = (int)indexPath.row + 1;
            [_Empresas_ListadoVC showSelectedView];
        }
    }
}

@end