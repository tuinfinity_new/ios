//
//  Empresas_ComoLlegarVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 14/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "DatosAplicacionSingleton.h"
#import "Globales.h"
#import "MapViewAnnotation.h"
#import "UIImage_Misc.h"

#import "Empresas_PuntoRutaCell.h"

#import "Empresas_PuntoRutaCollectionCell.h"

#import "Empresas_ComoLlegarVC.h"

@interface Empresas_ComoLlegarVC () <AVSpeechSynthesizerDelegate>

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet MKMapView *mapView;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UIButton *btnRuta1;
@property (nonatomic, weak) IBOutlet UIButton *btnRuta2;

@property (nonatomic, weak) IBOutlet UIImageView *imgBtnRuta1;
@property (nonatomic, weak) IBOutlet UIImageView *imgBtnRuta2;

@property (nonatomic, strong) MapViewAnnotation *annotationEmpresa;
@property (nonatomic, strong) MapViewAnnotation *annotationUsuario;

@property (nonatomic, readwrite) int tipoRuta;

@property (nonatomic, strong) Empresas_PuntoRutaCell *prototypeCellPuntoRuta;

@property (nonatomic, strong) NSMutableArray *listadoCoordenadas;
@property (nonatomic, strong) NSMutableArray *listadoPuntosRuta;

@property (nonatomic, readwrite) BOOL listadoPuntosRutaVisible;

@property (nonatomic, strong) CLLocation *lastLocation;

@property (nonatomic, strong) AVSpeechSynthesizer *speechSynthesizer;

@property (nonatomic, retain) NSString *rutaInstruccionesOld;

@end

@implementation Empresas_ComoLlegarVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _listadoCoordenadas = [NSMutableArray array];
    _listadoPuntosRuta = [NSMutableArray array];
    
    _tipoRuta = 1;
    [_btnRuta1 setSelected:( _tipoRuta == 1 ) ? YES : NO];
    [_btnRuta2 setSelected:( _tipoRuta == 2 ) ? YES : NO];
    [_imgBtnRuta1 setImage:[UIImage imageNamed:( _tipoRuta == 1 ) ? @"Btn_ComoLlegar_PeatonON" : @"Btn_ComoLlegar_PeatonOFF"]];
    [_imgBtnRuta2 setImage:[UIImage imageNamed:( _tipoRuta == 2 ) ? @"Btn_ComoLlegar_CocheON" : @"Btn_ComoLlegar_CocheOFF"]];
    
    [self configuraNavigationBar];

    CGRect frameTableView = _tableView.frame;
    frameTableView.origin.x = 320;
    _tableView.frame = frameTableView;
    
    _speechSynthesizer = [[AVSpeechSynthesizer alloc] init];
    _speechSynthesizer.delegate = self;
    
    _rutaInstruccionesOld = @"";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = COLOR_AZUL1;
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _listadoPuntosRutaVisible = NO;
    
    CGRect frameTableView = _tableView.frame;
    frameTableView.origin.x = 320;
    _tableView.frame = frameTableView;

    _annotationUsuario = [[MapViewAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(_mapView.userLocation.coordinate.latitude, _mapView.userLocation.coordinate.longitude)];
    _annotationUsuario.tag = 1;
    
    _annotationEmpresa = [[MapViewAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(_latitud, _longitud)];
    _annotationEmpresa.tag = 2;
    _annotationEmpresa.imagePinData = [_itemImagen objectForKey:@"fotoImgMapa"];
    
    [_mapView showAnnotations:@[_annotationUsuario, _annotationEmpresa] animated:NO];
    _mapView.showsUserLocation = YES;
    [_mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    
    _lastLocation = [[CLLocation alloc] initWithLatitude:_latitud longitude:_longitud];
    
    [self muestraRuta];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if ( _speechSynthesizer.speaking )
        [_speechSynthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Prototype cells

- (Empresas_PuntoRutaCell *)prototypeCellPuntoRuta
{
    if ( !_prototypeCellPuntoRuta )
        _prototypeCellPuntoRuta = [_tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_PuntoRuta"];
    return _prototypeCellPuntoRuta;
}

#pragma mark -
#pragma mark - Misc

- (void)listadoPuntosRuta_ChangeState
{
    _listadoPuntosRutaVisible = !_listadoPuntosRutaVisible;
    
    CGRect frameTableView = _tableView.frame;
    
    if ( _listadoPuntosRutaVisible )
        frameTableView.origin.x = 50;
    else
        frameTableView.origin.x = 320;
    
    [UIView animateWithDuration:1.0
                          delay:0.0
                        options:UIViewAnimationOptionTransitionNone
                     animations:^{
                         [_tableView setFrame:frameTableView];
                     }
                     completion:^(BOOL finished){
                     }];
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnRuta1_Pressed:(id)sender
{
    if ( _listadoPuntosRutaVisible )
        [self listadoPuntosRuta_ChangeState];

    _tipoRuta = 1;
    [_btnRuta1 setSelected:( _tipoRuta == 1 ) ? YES : NO];
    [_btnRuta2 setSelected:( _tipoRuta == 2 ) ? YES : NO];
    [_imgBtnRuta1 setImage:[UIImage imageNamed:( _tipoRuta == 1 ) ? @"Btn_ComoLlegar_PeatonON" : @"Btn_ComoLlegar_PeatonOFF"]];
    [_imgBtnRuta2 setImage:[UIImage imageNamed:( _tipoRuta == 2 ) ? @"Btn_ComoLlegar_CocheON" : @"Btn_ComoLlegar_CocheOFF"]];
    
    [self muestraRuta];
}

- (IBAction)btnRuta2_Pressed:(id)sender
{
    if ( _listadoPuntosRutaVisible )
        [self listadoPuntosRuta_ChangeState];

    _tipoRuta = 2;
    [_btnRuta1 setSelected:( _tipoRuta == 1 ) ? YES : NO];
    [_btnRuta2 setSelected:( _tipoRuta == 2 ) ? YES : NO];
    [_imgBtnRuta1 setImage:[UIImage imageNamed:( _tipoRuta == 1 ) ? @"Btn_ComoLlegar_PeatonON" : @"Btn_ComoLlegar_PeatonOFF"]];
    [_imgBtnRuta2 setImage:[UIImage imageNamed:( _tipoRuta == 2 ) ? @"Btn_ComoLlegar_CocheON" : @"Btn_ComoLlegar_CocheOFF"]];
    
    [self muestraRuta];
}

- (IBAction)btnPuntosRuta_Pressed:(id)sender
{
    [self listadoPuntosRuta_ChangeState];
}

#pragma mark -
#pragma mark  AVSpeechSynthesizerDelegate

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance
{
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
}

#pragma mark -
#pragma mark MapView

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ( [annotation isKindOfClass:[MapViewAnnotation class]] )
    {
        MapViewAnnotation *mapAnnotation = annotation;
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
        
        if ( !pinView )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
            
            pinView.animatesDrop = NO;
            pinView.canShowCallout = YES;
            pinView.draggable = NO;
            switch ( mapAnnotation.tag )
            {
                case 1:
                    pinView.image = nil;
                    break;
                case 2:
                    if ( [_itemImagen objectForKey:@"fotoImgMapa"] != [NSNull null] )
                        pinView.image = [UIImage imageWithData:mapAnnotation.imagePinData];
                    else
                        pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[[UIImage imageNamed:@"Img_NoFotoEmpresa"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
                    break;
                default:
                    break;
            }
            
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [rightButton setTitle:annotation.title forState:UIControlStateNormal];
            [rightButton setTag:mapAnnotation.tag];
            
            pinView.rightCalloutAccessoryView = rightButton;
            pinView.tag = mapAnnotation.tag;
            
        }
        else pinView.annotation = annotation;
        
        return pinView;
    }
    return nil;
}

- (void)muestraRuta
{
    MKDirectionsRequest *directionsRequest = [[MKDirectionsRequest alloc] init];
    
    directionsRequest.source = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:_annotationUsuario.coordinate addressDictionary:nil]];
    directionsRequest.destination = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:_annotationEmpresa.coordinate addressDictionary:nil]];
    directionsRequest.requestsAlternateRoutes = NO;
    switch ( _tipoRuta )
    {
        case 1: directionsRequest.transportType = MKDirectionsTransportTypeWalking; break;
        case 2: directionsRequest.transportType = MKDirectionsTransportTypeAutomobile; break;
        default: directionsRequest.transportType = MKDirectionsTransportTypeWalking; break;
    }
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
    [directions calculateDirectionsWithCompletionHandler: ^(MKDirectionsResponse *response, NSError *error) {
        if ( error )
            NSLog(@"Error %@", error.debugDescription);
        else
            [self drawRoute:response];
    }];
}

- (void)drawRoute:(MKDirectionsResponse *)response
{
    [_mapView removeOverlays:_listadoCoordenadas];
    [_listadoCoordenadas removeAllObjects];
    [_listadoPuntosRuta removeAllObjects];
    
    for ( MKRoute *route in response.routes )
    {
        [_listadoCoordenadas addObject:route.polyline];
        for ( MKRouteStep *step in route.steps )
        {
            NSString *distancia;
            if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
            {
                if ( step.distance < 1000.0 )
                    distancia = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (long)step.distance];
                else
                    distancia = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), (double)step.distance / 1000.0];
            }
            else
            {
                if ( step.distance < POINT_ONE_MILE_METERS )
                    distancia = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), (double)step.distance * METERS_TO_FEET];
                else
                    distancia = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), (double)step.distance * METERS_TO_MILES];
            }
            
            NSMutableDictionary *itemPuntoRuta = [[NSMutableDictionary alloc] init];
            [itemPuntoRuta setObject:distancia forKey:@"distancia"];
            [itemPuntoRuta setObject:step.instructions forKey:@"instrucciones"];
            [_listadoPuntosRuta addObject:itemPuntoRuta];
        }
    }
    [_mapView addOverlays:_listadoCoordenadas level:MKOverlayLevelAboveRoads];
    
    if ( [_listadoPuntosRuta count] > 0 )
    {
        _rutaInstruccionesOld = @"";
        [_tableView reloadData];
        [_collectionView reloadData];
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = COLOR_ROJO2;
    renderer.lineWidth = 5.0;
    return renderer;
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:_datosAplicacion.latitud longitude:_datosAplicacion.longitud];
    
    if ( [_lastLocation distanceFromLocation:newLocation] > 5.0 )
    {
        [_mapView removeAnnotations:_mapView.annotations];
        _annotationUsuario = [[MapViewAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(_mapView.userLocation.coordinate.latitude, _mapView.userLocation.coordinate.longitude)];
        _annotationUsuario.tag = 1;
        
        _annotationEmpresa = [[MapViewAnnotation alloc] initWithCoordinate:CLLocationCoordinate2DMake(_latitud, _longitud)];
        _annotationEmpresa.tag = 2;
        _annotationEmpresa.imagePinData = [_itemImagen objectForKey:@"fotoImgMapa"];
        
        [_mapView showAnnotations:@[_annotationUsuario, _annotationEmpresa] animated:NO];
        [self muestraRuta];
    }
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Empresas_PuntoRutaCell class]] )
    {
        Empresas_PuntoRutaCell *cellToConfigure = (Empresas_PuntoRutaCell *)cell;
        
        NSMutableDictionary *itemPuntoRuta = [_listadoPuntosRuta objectAtIndex:indexPath.row];
        
        cellToConfigure.lblDistancia.text = [itemPuntoRuta objectForKey:@"distancia"];
        cellToConfigure.lblInstrucciones.text = [itemPuntoRuta objectForKey:@"instrucciones"];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCellPuntoRuta inTableView:tableView forRowAtIndexPath:indexPath];
    _prototypeCellPuntoRuta.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellPuntoRuta.bounds));
    [_prototypeCellPuntoRuta layoutIfNeeded];
    
    CGSize size = [_prototypeCellPuntoRuta.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_listadoPuntosRuta count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Empresas_PuntoRutaCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_PuntoRuta"];
    [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
    return cell;
}

#pragma mark -
#pragma mark - UICollectionView

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return ( [_listadoPuntosRuta count] > 0 ) ? 1 : 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Empresas_PuntoRutaCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCellEmpresas_PuntoRuta" forIndexPath:indexPath];
    
    if ( [_listadoPuntosRuta count] > 0 )
    {
        NSMutableDictionary *itemPuntoRuta = [_listadoPuntosRuta objectAtIndex:indexPath.row];
        
        cell.lblInstrucciones.text = [itemPuntoRuta objectForKey:@"instrucciones"];

        if ( [_listadoPuntosRuta count] > 1 )
        {
            if ( ![_rutaInstruccionesOld isEqualToString:[itemPuntoRuta objectForKey:@"instrucciones"]] )
            {
                _rutaInstruccionesOld = [itemPuntoRuta objectForKey:@"instrucciones"];
                
                cell.lblInstrucciones.text = [NSString stringWithFormat:@"%@ %@. %@", [[_listadoPuntosRuta objectAtIndex:indexPath.row] objectForKey:@"instrucciones"], [[_listadoPuntosRuta objectAtIndex:indexPath.row + 1] objectForKey:@"distancia"], [[_listadoPuntosRuta objectAtIndex:indexPath.row + 1] objectForKey:@"instrucciones"]];

                if ( _speechSynthesizer.speaking )
                    [_speechSynthesizer stopSpeakingAtBoundary:AVSpeechBoundaryImmediate];
                
                AVSpeechUtterance *speechUtterance = [[AVSpeechUtterance alloc] initWithString:cell.lblInstrucciones.text];
                [speechUtterance setVoice:[AVSpeechSynthesisVoice voiceWithLanguage:[AVSpeechSynthesisVoice currentLanguageCode]]];
                [speechUtterance setRate:(AVSpeechUtteranceDefaultSpeechRate * 0.4)];
                [speechUtterance setPitchMultiplier:0.8];
                [_speechSynthesizer speakUtterance:speechUtterance];
            }
        }
        else
            cell.lblInstrucciones.text = [itemPuntoRuta objectForKey:@"instrucciones"];
    }
    else
        cell.lblInstrucciones.text = @"";
    
    return cell;
}

@end