//
//  Empresas_DetalleCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 22/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

#import "EDStarRating.h"

@interface Empresas_DetalleCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

@property (nonatomic, weak) IBOutlet UILabel *lblTituloContacto;
@property (nonatomic, weak) IBOutlet UILabel *lblTituloHorario;
@property (nonatomic, weak) IBOutlet UILabel *lblTituloLocalizacion;

@property (nonatomic, weak) IBOutlet UILabel *lblNombre;
@property (nonatomic, weak) IBOutlet UILabel *lblActividad;
@property (nonatomic, weak) IBOutlet UILabel *lblDistancia;
@property (nonatomic, weak) IBOutlet UILabel *lblNumOfertas;
@property (nonatomic, weak) IBOutlet UILabel *lblValoracionMedia;
@property (nonatomic, weak) IBOutlet UILabel *lblNumValoraciones;
@property (nonatomic, weak) IBOutlet UILabel *lblDescripcion;
@property (nonatomic, weak) IBOutlet UILabel *lblContactoWeb;
@property (nonatomic, weak) IBOutlet UILabel *lblContactoTelefono;
@property (nonatomic, weak) IBOutlet UILabel *lblContactoMail;
@property (nonatomic, weak) IBOutlet UILabel *lblHorario;
@property (nonatomic, weak) IBOutlet UILabel *lblDireccion;

@property (nonatomic, weak) IBOutlet UIView *viewValoracion;

@property (nonatomic, weak) IBOutlet EDStarRating *edsValoracion;

@property (nonatomic, weak) IBOutlet UIImageView *imgLogo;
@property (nonatomic, weak) IBOutlet UIImageView *imgWifi;

@property (nonatomic, weak) IBOutlet UIButton *btnRuta;
@property (nonatomic, weak) IBOutlet UIButton *btnValoracion;

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@property (nonatomic, weak) IBOutlet UIImageView *imgBtnRuta;

@property (nonatomic, weak) IBOutlet UILabel *lblBtnRuta;
@property (nonatomic, weak) IBOutlet UILabel *lblBtnComoLlegar;

@end