//
//  Empresas_ValoracionesCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 21/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EDStarRating.h"

@interface Empresas_ValoracionesCell : UITableViewCell

@property (nonatomic, weak) IBOutlet EDStarRating *edsValoracion1;
@property (nonatomic, weak) IBOutlet EDStarRating *edsValoracion2;

@property (nonatomic, weak) IBOutlet UITextView *txvDescripcion;

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;

@property (nonatomic, weak) IBOutlet UILabel *lblValoracionUsuario;
@property (nonatomic, weak) IBOutlet UILabel *lblValoracionFecha;
@property (nonatomic, weak) IBOutlet UILabel *lblValoracionDescripcion;

@property (nonatomic, weak) IBOutlet UIButton *btnEnviarValoracion;

@end
