//
//  Empresas_FotosCollectionCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 01/09/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Empresas_FotosCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UIImageView *imgFoto;

@end
