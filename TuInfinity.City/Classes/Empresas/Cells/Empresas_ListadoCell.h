//
//  Empresas_ListadoCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 22/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EDStarRating.h"

@interface Empresas_ListadoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *viewEmpresa;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UILabel *lblNombre;
@property (nonatomic, weak) IBOutlet UILabel *lblActividad;
@property (nonatomic, weak) IBOutlet UILabel *lblNumOfertas;
@property (nonatomic, weak) IBOutlet UILabel *lblDistancia;

@property (nonatomic, weak) IBOutlet UIImageView *imgFoto;
@property (nonatomic, weak) IBOutlet UIImageView *imgClientePremium;
@property (nonatomic, weak) IBOutlet UIImageView *imgWifi;

@property (nonatomic, weak) IBOutlet EDStarRating *edsValoracion;

@end