//
//  Empresas_DetalleVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 23/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import "Social/Social.h"

#import "AFNetworking.h"
#import "EDStarRating.h"
#import "UIAlertView+Blocks.h"
#import "UIImage_Blur.h"
#import "UIImageView+WebCache.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"
#import "MapViewAnnotation.h"
#import "UIImage_Misc.h"

#import "Empresas_DetalleCell.h"
#import "Cupones_CuponesListadoCell.h"
#import "Cupones_TituloCabeceraCell.h"
#import "Ofertas_TituloCabeceraCell.h"
#import "Ofertas_OfertasListadoCell.h"

#import "Empresas_FotosCollectionCell.h"

#import "Cupones_CuponesDetalleVC.h"
#import "Cupones_MisCuponesDetalleVC.h"
#import "Empresas_ComoLlegarVC.h"
#import "Empresas_ValoracionesVC.h"
#import "Ofertas_OfertasDetalleVC.h"

#import "Empresas_DetalleVC.h"

@interface Empresas_DetalleVC () <UIScrollViewDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, strong) NSMutableArray *listadoDatos_Categorias;

@property (nonatomic, strong) Empresas_DetalleCell *prototypeCellEmpresa1;
@property (nonatomic, strong) Empresas_DetalleCell *prototypeCellEmpresa2;

@property (nonatomic, readwrite) BOOL itemsDownload;

@property (nonatomic, weak) IBOutlet UIImageView *imgBlur;
@property (nonatomic, weak) IBOutlet UIImageView *imgFondo;

@property (nonatomic, weak) IBOutlet UIView *viewMenuCompartir;

@end

@implementation Empresas_DetalleVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _listadoDatos_Imagenes = [NSMutableArray array];
    _listadoDatos_Cupones = [NSMutableArray array];
    _listadoDatos_Cupones_Imagenes = [NSMutableArray array];
    _listadoDatos_Ofertas = [NSMutableArray array];
    _listadoDatos_Ofertas_Imagenes = [NSMutableArray array];

    for ( int numItem = 0; numItem < [[_itemEmpresa objectForKey:@"fotos"] count]; numItem++ )
    {
        NSMutableDictionary *itemImagen = [[NSMutableDictionary alloc] init];
        
        [itemImagen setValue:[NSNull null] forKey:@"fotoImgLista"];
        [itemImagen setValue:[NSNumber numberWithBool:YES] forKey:@"loading"];
        [_listadoDatos_Imagenes addObject:itemImagen];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
        dispatch_async(queue, ^{
            NSString *urlImagenComprimida = [[NSString stringWithFormat:@"https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=%d&refresh=31536000&url=%@", (int)self.view.frame.size.width, [[[_itemEmpresa objectForKey:@"fotos"] objectAtIndex:numItem] objectForKey:@"foto_medium"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            UIImage *imgFoto = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlImagenComprimida]]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                [[_listadoDatos_Imagenes objectAtIndex:numItem] setValue:[NSNumber numberWithBool:NO] forKey:@"loading"];
                [[_listadoDatos_Imagenes objectAtIndex:numItem] setValue:UIImagePNGRepresentation(imgFoto) forKey:@"fotoImgLista"];
                
                Empresas_DetalleCell *cellTableView = (Empresas_DetalleCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                [cellTableView.collectionView reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:numItem inSection:0]]];
            });
        });
    }

    [self configuraNavigationBar];
    [self menuCompartirVisible:NO];

    for ( int numItem = 0; numItem < [[_itemEmpresa objectForKey:@"listadoOfertas"] count]; numItem++ )
    {
        [_listadoDatos_Cupones addObject:[[[_itemEmpresa objectForKey:@"listadoOfertas"] objectAtIndex:numItem] mutableCopy]];

        NSMutableDictionary *itemImagen = [[NSMutableDictionary alloc] init];
        
        [itemImagen setValue:[NSNull null] forKey:@"fotoImgLista"];
        [itemImagen setValue:[NSNumber numberWithBool:NO] forKey:@"loading"];
        [_listadoDatos_Cupones_Imagenes addObject:itemImagen];
    }
    
    for ( int numItem = 0; numItem < [[_itemEmpresa objectForKey:@"listadoOfertasPago"] count]; numItem++ )
    {
        [_listadoDatos_Ofertas addObject:[[[_itemEmpresa objectForKey:@"listadoOfertasPago"] objectAtIndex:numItem] mutableCopy]];

        NSMutableDictionary *itemImagen = [[NSMutableDictionary alloc] init];
        
        [itemImagen setValue:[NSNull null] forKey:@"fotoImgLista"];
        [itemImagen setValue:[NSNumber numberWithBool:NO] forKey:@"loading"];
        [_listadoDatos_Ofertas_Imagenes addObject:itemImagen];
    }
    
    _listadoDatos_Categorias = [[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ListadoCategorias" ofType:@"plist"]];
    
    if ( [_itemEmpresa objectForKey:@"miValoracion"] != nil )
        _valoracion = [[[_itemEmpresa objectForKey:@"miValoracion"] objectForKey:@"valoracion"] floatValue];
    else
        _valoracion = 0;
    _valoracionMedia = [[_itemEmpresa objectForKey:@"valoracionMedia"] floatValue];
    _numValoraciones = [[_itemEmpresa objectForKey:@"numeroValoraciones"] intValue];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_activityIndicator stopAnimating];
    [[SDWebImageManager sharedManager] cancelAll];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Prototype cells

- (Empresas_DetalleCell *)prototypeCellEmpresa1
{
    if ( !_prototypeCellEmpresa1 )
        _prototypeCellEmpresa1 = [_tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Detalle1"];
    return _prototypeCellEmpresa1;
}

- (Empresas_DetalleCell *)prototypeCellEmpresa2
{
    if ( !_prototypeCellEmpresa2 )
        _prototypeCellEmpresa2 = [_tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Detalle2"];
    return _prototypeCellEmpresa2;
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - Misc

- (void)menuCompartirVisible:(BOOL)stateVisible
{
    _tableView.userInteractionEnabled = !stateVisible;
    
    _imgBlur.hidden = !stateVisible;
    _imgFondo.hidden = !stateVisible;
    _viewMenuCompartir.hidden = !stateVisible;
    _viewMenuCompartir.userInteractionEnabled = stateVisible;
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnComoLlegar_Pressed:(id)sender
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Empresas_ComoLlegarVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Empresas_ComoLlegar"];
    [pantallaDestino setLatitud:[[_itemEmpresa objectForKey:@"latitud"] doubleValue]];
    [pantallaDestino setLongitud:[[_itemEmpresa objectForKey:@"longitud"] doubleValue]];
    [pantallaDestino setItemImagen:_itemImagen];
    [self.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnValoracion_Pressed:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[userDefaults objectForKey:@"usrInvitado"] boolValue] )
        [UIAlertView showConfirmationDialogWithTitle:@""
                                             message:NSLocalizedString(@"OPCION_REQUERIDOLOGIN", @"")
                                        buttonCancel:NSLocalizedString(@"OPC_MASTARDE", @"")
                                            buttonOk:NSLocalizedString(@"OPC_LOGIN", @"")
                                             handler:^(UIAlertView *alertView, NSInteger buttonIndex){
                                                 if ( buttonIndex == 1 )
                                                 {
                                                     UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
                                                     [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Logout"] animated:NO];
                                                 }
                                             }];
    else
    {
        UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
        
        Empresas_ValoracionesVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Empresas_Valoraciones"];
        [pantallaDestino setOriginView:self];
        [pantallaDestino setItemEmpresa:_itemEmpresa];
        [self.navigationController pushViewController:pantallaDestino animated:NO];
    }
}

- (IBAction)btnOferta_Pressed:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[userDefaults objectForKey:@"usrInvitado"] boolValue] )
        [UIAlertView showConfirmationDialogWithTitle:@""
                                             message:NSLocalizedString(@"OPCION_REQUERIDOLOGIN", @"")
                                        buttonCancel:NSLocalizedString(@"OPC_MASTARDE", @"")
                                            buttonOk:NSLocalizedString(@"OPC_LOGIN", @"")
                                             handler:^(UIAlertView *alertView, NSInteger buttonIndex){
                                                 if ( buttonIndex == 1 )
                                                 {
                                                     UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
                                                     [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Logout"] animated:NO];
                                                 }
                                             }];
    else
    {
        int numRow = (int)[(UIButton*)sender tag];

        NSMutableDictionary *itemOferta = [_listadoDatos_Cupones objectAtIndex:numRow];

        if ( ( [[itemOferta objectForKey:@"limite_cupones"] intValue] > 0 ) && !( [[itemOferta objectForKey:@"limite_cupones"] intValue] > [[itemOferta objectForKey:@"veces_adquirido"] intValue] ) )
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:NSLocalizedString(@"CUPONES_CUPONAGOTADO", @"")
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        else
        {
            [_activityIndicator startAnimating];
            
            NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
            
            AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
            [operationManager.securityPolicy setAllowInvalidCertificates:YES];
            [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
            [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
            [operationManager POST:[NSString stringWithFormat:@"oferta/%d/adquirir", [[[_listadoDatos_Cupones objectAtIndex:numRow] objectForKey:@"id_oferta"] intValue]]
                        parameters:nil
                           success:^(AFHTTPRequestOperation *operation, id responseObject){
                               [_activityIndicator stopAnimating];
                               
                               NSMutableDictionary *receivedData = [[NSJSONSerialization JSONObjectWithData:[[operation responseString] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] mutableCopy];
                               
                               [[_listadoDatos_Cupones objectAtIndex:numRow] setValue:[NSNumber numberWithBool:YES] forKey:@"cupon_adquirido"];
                               [[_listadoDatos_Cupones objectAtIndex:numRow] setValue:[[receivedData objectForKey:@"oferta"] objectForKey:@"cupon"] forKey:@"cupon"];
                               [[_listadoDatos_Cupones objectAtIndex:numRow] setValue:[NSNumber numberWithInt:[[[_listadoDatos_Cupones objectAtIndex:numRow] objectForKey:@"veces_adquirido"] intValue] + 1] forKey:@"veces_adquirido"];
                               if ( [[[_listadoDatos_Cupones objectAtIndex:numRow] objectForKey:@"max_unidades"] intValue] > 0 )
                                   [[_listadoDatos_Cupones objectAtIndex:numRow] setValue:[NSNumber numberWithInt:[[[_listadoDatos_Cupones objectAtIndex:numRow] objectForKey:@"unidades_vendidas"] intValue] + 1] forKey:@"unidades_vendidas"];
                               
                               _originView.reloadDataAtAppear = YES;

                               [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:numRow inSection:3]] withRowAnimation:UITableViewRowAnimationNone];

                               [[[UIAlertView alloc] initWithTitle:@""
                                                           message:NSLocalizedString(@"CUPONES_CUPONGUARDADO", @"")
                                                          delegate:self
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil] show];
                           }
                           failure:^(AFHTTPRequestOperation *operation, NSError *error){
                               NSLog(@"%@",[error description]);
                               [_activityIndicator stopAnimating];
                           }];
        }
    }
}

- (IBAction)btnRuta_Pressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@""
                                message:NSLocalizedString(@"OPCION_PROXIMAMENTEDISPONIBLE", @"")
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

- (IBAction)btnCompartir_Pressed:(id)sender
{
    UIImage *screenShotImage = [self.view screenshotWitWidth:self.view.frame.size.width andHeight:self.view.frame.size.height];
    UIImage *blurredImage = [screenShotImage blurredImageWithRadius:10.0f iterations:5 tintColor:nil];
    
    _imgBlur.image = blurredImage;
    
    [self menuCompartirVisible:YES];
}

- (IBAction)btnCompartir_Mail_Pressed:(id)sender
{
    if ( ![MFMailComposeViewController canSendMail] )
        [UIAlertView showWithTitle:@""
                           message:NSLocalizedString(@"COMPARTIR_MAIL_ERROR", @"")
                           handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                               [self menuCompartirVisible:NO];
                           }];
    else
    {
        MFMailComposeViewController *mailView = [[MFMailComposeViewController alloc] init];
        
        mailView.mailComposeDelegate = self;
        
        [mailView setSubject:NSLocalizedString(@"COMPARTIR_MAIL_EMPRESA_TITULO", "")];
        
        NSString *mailBody = [NSString stringWithFormat:
                              @"<html>"
                                "<head>"
                                  "<style type=\"text/css\">"
                                    "body { font-family:Helvetica; font-size:14px; color:#000000; background-color:#FFFFFF;}"
                                  "</style>"
                                "</head>"
                                "<body>"
                                  "%@"
                                  "<br><br>"
                                  "<a href='%@'>%@</a>"
                                  "<br><br>"
                                  "%@"
                                "</body>"
                              "</html>",
                              NSLocalizedString(@"COMPARTIR_MAIL_EMPRESA_MENSAJE1", ""),
                              [NSString stringWithFormat:@"%@%@",URL_SERVIDOR_EMPRESA, [_itemEmpresa objectForKey:@"id_empresa"]], [_itemEmpresa objectForKey:@"nombre"],
                              NSLocalizedString(@"COMPARTIR_MAIL_EMPRESA_MENSAJE2", "")];
        [mailView setMessageBody:mailBody isHTML:YES];
        
        [self presentViewController:mailView animated:YES completion:nil];
    }
}

- (IBAction)btnCompartir_Facebook_Pressed:(id)sender
{
    if ( ![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook] )
        [UIAlertView showWithTitle:@""
                           message:NSLocalizedString(@"COMPARTIR_FACEBOOK_ERROR", @"")
                           handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                               [self menuCompartirVisible:NO];
                           }];
    else
    {
        NSString *urlDestino = [NSString stringWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@", [NSString stringWithFormat:@"%@%@",URL_SERVIDOR_EMPRESA, [_itemEmpresa objectForKey:@"id_empresa"]]]]
                                                        encoding:NSUTF8StringEncoding
                                                           error:nil];
        
        SLComposeViewController *composeView = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [composeView setInitialText:[NSString stringWithFormat:NSLocalizedString(@"COMPARTIR_FACEBOOK_EMPRESA_MENSAJE", ""), [_itemEmpresa objectForKey:@"nombre"]]];
        [composeView addURL:[NSURL URLWithString:urlDestino]];
        if ( [_itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
            [composeView addImage:[UIImage imageWithData:[_itemImagen objectForKey:@"fotoImgLista"]]];
        [self presentViewController:composeView animated:YES completion:nil];
    }
    
    [self menuCompartirVisible:NO];
}

- (IBAction)btnCompartir_Twitter_Pressed:(id)sender
{
    if ( ![SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter] )
        [UIAlertView showWithTitle:@""
                           message:NSLocalizedString(@"COMPARTIR_TWITTER_ERROR", @"")
                           handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                               [self menuCompartirVisible:NO];
                           }];
    else
    {
        NSString *urlDestino = [NSString stringWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@", [NSString stringWithFormat:@"%@%@",URL_SERVIDOR_EMPRESA, [_itemEmpresa objectForKey:@"id_empresa"]]]]
                                                        encoding:NSUTF8StringEncoding
                                                           error:nil];
        
        SLComposeViewController *composeView = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [composeView setInitialText:[NSString stringWithFormat:NSLocalizedString(@"COMPARTIR_TWITTER_EMPRESA_MENSAJE", ""), urlDestino]];
        if ( [_itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
            [composeView addImage:[UIImage imageWithData:[_itemImagen objectForKey:@"fotoImgLista"]]];
        
        [self presentViewController:composeView animated:YES completion:nil];
    }
    [self menuCompartirVisible:NO];
}

- (IBAction)btnCompartir_Cancelar_Pressed:(id)sender
{
    [self menuCompartirVisible:NO];
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Empresas_DetalleCell class]] )
    {
        if ( indexPath.section == 0 )
        {
            Empresas_DetalleCell *cellToConfigure = (Empresas_DetalleCell *)cell;

            cellToConfigure.imgLogo.layer.borderColor = [COLOR_AZUL1 CGColor];
            cellToConfigure.viewValoracion.layer.borderColor = [COLOR_ORO CGColor];
            
            cellToConfigure.lblNombre.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:20];
            
            cellToConfigure.imgBtnRuta.image = [UIImage imageNamed:@"Btn_Ruta_Add"];
            cellToConfigure.lblBtnRuta.text = NSLocalizedString(@"RUTA_ADD", @"");

            if ( [_itemImagen objectForKey:@"fotoImgLogo"] != [NSNull null] )
                cellToConfigure.imgLogo.image = [UIImage imageWithData:[_itemImagen objectForKey:@"fotoImgLogo"]];
            
            cellToConfigure.lblNombre.text = [_itemEmpresa objectForKey:@"nombre"];
            
            if ( [[_itemEmpresa objectForKey:@"subCategorias"] count] == 0 )
                cellToConfigure.lblActividad.text = @"";
            else
            {
                if ( [[_listadoDatos_Categorias filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"IdCategoria=%d", [[[_itemEmpresa objectForKey:@"subCategorias"] objectAtIndex:0] intValue]]] count] == 0 )
                    cellToConfigure.lblActividad.text = @"";
                else
                    cellToConfigure.lblActividad.text = [[[_listadoDatos_Categorias filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"IdCategoria=%d", [[[_itemEmpresa objectForKey:@"subCategorias"] objectAtIndex:0] intValue]]] objectAtIndex:0] objectForKey:@"Nombre"];
            }
            
            if ( [_itemEmpresa objectForKey:@"hasWifi"] == nil )
                cellToConfigure.imgWifi.hidden = YES;
            else
                cellToConfigure.imgWifi.hidden = ![[_itemEmpresa objectForKey:@"hasWifi"] boolValue];
            
            int numCupones = 0;
            int numOfertas = 0;
            
            if ( [_itemEmpresa objectForKey:@"listadoOfertas"] != nil )
                numCupones = [[_itemEmpresa objectForKey:@"listadoOfertas"] count];
            
            if ( [_itemEmpresa objectForKey:@"listadoOfertasPago"] != nil )
                numOfertas = [[_itemEmpresa objectForKey:@"listadoOfertasPago"] count];
            
            cellToConfigure.lblNumOfertas.text = [NSString stringWithFormat:NSLocalizedString(@"EMPRESAS_OFERTASACTIVAS", @""), ( numCupones + numOfertas )];
            
            CLLocationDistance distance = [_datosAplicacion.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[[_itemEmpresa objectForKey:@"latitud"] doubleValue] longitude:[[_itemEmpresa objectForKey:@"longitud"] doubleValue]]];
            
            if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
                cellToConfigure.lblDistancia.text = ( (long)distance < 1000L ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (long)distance] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), distance / 1000.0];
            else
                cellToConfigure.lblDistancia.text = ( distance < POINT_ONE_MILE_METERS ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), distance * METERS_TO_FEET] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), distance * METERS_TO_MILES];
            
            cellToConfigure.lblValoracionMedia.text = [NSString stringWithFormat:@"%.1f", _valoracionMedia];
            cellToConfigure.lblNumValoraciones.text = [NSString stringWithFormat:NSLocalizedString(@"EMPRESAS_VALORACION_NUMEROVALORACIONES", @""), _numValoraciones];
            
            cellToConfigure.edsValoracion.tintColor = COLOR_ORO;
            cellToConfigure.edsValoracion.starImage = [[[UIImage imageNamed:@"Img_Valoracion_iPhone1OFF"] imageByScalingProportionallyToSize:CGSizeMake(18, 18)] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cellToConfigure.edsValoracion.starHighlightedImage = [[[UIImage imageNamed:@"Img_Valoracion_iPhone1ON"] imageByScalingProportionallyToSize:CGSizeMake(18, 18)] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cellToConfigure.edsValoracion.backgroundColor  = [UIColor clearColor];
            cellToConfigure.edsValoracion.maxRating = 5.0;
            cellToConfigure.edsValoracion.horizontalMargin = 0.0;
            cellToConfigure.edsValoracion.editable = NO;
            cellToConfigure.edsValoracion.rating = _valoracion;
            cellToConfigure.edsValoracion.displayMode = EDStarRatingDisplayHalf;
            [cellToConfigure.edsValoracion setNeedsDisplay];
        }
        else if ( indexPath.section == 1 )
        {
            Empresas_DetalleCell *cellToConfigure = (Empresas_DetalleCell *)cell;
            
            cellToConfigure.lblTituloContacto.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
            cellToConfigure.lblTituloHorario.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
            cellToConfigure.lblTituloLocalizacion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
            
            cellToConfigure.lblDescripcion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
            cellToConfigure.lblContactoMail.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
            cellToConfigure.lblContactoTelefono.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
            cellToConfigure.lblContactoWeb.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
            cellToConfigure.lblHorario.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
            cellToConfigure.lblDireccion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
            cellToConfigure.lblBtnComoLlegar.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:12];
            cellToConfigure.lblBtnRuta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];

            cellToConfigure.lblDescripcion.text = [_itemEmpresa objectForKey:@"descripcion"];
            cellToConfigure.lblContactoWeb.text = [_itemEmpresa objectForKey:@"web"];
            cellToConfigure.lblContactoTelefono.text = [_itemEmpresa objectForKey:@"telefono"];
            cellToConfigure.lblContactoMail.text = [_itemEmpresa objectForKey:@"emailweb"];
            
            cellToConfigure.lblHorario.text = [_itemEmpresa objectForKey:@"horario"];
            
            [cellToConfigure.mapView removeAnnotations:cellToConfigure.mapView.annotations];
            MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:@""
                                                                       andCoordinate:CLLocationCoordinate2DMake([[_itemEmpresa objectForKey:@"latitud"] doubleValue], [[_itemEmpresa objectForKey:@"longitud"] doubleValue])];
            annotation.tag = [[_itemEmpresa objectForKey:@"id_empresa"] intValue];
            annotation.imagePinData = [_itemImagen objectForKey:@"fotoImgMapa"];
            [cellToConfigure.mapView addAnnotation:annotation];
            
            [cellToConfigure.mapView setRegion:[cellToConfigure.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake([[_itemEmpresa objectForKey:@"latitud"] doubleValue], [[_itemEmpresa objectForKey:@"longitud"] doubleValue]), 1500, 1500)]];
            
            cellToConfigure.lblDireccion.text = [_itemEmpresa objectForKey:@"direccion"];
        }
    }
    else if ( [cell isKindOfClass:[Cupones_TituloCabeceraCell class]] )
    {
        Cupones_TituloCabeceraCell *cellToConfigure = (Cupones_TituloCabeceraCell *)cell;

        cellToConfigure.lblTitulo.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
    }
    else if ( [cell isKindOfClass:[Cupones_CuponesListadoCell class]] )
    {
        Cupones_CuponesListadoCell *cellToConfigure = (Cupones_CuponesListadoCell *)cell;

        NSMutableDictionary *itemOferta = [_listadoDatos_Cupones objectAtIndex:indexPath.row];

        cellToConfigure.lblBtnRuta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];
        cellToConfigure.lblBtnOferta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];

        cellToConfigure.viewOferta.layer.shadowOffset = CGSizeMake(3,3);
        cellToConfigure.viewOferta.layer.shadowColor = [COLOR_NEGRO CGColor];
        cellToConfigure.viewOferta.layer.shadowRadius = 10.0;
        cellToConfigure.viewOferta.layer.shadowOpacity = 0.5;
        cellToConfigure.viewOferta.layer.shadowPath = [UIBezierPath bezierPathWithRect:cellToConfigure.viewOferta.bounds].CGPath;
        
        cellToConfigure.imgOfertaExclusiva.hidden = YES;
        cellToConfigure.imgUltimaHora.hidden = YES;
        cellToConfigure.lblUltimaHora.hidden = YES;
        
        cellToConfigure.lblTitulo.text = [itemOferta objectForKey:@"titulo"];
        
        if ( [[itemOferta objectForKey:@"precio"] doubleValue] - floor([[itemOferta objectForKey:@"precio"] doubleValue]) == 0.0 )
            cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%d€", [[itemOferta objectForKey:@"precio"] intValue]];
        else
            cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%.2f€", [[itemOferta objectForKey:@"precio"] doubleValue]];
        
        NSDictionary *attributedStringSettings = @{ NSStrikethroughStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle] };
        
        if ( [[itemOferta objectForKey:@"precio_anterior"] doubleValue] - floor([[itemOferta objectForKey:@"precio_anterior"] doubleValue]) == 0.0 )
            cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d€ ", [[itemOferta objectForKey:@"precio_anterior"] intValue]] attributes:attributedStringSettings];
        else
            cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f€", [[itemOferta objectForKey:@"precio_anterior"] doubleValue]] attributes:attributedStringSettings];
        
        cellToConfigure.imgUltimaHora.hidden = ![[itemOferta objectForKey:@"isFlash"] boolValue];
        cellToConfigure.lblUltimaHora.hidden = ![[itemOferta objectForKey:@"isFlash"] boolValue];
        
        NSString *descripcionTipo;
        switch ( [[itemOferta objectForKey:@"tipo"] intValue] )
        {
            case 0:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO0", ""), [itemOferta objectForKey:@"param1"], [itemOferta objectForKey:@"param2"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 1:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO1", ""), [itemOferta objectForKey:@"param1"], [itemOferta objectForKey:@"param2"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 2:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO2", ""), [itemOferta objectForKey:@"param1"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 3:
                descripcionTipo = @"";
                cellToConfigure.imgDescripcionTipo.hidden = YES;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 4:
                descripcionTipo = @"";
                cellToConfigure.imgDescripcionTipo.hidden = YES;
                cellToConfigure.lblPrecio.hidden = YES;
                cellToConfigure.lblPrecioOld.hidden = YES;
                cellToConfigure.imgOfertaRegalo.hidden = NO;
                break;
        }
        cellToConfigure.lblDescripcionTipo.text = descripcionTipo;
        
        CLLocationDistance distance = [_datosAplicacion.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[[[itemOferta objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue] longitude:[[[itemOferta objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue]]];
        
        if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
            cellToConfigure.lblDistancia.text = ( (long)distance < 1000L ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (long)distance] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), distance / 1000.0];
        else
            cellToConfigure.lblDistancia.text = ( distance < POINT_ONE_MILE_METERS ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), distance * METERS_TO_FEET] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), distance * METERS_TO_MILES];
        
        if ( [[itemOferta objectForKey:@"max_unidades"] intValue] > 0 )
        {
            cellToConfigure.imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Cantidad"];
            cellToConfigure.lblFinOferta.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_UNIDADESDISPONIBLES", @""), ([[itemOferta objectForKey:@"max_unidades"] intValue] - [[itemOferta objectForKey:@"unidades_vendidas"] intValue])];
        }
        else
        {
            NSDateFormatter *formatoFecha = [[NSDateFormatter alloc] init];
            [formatoFecha setDateFormat:@"dd MM yyyy HH:mm:ss"];
            
            cellToConfigure.imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Caducidad"];
            cellToConfigure.lblFinOferta.text = [Globales showDateDifferenceBetween:[NSDate date] and:[formatoFecha dateFromString:[itemOferta objectForKey:@"fecha_caducidad"]]];
        }
        
        [cellToConfigure.btnOferta setTag:indexPath.row];
        [cellToConfigure.btnOferta setBackgroundImage:[UIImage imageNamed:@"Btn_ColorAzul"] forState:UIControlStateNormal];
        cellToConfigure.imgBtnOferta.image = [UIImage imageNamed:@"Btn_Cupon_Add"];
        if ( [[itemOferta objectForKey:@"veces_adquirido"] intValue] == 0 )
            cellToConfigure.lblBtnOferta.text = NSLocalizedString(@"CUPONES_CUPON_ADD1", @"");
        else
            cellToConfigure.lblBtnOferta.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_CUPON_ADD2", @""), [[itemOferta objectForKey:@"veces_adquirido"] intValue]];
        
        [cellToConfigure.btnRuta setBackgroundImage:[UIImage imageNamed:@"Btn_ColorAzul"] forState:UIControlStateNormal];
        cellToConfigure.imgBtnRuta.image = [UIImage imageNamed:@"Btn_Ruta_Add"];
        cellToConfigure.lblBtnRuta.text = NSLocalizedString(@"RUTA_ADD", @"");
    }
    else if ( [cell isKindOfClass:[Ofertas_TituloCabeceraCell class]] )
    {
        Ofertas_TituloCabeceraCell *cellToConfigure = (Ofertas_TituloCabeceraCell *)cell;
        
        cellToConfigure.lblTitulo.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
    }
    else if ( [cell isKindOfClass:[Ofertas_OfertasListadoCell class]] )
    {
        Ofertas_OfertasListadoCell *cellToConfigure = (Ofertas_OfertasListadoCell *)cell;
        
        NSMutableDictionary *itemOferta = [_listadoDatos_Ofertas objectAtIndex:indexPath.row];
        
        cellToConfigure.lblBtnRuta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];
        
        cellToConfigure.viewOferta.layer.shadowOffset = CGSizeMake(3,3);
        cellToConfigure.viewOferta.layer.shadowColor = [COLOR_NEGRO CGColor];
        cellToConfigure.viewOferta.layer.shadowRadius = 10.0;
        cellToConfigure.viewOferta.layer.shadowOpacity = 0.5;
        cellToConfigure.viewOferta.layer.shadowPath = [UIBezierPath bezierPathWithRect:cellToConfigure.viewOferta.bounds].CGPath;
        
        cellToConfigure.imgOfertaExclusiva.hidden = YES;
        cellToConfigure.imgUltimaHora.hidden = YES;
        cellToConfigure.lblUltimaHora.hidden = YES;
        
        cellToConfigure.lblTitulo.text = [itemOferta objectForKey:@"titulo"];
        
        if ( [[itemOferta objectForKey:@"precio"] doubleValue] - floor([[itemOferta objectForKey:@"precio"] doubleValue]) == 0.0 )
            cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%d€", [[itemOferta objectForKey:@"precio"] intValue]];
        else
            cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%.2f€", [[itemOferta objectForKey:@"precio"] doubleValue]];
        
        NSDictionary *attributedStringSettings = @{ NSStrikethroughStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle] };
        
        if ( [[itemOferta objectForKey:@"precio_anterior"] doubleValue] - floor([[itemOferta objectForKey:@"precio_anterior"] doubleValue]) == 0.0 )
            cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d€ ", [[itemOferta objectForKey:@"precio_anterior"] intValue]] attributes:attributedStringSettings];
        else
            cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f€", [[itemOferta objectForKey:@"precio_anterior"] doubleValue]] attributes:attributedStringSettings];
        
        cellToConfigure.imgUltimaHora.hidden = ![[itemOferta objectForKey:@"isFlash"] boolValue];
        cellToConfigure.lblUltimaHora.hidden = ![[itemOferta objectForKey:@"isFlash"] boolValue];
        
        NSString *descripcionTipo;
        switch ( [[itemOferta objectForKey:@"tipo"] intValue] )
        {
            case 0:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO0", ""), [itemOferta objectForKey:@"param1"], [itemOferta objectForKey:@"param2"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 1:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO1", ""), [itemOferta objectForKey:@"param1"], [itemOferta objectForKey:@"param2"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 2:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO2", ""), [itemOferta objectForKey:@"param1"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 3:
                descripcionTipo = @"";
                cellToConfigure.imgDescripcionTipo.hidden = YES;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 4:
                descripcionTipo = @"";
                cellToConfigure.imgDescripcionTipo.hidden = YES;
                cellToConfigure.lblPrecio.hidden = YES;
                cellToConfigure.lblPrecioOld.hidden = YES;
                cellToConfigure.imgOfertaRegalo.hidden = NO;
                break;
        }
        cellToConfigure.lblDescripcionTipo.text = descripcionTipo;
        
        CLLocationDistance distance = [_datosAplicacion.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[[[itemOferta objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue] longitude:[[[itemOferta objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue]]];
        
        if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
            cellToConfigure.lblDistancia.text = ( (long)distance < 1000L ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (long)distance] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), distance / 1000.0];
        else
            cellToConfigure.lblDistancia.text = ( distance < POINT_ONE_MILE_METERS ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), distance * METERS_TO_FEET] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), distance * METERS_TO_MILES];
        
        if ( [[itemOferta objectForKey:@"max_unidades"] intValue] > 0 )
        {
            cellToConfigure.imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Cantidad"];
            cellToConfigure.lblFinOferta.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_UNIDADESDISPONIBLES", @""), ([[itemOferta objectForKey:@"max_unidades"] intValue] - [[itemOferta objectForKey:@"unidades_vendidas"] intValue])];
        }
        else
        {
            NSDateFormatter *formatoFecha = [[NSDateFormatter alloc] init];
            [formatoFecha setDateFormat:@"dd MM yyyy HH:mm:ss"];
            
            cellToConfigure.imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Caducidad"];
            cellToConfigure.lblFinOferta.text = [Globales showDateDifferenceBetween:[NSDate date] and:[formatoFecha dateFromString:[itemOferta objectForKey:@"fecha_caducidad"]]];
        }
        
        [cellToConfigure.btnRuta setBackgroundImage:[UIImage imageNamed:@"Btn_ColorAzul"] forState:UIControlStateNormal];
        cellToConfigure.imgBtnRuta.image = [UIImage imageNamed:@"Btn_Ruta_Add"];
        cellToConfigure.lblBtnRuta.text = NSLocalizedString(@"RUTA_ADD", @"");
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
        return 285;
    else if ( indexPath.section == 1 )
    {
        [self configureCell:self.prototypeCellEmpresa2 inTableView:tableView forRowAtIndexPath:indexPath];
        _prototypeCellEmpresa2.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellEmpresa2.bounds));
        [_prototypeCellEmpresa2 layoutIfNeeded];
        
        CGSize size = [_prototypeCellEmpresa2.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1;
    }
    else if ( indexPath.section == 2 )
        return 40;
    else if ( indexPath.section == 3 )
        return 255;
    else if ( indexPath.section == 4 )
        return 40;
    else if ( indexPath.section == 5 )
        return 255;
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ( section )
    {
        case 0: return 1;
        case 1: return 1;
        case 2: return ( [_listadoDatos_Cupones count] > 0 ) ? 1 : 0;
        case 3: return [_listadoDatos_Cupones count];
        case 4: return ( [_listadoDatos_Ofertas count] > 0 ) ? 1 : 0;
        case 5: return [_listadoDatos_Ofertas count];
        default: return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        Empresas_DetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Detalle1"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    else if ( indexPath.section == 1 )
    {
        Empresas_DetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Detalle2"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    else if ( indexPath.section == 2 )
    {
        Cupones_TituloCabeceraCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Cupones_Cabecera"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    else if ( indexPath.section == 3 )
    {
        Cupones_CuponesListadoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Cupones"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        
        NSMutableDictionary *itemOferta = [_listadoDatos_Cupones objectAtIndex:indexPath.row];
        NSMutableDictionary *itemImagen = [_listadoDatos_Cupones_Imagenes objectAtIndex:indexPath.row];

        if ( [itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
            cell.imgFoto.image = [UIImage imageWithData:[itemImagen objectForKey:@"fotoImgLista"]];
        else
        {
            cell.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
            
            NSString *urlImagenComprimida = [[NSString stringWithFormat:@"https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=%d&refresh=31536000&url=%@", (int)self.view.frame.size.width, [[itemOferta objectForKey:@"foto"] objectForKey:@"foto_medium"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [cell.activityIndicator startAnimating];
            [cell.imgFoto sd_setImageWithURL:[NSURL URLWithString:urlImagenComprimida]
                                   completed:^(UIImage *downloadedImage, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                       [cell.activityIndicator stopAnimating];
                                       if ( [_listadoDatos_Cupones_Imagenes count] > indexPath.row )
                                           [[_listadoDatos_Cupones_Imagenes objectAtIndex:indexPath.row] setValue:UIImagePNGRepresentation(downloadedImage) forKey:@"fotoImgLista"];
                                   }];
        }
        return cell;
    }
    else if ( indexPath.section == 4 )
    {
        Ofertas_TituloCabeceraCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Ofertas_Cabecera"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    else if ( indexPath.section == 5 )
    {
        Ofertas_OfertasListadoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellEmpresas_Ofertas"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        
        NSMutableDictionary *itemOferta = [_listadoDatos_Ofertas objectAtIndex:indexPath.row];
        NSMutableDictionary *itemImagen = [_listadoDatos_Ofertas_Imagenes objectAtIndex:indexPath.row];
        
        if ( [itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
            cell.imgFoto.image = [UIImage imageWithData:[itemImagen objectForKey:@"fotoImgLista"]];
        else
        {
            cell.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
            
            NSString *urlImagenComprimida = [[NSString stringWithFormat:@"https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=%d&refresh=31536000&url=%@", (int)self.view.frame.size.width, [[itemOferta objectForKey:@"foto"] objectForKey:@"foto_medium"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [cell.activityIndicator startAnimating];
            [cell.imgFoto sd_setImageWithURL:[NSURL URLWithString:urlImagenComprimida]
                                   completed:^(UIImage *downloadedImage, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                       [cell.activityIndicator stopAnimating];
                                       if ( [_listadoDatos_Ofertas_Imagenes count] > indexPath.row )
                                           [[_listadoDatos_Ofertas_Imagenes objectAtIndex:indexPath.row] setValue:UIImagePNGRepresentation(downloadedImage) forKey:@"fotoImgLista"];
                                   }];
        }
        return cell;
    }

    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 3 )
    {
        UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
        
        Cupones_CuponesDetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Cupones_CuponesDetalle"];
        
        [pantallaDestino setTypeOriginView:PANTALLAORIGEN_EMPRESASDETALLE];
        [pantallaDestino setOriginViewEmpresas:self];
        [pantallaDestino setItemOferta:[_listadoDatos_Cupones objectAtIndex:indexPath.row]];
        [pantallaDestino setItemImagen:[_listadoDatos_Cupones_Imagenes objectAtIndex:indexPath.row]];
        [pantallaDestino setItemImagenEmpresa:_itemImagen];
        [self.navigationController pushViewController:pantallaDestino animated:NO];
    }
    else if ( indexPath.section == 5 )
    {
        UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;

        Ofertas_OfertasDetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Ofertas_OfertasDetalle"];
        
        [pantallaDestino setTypeOriginView:PANTALLAORIGEN_EMPRESASDETALLE];
        [pantallaDestino setOriginViewEmpresas:self];
        [pantallaDestino setItemOferta:[_listadoDatos_Ofertas objectAtIndex:indexPath.row]];
        [pantallaDestino setItemImagen:[_listadoDatos_Ofertas_Imagenes objectAtIndex:indexPath.row]];
        [pantallaDestino setItemImagenEmpresa:_itemImagen];
        [self.navigationController pushViewController:pantallaDestino animated:NO];
    }
}

#pragma mark -
#pragma mark - UICollectionView

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_listadoDatos_Imagenes count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Empresas_FotosCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCellEmpresas_Fotos" forIndexPath:indexPath];
    
    NSMutableDictionary *itemImagen = [_listadoDatos_Imagenes objectAtIndex:indexPath.row];

    if ( [itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
        cell.imgFoto.image = [UIImage imageWithData:[itemImagen objectForKey:@"fotoImgLista"]];
    else
    {
        cell.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
        
        if ( [_listadoDatos_Imagenes count] > indexPath.row )
        {
            cell.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
            
            NSString *urlImagenComprimida = [[NSString stringWithFormat:@"https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=%d&refresh=31536000&url=%@", (int)self.view.frame.size.width, [[[_itemEmpresa objectForKey:@"fotos"] objectAtIndex:indexPath.row] objectForKey:@"foto_medium"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [cell.activityIndicator startAnimating];
            [cell.imgFoto sd_setImageWithURL:[NSURL URLWithString:urlImagenComprimida]
                                   completed:^(UIImage *downloadedImage, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                       [cell.activityIndicator stopAnimating];
                                       if ( [_listadoDatos_Imagenes count] > indexPath.row )
                                           [[_listadoDatos_Imagenes objectAtIndex:indexPath.row] setValue:UIImagePNGRepresentation(downloadedImage) forKey:@"fotoImgLista"];
                                   }];
        }
    }
    
    return cell;
}

#pragma mark -
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    Empresas_DetalleCell *cellTableView = (Empresas_DetalleCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    if ( scrollView == cellTableView.collectionView )
    {
        CGFloat frameWidth = scrollView.frame.size.width;
        int numCell = floor((scrollView.contentOffset.x - frameWidth / 2) / frameWidth) + 1;

        [cellTableView.collectionView  scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:numCell inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    }
}

#pragma mark -
#pragma mark MapView

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ( [annotation isKindOfClass:[MapViewAnnotation class]] )
    {
        MapViewAnnotation *mapAnnotation = annotation;
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
        
        if ( !pinView )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
            
            pinView.animatesDrop = NO;
            pinView.canShowCallout = NO;
            pinView.draggable = NO;
            if ( [_itemImagen objectForKey:@"fotoImgMapa"] != [NSNull null] )
                pinView.image = [UIImage imageWithData:mapAnnotation.imagePinData];
            else
                pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[[UIImage imageNamed:@"Img_NoFotoEmpresa"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
        }
        else pinView.annotation = annotation;
        
        return pinView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate

- (void) mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch ( result )
	{
		case MFMailComposeResultCancelled:
            break;
		case MFMailComposeResultSaved:
            break;
		case MFMailComposeResultSent:
            break;
		case MFMailComposeResultFailed:
            break;
		default:
            break;
	}
    [self dismissViewControllerAnimated:YES completion:nil];
    [self menuCompartirVisible:NO];
}

@end