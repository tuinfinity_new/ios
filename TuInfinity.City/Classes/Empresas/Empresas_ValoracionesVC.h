//
//  Empresas_ValoracionesVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 21/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Empresas_DetalleVC.h"

@interface Empresas_ValoracionesVC : UIViewController

@property (nonatomic, strong) NSMutableDictionary *itemEmpresa;

@property (nonatomic, weak) IBOutlet Empresas_DetalleVC *originView;

@end
