//
//  Empresas_ListadoVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 21/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ARKit.h"
#import "OCMapView.h"

#import "EmpresasVC.h"

@interface Empresas_ListadoVC : UIViewController

@property (nonatomic, weak) IBOutlet EmpresasVC *originView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet OCMapView *mapView;
@property (nonatomic, strong) IBOutlet ARViewController *arView;

@property (nonatomic, readwrite) NSString *opcionesBusqueda_Parametros;

@property (nonatomic, readwrite) int itemsInicio;
@property (nonatomic, readwrite) int itemsNum;
@property (nonatomic, readwrite) BOOL itemsDownload;

@property (nonatomic, strong) NSMutableArray *listadoDatos;
@property (nonatomic, strong) NSMutableArray *listadoDatos_Imagenes;
@property (nonatomic, strong) NSMutableArray *listadoDatos_CoordenadasMapa;
@property (nonatomic, strong) NSMutableArray *listadoDatos_CoordenadasAR;

@property (nonatomic, readwrite) BOOL reloadDataAtAppear;

@property (nonatomic, readwrite) int previousSelectedView;
@property (nonatomic, readwrite) int selectedView;

- (void)downloadData;

- (void)showSelectedView;

@end
