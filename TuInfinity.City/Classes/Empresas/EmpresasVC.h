//
//  EmpresasVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 02/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmpresasVC : UIViewController

@property (nonatomic, readwrite) int lastCategoria;
@property (nonatomic, retain) NSString *lastTextoBusqueda;

- (void)configuraNavigationBarWithBtnModoVista:(BOOL)btnModoVistaVisible andBtnFiltro:(BOOL)btnFiltroVisible andBtnGeolocalizacion:(BOOL)btnGeolocalizacionVisible;

@end