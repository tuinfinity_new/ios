//
//  Empresas_DetalleVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 23/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Empresas_ListadoVC.h"

@interface Empresas_DetalleVC : UIViewController

@property (nonatomic, weak) IBOutlet Empresas_ListadoVC *originView;

@property (nonatomic, readwrite) int numItemOrigen;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableDictionary *itemEmpresa;
@property (nonatomic, strong) NSMutableDictionary *itemImagen;

@property (nonatomic, strong) NSMutableArray *listadoDatos_Imagenes;
@property (nonatomic, strong) NSMutableArray *listadoDatos_Cupones;
@property (nonatomic, strong) NSMutableArray *listadoDatos_Cupones_Imagenes;
@property (nonatomic, strong) NSMutableArray *listadoDatos_Ofertas;
@property (nonatomic, strong) NSMutableArray *listadoDatos_Ofertas_Imagenes;

@property (nonatomic, readwrite) float valoracion;
@property (nonatomic, readwrite) float valoracionMedia;
@property (nonatomic, readwrite) int numValoraciones;

@end
