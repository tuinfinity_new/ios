//
//  Cupones_MisCuponesCanjeoVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 19/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cupones_MisCuponesCanjeoVC : UIViewController

@property (nonatomic, strong) NSMutableDictionary *itemCupon;

@end
