//
//  Cupones_MisCuponesDetalleVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 19/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cupones_MisCuponesDetalleVC : UIViewController

@property (nonatomic, readwrite) int estadoCupon;

@property (nonatomic, strong) NSMutableDictionary *itemCupon;
@property (nonatomic, strong) NSMutableDictionary *itemImagen;
@property (nonatomic, strong) NSMutableDictionary *itemImagenEmpresa;

@end
