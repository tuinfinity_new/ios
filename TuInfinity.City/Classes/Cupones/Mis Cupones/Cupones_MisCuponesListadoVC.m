//
//  Cupones_MisCuponesListadoVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 21/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"
#import "UIImage_Misc.h"

#import "Cupones_MisCuponesListadoCell.h"

#import "Cupones_MisCuponesDetalleVC.h"

#import "Cupones_MisCuponesListadoVC.h"

@interface Cupones_MisCuponesListadoVC () <UINavigationControllerDelegate, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, readwrite) BOOL itemsDownload;

@property (nonatomic, readwrite) NSMutableArray *sectionHidden;

@end

@implementation Cupones_MisCuponesListadoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _originView.navigationController.delegate = self;

    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _listadoDatos = [[NSMutableArray alloc] initWithArray:@[[NSMutableArray array],[NSMutableArray array],[NSMutableArray array]]];
    _listadoDatos_Imagenes = [[NSMutableArray alloc] initWithArray:@[[NSMutableArray array],[NSMutableArray array],[NSMutableArray array]]];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"TABLEVIEW_REFRESCO", @"")
                                                                     attributes:[NSDictionary dictionaryWithObject:[UIFont fontWithName:@"Arial" size:20] forKey:NSFontAttributeName]];
    [refreshControl addTarget:self action:@selector(reloadDataTableview:) forControlEvents:UIControlEventValueChanged];
    
    [_tableView addSubview:refreshControl];
    
    _itemsDownload = NO;
    
    _sectionHidden = [[NSMutableArray alloc] initWithArray:@[[NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO]]];
    
    _reloadDataAtAppear = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _originView.navigationController.delegate = self;
    
    if ( _reloadDataAtAppear )
    {
        [_listadoDatos removeAllObjects];
        [_listadoDatos_Imagenes removeAllObjects];
        
        _listadoDatos = [[NSMutableArray alloc] initWithArray:@[[NSMutableArray array],[NSMutableArray array],[NSMutableArray array]]];
        _listadoDatos_Imagenes = [[NSMutableArray alloc] initWithArray:@[[NSMutableArray array],[NSMutableArray array],[NSMutableArray array]]];
        
        [_tableView reloadData];
        
        _itemsDownload = NO;
        
        [self downloadData];
    }
    else
        _reloadDataAtAppear = YES;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    _tableView.frame = self.view.frame;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Misc

- (void)downloadData
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( ![[userDefaults objectForKey:@"usrInvitado"] boolValue] )
    {
        [_activityIndicator startAnimating];
        
        _itemsDownload = YES;
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
        
        AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
        [operationManager.securityPolicy setAllowInvalidCertificates:YES];
        [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
        [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
        [operationManager GET:@"usuario/cuponesListado"
                   parameters:nil
                      success:^(AFHTTPRequestOperation *operation, id responseObject){
                          NSMutableDictionary *receivedData = [[NSJSONSerialization JSONObjectWithData:[[operation responseString] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] mutableCopy];

                          [[_listadoDatos objectAtIndex:0] addObjectsFromArray:[receivedData objectForKey:@"cuponesActivos"]];
                          [[_listadoDatos objectAtIndex:1] addObjectsFromArray:[receivedData objectForKey:@"cuponesCaducados"]];
                          [[_listadoDatos objectAtIndex:2] addObjectsFromArray:[receivedData objectForKey:@"cuponesCanjeados"]];
                          
                          for ( int numSection = 0; numSection < 3; numSection++ )
                          {
                              for ( int numItem = 0; numItem < [[_listadoDatos objectAtIndex:numSection] count]; numItem++ )
                              {
                                  NSMutableDictionary *itemImagen = [[NSMutableDictionary alloc] init];
                                  
                                  [itemImagen setValue:[NSNull null] forKey:@"fotoImgLista"];
                                  [itemImagen setValue:[NSNull null] forKey:@"fotoImgLogo"];
                                  [itemImagen setValue:[NSNull null] forKey:@"fotoImgMapa"];
                                  [itemImagen setValue:[NSNull null] forKey:@"fotoImgRA"];
                                  [itemImagen setValue:[NSNumber numberWithBool:NO] forKey:@"loading"];
                                  
                                  [[_listadoDatos_Imagenes objectAtIndex:numSection] addObject:itemImagen];
                              }
                          }

                          [_tableView reloadData];
                          _itemsDownload = NO;
                          [_activityIndicator stopAnimating];
                      }
                      failure:^(AFHTTPRequestOperation *operation, NSError *error){
                          NSLog(@"%@",[error description]);
                          [_activityIndicator stopAnimating];
                      }];
    }
}

- (IBAction)reloadDataTableview:(id)sender
{
    [sender endRefreshing];
    
    if ( !_itemsDownload )
    {
        [_listadoDatos removeAllObjects];
        [_listadoDatos_Imagenes removeAllObjects];

        _listadoDatos = [[NSMutableArray alloc] initWithArray:@[[NSMutableArray array],[NSMutableArray array],[NSMutableArray array]]];
        _listadoDatos_Imagenes = [[NSMutableArray alloc] initWithArray:@[[NSMutableArray array],[NSMutableArray array],[NSMutableArray array]]];

        [_tableView reloadData];
        
        [self downloadData];
    }
}

#pragma mark -
#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ( viewController == _originView )
        [self viewWillAppear:animated];
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnOferta0_Pressed:(id)sender
{
    [_activityIndicator startAnimating];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
    
    int numRow = (int)[(UIButton*)sender tag];

    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager.securityPolicy setAllowInvalidCertificates:YES];
    [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];

    [operationManager DELETE:[NSString stringWithFormat:@"cupones/%d", [[[[_listadoDatos objectAtIndex:0] objectAtIndex:numRow] objectForKey:@"idOfertaUsuario"] intValue]]
                  parameters:nil
                     success:^(AFHTTPRequestOperation *operation, id responseObject){
                         [_activityIndicator stopAnimating];

                         [[_listadoDatos objectAtIndex:0] removeObjectAtIndex:numRow];
                         [[_listadoDatos_Imagenes objectAtIndex:0] removeObjectAtIndex:numRow];
                         [_tableView reloadData];
                     }
                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                         NSLog(@"%@",[error description]);
                         [_activityIndicator stopAnimating];
                     }];
}

- (IBAction)btnOferta1_Pressed:(id)sender
{
    [_activityIndicator startAnimating];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
    
    int numRow = (int)[(UIButton*)sender tag];
    
    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager.securityPolicy setAllowInvalidCertificates:YES];
    [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
    
     [operationManager DELETE:[NSString stringWithFormat:@"cupones/%d", [[[[_listadoDatos objectAtIndex:1] objectAtIndex:numRow] objectForKey:@"idOfertaUsuario"] intValue]]
                  parameters:nil
                     success:^(AFHTTPRequestOperation *operation, id responseObject){
                         [_activityIndicator stopAnimating];
                         
                         [[_listadoDatos objectAtIndex:1] removeObjectAtIndex:numRow];
                         [[_listadoDatos_Imagenes objectAtIndex:1] removeObjectAtIndex:numRow];
                         [_tableView reloadData];
                     }
                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                         NSLog(@"%@",[error description]);
                         [_activityIndicator stopAnimating];
                     }];
}

- (IBAction)btnOferta2_Pressed:(id)sender
{
    [_activityIndicator startAnimating];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
    
    int numRow = (int)[(UIButton*)sender tag];
    
    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager.securityPolicy setAllowInvalidCertificates:YES];
    [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
    
     [operationManager DELETE:[NSString stringWithFormat:@"cupones/%d", [[[[_listadoDatos objectAtIndex:2] objectAtIndex:numRow] objectForKey:@"idOfertaUsuario"] intValue]]
                  parameters:nil
                     success:^(AFHTTPRequestOperation *operation, id responseObject){
                         [_activityIndicator stopAnimating];
                         
                         [[_listadoDatos objectAtIndex:2] removeObjectAtIndex:numRow];
                         [[_listadoDatos_Imagenes objectAtIndex:2] removeObjectAtIndex:numRow];
                         [_tableView reloadData];
                     }
                     failure:^(AFHTTPRequestOperation *operation, NSError *error){
                         NSLog(@"%@",[error description]);
                         [_activityIndicator stopAnimating];
                     }];
}

- (IBAction)btnSeccion_Pressed:(id)sender
{
    int section = (int)[(UIButton*)sender tag];
    
    NSIndexSet *indexSection = [[NSIndexSet alloc] initWithIndex:section];
    
    [_sectionHidden replaceObjectAtIndex:section withObject:[NSNumber numberWithBool:![[_sectionHidden objectAtIndex:section] boolValue]]];
    [_tableView reloadSections:indexSection withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark -
#pragma mark - TableView

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *lblTitulo = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, tableView.frame.size.width - 30, 30)];
    [lblTitulo setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:15]];
    [lblTitulo setTextColor:COLOR_BLANCO];
    
    UIImageView *imgSeccion = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 15, 15)];

    switch ( section )
    {
        case 0:
            [imgSeccion setImage:[UIImage imageNamed:( [[_sectionHidden objectAtIndex:section] boolValue] ) ? @"Img_SeccionON" : @"Img_SeccionOFF"]];
            [lblTitulo setText:NSLocalizedString(@"MISCUPONES_CUPONESACTIVOS", @"")];
            break;
        case 1:
            [imgSeccion setImage:[UIImage imageNamed:( [[_sectionHidden objectAtIndex:section] boolValue] ) ? @"Img_SeccionON" : @"Img_SeccionOFF"]];
            [lblTitulo setText:NSLocalizedString(@"MISCUPONES_CUPONESCADUCADOS", @"")];
            break;
        case 2:
            [imgSeccion setImage:[UIImage imageNamed:( [[_sectionHidden objectAtIndex:section] boolValue] ) ? @"Img_SeccionON" : @"Img_SeccionOFF"]];
            [lblTitulo setText:NSLocalizedString(@"MISCUPONES_CUPONESUSADOS", @"")];
            break;
        default:
            break;
    }
    
    UIButton *btnSeccion = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSeccion.frame = CGRectMake(0, 0, tableView.frame.size.width, 25);
    btnSeccion.tag = section;
    [btnSeccion addTarget:self action:@selector(btnSeccion_Pressed:) forControlEvents:UIControlEventTouchUpInside];

    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = COLOR_AZUL1;

    [headerView addSubview:imgSeccion];
    [headerView addSubview:lblTitulo];
    [headerView addSubview:btnSeccion];

    return headerView;
}

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Cupones_MisCuponesListadoCell class]] )
    {
        if ( [[_listadoDatos_Imagenes objectAtIndex:indexPath.section] count] > indexPath.row )
        {
            Cupones_MisCuponesListadoCell *cellToConfigure = (Cupones_MisCuponesListadoCell *)cell;

            NSMutableDictionary *itemCupon = [[[_listadoDatos objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"oferta"];
            NSMutableDictionary *itemImagen = [[_listadoDatos_Imagenes objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            
            cellToConfigure.lblBtnOferta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];

            cellToConfigure.viewCupon.layer.shadowOffset = CGSizeMake(3,3);
            cellToConfigure.viewCupon.layer.shadowColor = [COLOR_NEGRO CGColor];
            cellToConfigure.viewCupon.layer.shadowRadius = 10.0;
            cellToConfigure.viewCupon.layer.shadowOpacity = 0.5;
            cellToConfigure.viewCupon.layer.shadowPath = [UIBezierPath bezierPathWithRect:cellToConfigure.viewCupon.bounds].CGPath;

            cellToConfigure.imgOfertaRA1.hidden = YES;
            cellToConfigure.imgOfertaRA2.hidden = YES;
            cellToConfigure.imgOfertaExclusiva.hidden = YES;
            cellToConfigure.imgUltimaHora.hidden = YES;
            cellToConfigure.lblUltimaHora.hidden = YES;
            
            cellToConfigure.lblTitulo.text = [itemCupon objectForKey:@"titulo"];
            
            if ( [[itemCupon objectForKey:@"precio"] doubleValue] - floor([[itemCupon objectForKey:@"precio"] doubleValue]) == 0.0 )
                cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%d€", [[itemCupon objectForKey:@"precio"] intValue]];
            else
                cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%.2f€", [[itemCupon objectForKey:@"precio"] doubleValue]];
            
            NSDictionary *attributedStringSettings = @{ NSStrikethroughStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle] };
            
            if ( [[itemCupon objectForKey:@"precio_anterior"] doubleValue] - floor([[itemCupon objectForKey:@"precio_anterior"] doubleValue]) == 0.0 )
                cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d€ ", [[itemCupon objectForKey:@"precio_anterior"] intValue]] attributes:attributedStringSettings];
            else
                cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f€", [[itemCupon objectForKey:@"precio_anterior"] doubleValue]] attributes:attributedStringSettings];
            
            cellToConfigure.imgUltimaHora.hidden = ![[itemCupon objectForKey:@"isFlash"] boolValue];
            cellToConfigure.lblUltimaHora.hidden = ![[itemCupon objectForKey:@"isFlash"] boolValue];

            switch ( [[itemCupon objectForKey:@"tipo"] intValue] )
            {
                case 0:
                    cellToConfigure.lblPrecio.hidden = NO;
                    cellToConfigure.lblPrecioOld.hidden = NO;
                    cellToConfigure.imgOfertaRegalo.hidden = YES;
                    break;
                case 1:
                    cellToConfigure.lblPrecio.hidden = NO;
                    cellToConfigure.lblPrecioOld.hidden = NO;
                    cellToConfigure.imgOfertaRegalo.hidden = YES;
                    break;
                case 2:
                    cellToConfigure.lblPrecio.hidden = NO;
                    cellToConfigure.lblPrecioOld.hidden = NO;
                    cellToConfigure.imgOfertaRegalo.hidden = YES;
                    break;
                case 3:
                    cellToConfigure.lblPrecio.hidden = NO;
                    cellToConfigure.lblPrecioOld.hidden = NO;
                    cellToConfigure.imgOfertaRegalo.hidden = YES;
                    break;
                case 4:
                    cellToConfigure.lblPrecio.hidden = YES;
                    cellToConfigure.lblPrecioOld.hidden = YES;
                    cellToConfigure.imgOfertaRegalo.hidden = NO;
                    break;
            }

            [cellToConfigure.btnOferta setBackgroundImage:[UIImage imageNamed:@"Btn_ColorNegro"] forState:UIControlStateNormal];
            cellToConfigure.imgBtnOferta.image = [UIImage imageNamed:@"Btn_Cupon_Remove"];
            cellToConfigure.lblBtnOferta.text = NSLocalizedString(@"CUPONES_CUPON_REMOVE", @"");

            NSDateFormatter *formatoFecha1 = [[NSDateFormatter alloc] init];
            NSDateFormatter *formatoFecha2 = [[NSDateFormatter alloc] init];
            NSDateFormatter *formatoFecha3 = [[NSDateFormatter alloc] init];
            [formatoFecha1 setDateFormat:@"dd MM yyyy HH:mm:ss"];
            [formatoFecha2 setDateStyle:NSDateFormatterMediumStyle];
            [formatoFecha3 setDateFormat:@"HH:mm"];

            switch ( indexPath.section )
            {
                case 0:
                    cellToConfigure.imgFechaCaducidad.hidden = NO;
                    cellToConfigure.lblFechaCaducidad1.text = NSLocalizedString(@"CUPONES_FECHACADUCIDAD1", @"");
                    cellToConfigure.lblFechaCaducidad2.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_FECHA", @""), [formatoFecha2 stringFromDate:[formatoFecha1 dateFromString:[itemCupon objectForKey:@"fecha_caducidad"]]], [formatoFecha3 stringFromDate:[formatoFecha1 dateFromString:[itemCupon objectForKey:@"fecha_caducidad"]]]];
                    break;
                case 1:
                    cellToConfigure.imgFechaCaducidad.hidden = NO;
                    cellToConfigure.lblFechaCaducidad1.text = NSLocalizedString(@"CUPONES_FECHACADUCIDAD3", @"");
                    cellToConfigure.lblFechaCaducidad2.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_FECHA", @""), [formatoFecha2 stringFromDate:[formatoFecha1 dateFromString:[itemCupon objectForKey:@"fecha_caducidad"]]], [formatoFecha3 stringFromDate:[formatoFecha1 dateFromString:[itemCupon objectForKey:@"fecha_caducidad"]]]];
                    break;
                case 2:
                    cellToConfigure.imgFechaCaducidad.hidden = YES;
                    cellToConfigure.lblFechaCaducidad1.text = @"";
                    cellToConfigure.lblFechaCaducidad2.text = @"";
                    break;
                default:
                    break;
            }

            [cellToConfigure.btnOferta setTag:indexPath.row];
            switch ( indexPath.section )
            {
                case 0:
                    [cellToConfigure.btnOferta addTarget:self action:@selector(btnOferta0_Pressed:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case 1:
                    [cellToConfigure.btnOferta addTarget:self action:@selector(btnOferta1_Pressed:) forControlEvents:UIControlEventTouchUpInside];
                    break;
                case 2:
                    [cellToConfigure.btnOferta addTarget:self action:@selector(btnOferta2_Pressed:) forControlEvents:UIControlEventTouchUpInside];
                    break;
            }

            if ( [itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
                cellToConfigure.imgFoto.image = [UIImage imageWithData:[itemImagen objectForKey:@"fotoImgLista"]];
            else
            {
                cellToConfigure.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
                if ( ![[itemImagen objectForKey:@"loading"] boolValue] )
                {
                    [[[_listadoDatos_Imagenes objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] setValue:[NSNumber numberWithBool:YES] forKey:@"loading"];
                    
                    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
                    dispatch_async(queue, ^{
                        UIImage *imgFoto = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[itemCupon objectForKey:@"foto"] objectForKey:@"foto_medium"]]]];
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            UIImage *imgFotoLista = [UIImage imageWithImage:imgFoto scaledToWidth:self.view.frame.size.width];
                            
                            [[[_listadoDatos_Imagenes objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] setValue:[NSNumber numberWithBool:NO] forKey:@"loading"];
                            [[[_listadoDatos_Imagenes objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] setValue:UIImagePNGRepresentation(imgFotoLista) forKey:@"fotoImgLista"];
                            [_tableView beginUpdates];
                            [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                            [_tableView endUpdates];
                        });
                    });
                }
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ( section )
    {
        case 0: return ( ![[_sectionHidden objectAtIndex:section] boolValue] ) ? 0 : [[_listadoDatos objectAtIndex:0] count];
        case 1: return ( ![[_sectionHidden objectAtIndex:section] boolValue] ) ? 0 : [[_listadoDatos objectAtIndex:1] count];
        case 2: return ( ![[_sectionHidden objectAtIndex:section] boolValue] ) ? 0 : [[_listadoDatos objectAtIndex:2] count];
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Cupones_MisCuponesListadoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellCupones_MisCuponesListado"];
    [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _reloadDataAtAppear = NO;
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;

    Cupones_MisCuponesDetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Cupones_MisCuponesDetalle"];
    
    pantallaDestino.estadoCupon = (int)indexPath.section;
    switch ( indexPath.section )
    {
        case 0:
            [pantallaDestino setItemCupon:[[_listadoDatos objectAtIndex:0] objectAtIndex:indexPath.row]];
            [pantallaDestino setItemImagen:[[_listadoDatos_Imagenes objectAtIndex:0] objectAtIndex:indexPath.row]];
            break;
        case 1:
            [pantallaDestino setItemCupon:[[_listadoDatos objectAtIndex:1] objectAtIndex:indexPath.row]];
            [pantallaDestino setItemImagen:[[_listadoDatos_Imagenes objectAtIndex:1] objectAtIndex:indexPath.row]];
            break;
        case 2:
            [pantallaDestino setItemCupon:[[_listadoDatos objectAtIndex:2] objectAtIndex:indexPath.row]];
            [pantallaDestino setItemImagen:[[_listadoDatos_Imagenes objectAtIndex:2] objectAtIndex:indexPath.row]];
            break;
    }
    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

@end