//
//  Cupones_MisCuponesCanjeoVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 19/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "ZXingObjC.h"

#import "Globales.h"

#import "Cupones_MisCuponesDetalleCell.h"

#import "Cupones_MisCuponesCanjeoVC.h"

@interface Cupones_MisCuponesCanjeoVC ()

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) Cupones_MisCuponesDetalleCell *prototypeCellCupon;

@property (nonatomic, readwrite) BOOL btnCodigoQRActivo;
@property (nonatomic, readwrite) BOOL btnCodigoEANActivo;

@end

@implementation Cupones_MisCuponesCanjeoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _btnCodigoQRActivo = NO;
    _btnCodigoEANActivo = NO;

    [self configuraNavigationBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Prototype cells

- (Cupones_MisCuponesDetalleCell *)prototypeCellCupon
{
    if ( !_prototypeCellCupon )
        _prototypeCellCupon = [_tableView dequeueReusableCellWithIdentifier:@"CellCupones_MisCuponesCanjeo"];
    return _prototypeCellCupon;
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnCodigoQR_Pressed:(id)sender
{
    _btnCodigoQRActivo = YES;
    _btnCodigoEANActivo = NO;

    [_tableView reloadData];
}

- (IBAction)btnCodigoEAN_Pressed:(id)sender
{
    _btnCodigoQRActivo = NO;
    _btnCodigoEANActivo = YES;

    [_tableView reloadData];
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Cupones_MisCuponesDetalleCell class]] )
    {
        Cupones_MisCuponesDetalleCell *cellToConfigure = (Cupones_MisCuponesDetalleCell *)cell;

        cellToConfigure.lblTitulo.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:20];
        cellToConfigure.lblTituloCodigo.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
        cellToConfigure.lblCodigo.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:25];

        cellToConfigure.btnCodigoQR.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
        cellToConfigure.btnCodigoEAN.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];

        cellToConfigure.lblTitulo.text = [[_itemCupon objectForKey:@"oferta"] objectForKey:@"titulo"];
        
        NSDateFormatter *formatoFecha1 = [[NSDateFormatter alloc] init];
        NSDateFormatter *formatoFecha2 = [[NSDateFormatter alloc] init];
        NSDateFormatter *formatoFecha3 = [[NSDateFormatter alloc] init];
        [formatoFecha1 setDateFormat:@"dd MM yyyy HH:mm:ss"];
        [formatoFecha2 setDateStyle:NSDateFormatterMediumStyle];
        [formatoFecha3 setDateFormat:@"HH:mm"];

        cellToConfigure.lblFechaCaducidad.text = [NSString stringWithFormat:NSLocalizedString(@"MISCUPONES_FECHACADUCIDAD", @""), [formatoFecha2 stringFromDate:[formatoFecha1 dateFromString:[[_itemCupon objectForKey:@"oferta"] objectForKey:@"fecha_caducidad"]]], [formatoFecha3 stringFromDate:[formatoFecha1 dateFromString:[[_itemCupon objectForKey:@"oferta"] objectForKey:@"fecha_caducidad"]]]];

        cellToConfigure.lblCodigo.text = [_itemCupon objectForKey:@"codigo"];

        cellToConfigure.btnCodigoQR.selected = _btnCodigoQRActivo;
        cellToConfigure.btnCodigoEAN.selected = _btnCodigoEANActivo;

        if ( !_btnCodigoQRActivo )
            cellToConfigure.imgCodigoQR.image = [UIImage imageNamed:@"Img_FondoTransparente"];
        else
        {
            ZXMultiFormatWriter *zxWriter = [[ZXMultiFormatWriter alloc] init];
            ZXBitMatrix *zxResult = [zxWriter encode:[_itemCupon objectForKey:@"codigo"]
                                              format:kBarcodeFormatQRCode
                                               width:(int)cellToConfigure.imgCodigoQR.frame.size.width
                                              height:(int)cellToConfigure.imgCodigoQR.frame.size.height
                                               error:nil];
            
            if ( !zxResult )
                cellToConfigure.imgCodigoQR.image = [UIImage imageNamed:@"Img_FondoTransparente"];
            else
            {
                ZXImage *zxImage = [ZXImage imageWithMatrix:zxResult];
                cellToConfigure.imgCodigoQR.image = [UIImage imageWithCGImage:zxImage.cgimage];
            }
        }
        
        if ( !_btnCodigoEANActivo )
            cellToConfigure.imgCodigoEAN.image = [UIImage imageNamed:@"Img_FondoTransparente"];
        else
        {
            ZXMultiFormatWriter *zxWriter = [[ZXMultiFormatWriter alloc] init];
            ZXBitMatrix *zxResult = [zxWriter encode:[_itemCupon objectForKey:@"codigo"]
                                              format:kBarcodeFormatEan8
                                               width:(int)cellToConfigure.imgCodigoEAN.frame.size.width
                                              height:(int)cellToConfigure.imgCodigoEAN.frame.size.height
                                               error:nil];
            
            if ( !zxResult )
                cellToConfigure.imgCodigoEAN.image = [UIImage imageNamed:@"Img_FondoTransparente"];
            else
            {
                ZXImage *zxImage = [ZXImage imageWithMatrix:zxResult];
                cellToConfigure.imgCodigoEAN.image = [UIImage imageWithCGImage:zxImage.cgimage];
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        [self configureCell:self.prototypeCellCupon inTableView:tableView forRowAtIndexPath:indexPath];
        
        _prototypeCellCupon.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellCupon.bounds));
        [_prototypeCellCupon layoutIfNeeded];
        
        CGSize size = [_prototypeCellCupon.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1;
    }
    else if ( indexPath.section == 1 )
        return ( _btnCodigoQRActivo ) ? 170 : 0;
    else if ( indexPath.section == 2 )
        return ( _btnCodigoEANActivo ) ? 120 : 0;

    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        Cupones_MisCuponesDetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellCupones_MisCuponesCanjeo"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    else if ( indexPath.section == 1 )
    {
        Cupones_MisCuponesDetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellCupones_MisCuponesCanjeoCodigoQR"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    else if ( indexPath.section == 2 )
    {
        Cupones_MisCuponesDetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellCupones_MisCuponesCanjeoCodigoEAN"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }

    return nil;
}

@end