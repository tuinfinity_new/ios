//
//  Cupones_MisCuponesDetalleVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 19/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"
#import "UIAlertView+Blocks.h"
#import "UIImageView+WebCache.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"
#import "MapViewAnnotation.h"
#import "UIImage_Misc.h"

#import "Cupones_MisCuponesDetalleCell.h"

#import "Empresas_ComoLlegarVC.h"
#import "Cupones_MisCuponesCanjeoVC.h"

#import "Cupones_MisCuponesDetalleVC.h"

@interface Cupones_MisCuponesDetalleVC ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) Cupones_MisCuponesDetalleCell *prototypeCellCupon1;
@property (nonatomic, strong) Cupones_MisCuponesDetalleCell *prototypeCellCupon2;

@property (nonatomic, weak) IBOutlet UIImageView *imgFinOferta;
@property (nonatomic, weak) IBOutlet UILabel *lblFinOferta;

@property (nonatomic, weak) IBOutlet UILabel *lblPrecio;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecioOld;

@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaRegalo;

@property (nonatomic, weak) IBOutlet UIView *viewEstado0;
@property (nonatomic, weak) IBOutlet UIView *viewEstado1;
@property (nonatomic, weak) IBOutlet UIView *viewEstado2;

@property (nonatomic, weak) IBOutlet UILabel *lblEstado1;
@property (nonatomic, weak) IBOutlet UILabel *lblEstado2;

@end

@implementation Cupones_MisCuponesDetalleVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    [self configuraNavigationBar];
    
    [_lblEstado1 setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:16]];
    [_lblEstado2 setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:16]];

    if ( [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"max_unidades"] intValue] > 0 )
    {
        _imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Cantidad"];
        _lblFinOferta.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_UNIDADESDISPONIBLES", @""), ([[[_itemCupon objectForKey:@"oferta"] objectForKey:@"max_unidades"] intValue] - [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"unidades_vendidas"] intValue])];
    }
    else
    {
        NSDateFormatter *formatoFecha = [[NSDateFormatter alloc] init];
        [formatoFecha setDateFormat:@"dd MM yyyy HH:mm:ss"];
        
        _imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Caducidad"];
        _lblFinOferta.text = [Globales showDateDifferenceBetween:[NSDate date] and:[formatoFecha dateFromString:[[_itemCupon objectForKey:@"oferta"] objectForKey:@"fecha_caducidad"]]];
    }
    
    if ( [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio"] doubleValue] - floor([[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio"] doubleValue]) == 0.0 )
        _lblPrecio.text = [NSString stringWithFormat:@"%d€", [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio"] intValue]];
    else
        _lblPrecio.text = [NSString stringWithFormat:@"%.2f€", [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio"] doubleValue]];
    
    NSDictionary *attributedStringSettings = @{ NSStrikethroughStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle] };
    
    if ( [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio_anterior"] doubleValue] - floor([[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio_anterior"] doubleValue]) == 0.0 )
        _lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d€ ", [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio_anterior"] intValue]] attributes:attributedStringSettings];
    else
        _lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f€", [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio_anterior"] doubleValue]] attributes:attributedStringSettings];
    
    switch ( [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"tipo"] intValue] )
    {
        case 0:
            _imgOfertaRegalo.hidden = YES;
            break;
        case 1:
            _imgOfertaRegalo.hidden = YES;
            break;
        case 2:
            _imgOfertaRegalo.hidden = YES;
            break;
        case 3:
            _imgOfertaRegalo.hidden = YES;
            break;
        case 4:
            _lblPrecio.hidden = YES;
            _lblPrecioOld.hidden = YES;
            break;
    }

    if ( [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"id_logo"] != nil )
    {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
        dispatch_async(queue, ^{
            UIImage *imgFoto = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"id_logo"] objectForKey:@"foto_medium"]]]];
            dispatch_sync(dispatch_get_main_queue(), ^{
                UIImage *imgFotoLogo = imgFoto;
                UIImage *imgFotoMapa = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[UIImage imageWithImage:imgFoto scaledToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
                
                [_itemImagen setObject:UIImagePNGRepresentation(imgFotoLogo) forKey:@"fotoImgLogo"];
                [_itemImagen setObject:UIImagePNGRepresentation(imgFotoMapa) forKey:@"fotoImgMapa"];
                
                [_tableView beginUpdates];
                [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:1]] withRowAnimation:UITableViewRowAnimationNone];
                [_tableView endUpdates];
            });
        });
    }

    _viewEstado0.hidden = YES;
    _viewEstado1.hidden = YES;
    _viewEstado2.hidden = YES;
    switch ( _estadoCupon )
    {
        case 0: _viewEstado0.hidden = NO; break;
        case 1: _viewEstado1.hidden = NO; break;
        case 2: _viewEstado2.hidden = NO; break;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Prototype cells

- (Cupones_MisCuponesDetalleCell *)prototypeCellCupon1
{
    if ( !_prototypeCellCupon1 )
        _prototypeCellCupon1 = [_tableView dequeueReusableCellWithIdentifier:@"CellCupones_MisCuponesDetalle1"];
    return _prototypeCellCupon1;
}

- (Cupones_MisCuponesDetalleCell *)prototypeCellCupon2
{
    if ( !_prototypeCellCupon2 )
        _prototypeCellCupon2 = [_tableView dequeueReusableCellWithIdentifier:@"CellCupones_MisCuponesDetalle2"];
    return _prototypeCellCupon2;
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnRuta_Pressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@""
                                message:NSLocalizedString(@"OPCION_PROXIMAMENTEDISPONIBLE", @"")
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

- (IBAction)btnComoLlegar_Pressed:(id)sender
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Empresas_ComoLlegarVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Empresas_ComoLlegar"];
    [pantallaDestino setLatitud:[[[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue]];
    [pantallaDestino setLongitud:[[[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue]];
    [pantallaDestino setItemImagen:_itemImagen];
    [self.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnCanjearCupon_Pressed:(id)sender
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;

    Cupones_MisCuponesCanjeoVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Cupones_MisCuponesCanjeo"];
    
    [pantallaDestino setItemCupon:_itemCupon];
    [self.navigationController pushViewController:pantallaDestino animated:NO];
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Cupones_MisCuponesDetalleCell class]] )
    {
        Cupones_MisCuponesDetalleCell *cellToConfigure = (Cupones_MisCuponesDetalleCell *)cell;

        cellToConfigure.imgLogo.layer.borderColor = [COLOR_AZUL1 CGColor];
        
        cellToConfigure.lblTituloDescripcion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
        cellToConfigure.lblTituloCondiciones.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
        cellToConfigure.lblTituloLocalizacion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
        
        cellToConfigure.lblNombre.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:20];
        cellToConfigure.lblDescripcion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
        cellToConfigure.lblCondiciones.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
        cellToConfigure.lblDireccion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
        cellToConfigure.lblBtnComoLlegar.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:12];
        cellToConfigure.lblBtnRuta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];

        cellToConfigure.imgOfertaExclusiva.hidden = YES;
        cellToConfigure.imgUltimaHora.hidden = YES;
        cellToConfigure.lblUltimaHora.hidden = YES;
        
        cellToConfigure.lblTitulo.text = [[_itemCupon objectForKey:@"oferta"] objectForKey:@"titulo"];
        
        if ( [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio"] doubleValue] - floor([[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio"] doubleValue]) == 0.0 )
            cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%d€", [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio"] intValue]];
        else
            cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%.2f€", [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio"] doubleValue]];
        
        NSDictionary *attributedStringSettings = @{ NSStrikethroughStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle] };
        
        if ( [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio_anterior"] doubleValue] - floor([[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio_anterior"] doubleValue]) == 0.0 )
            cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d€ ", [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio_anterior"] intValue]] attributes:attributedStringSettings];
        else
            cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f€", [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"precio_anterior"] doubleValue]] attributes:attributedStringSettings];
        
        cellToConfigure.imgUltimaHora.hidden = ![[[_itemCupon objectForKey:@"oferta"] objectForKey:@"isFlash"] boolValue];
        cellToConfigure.lblUltimaHora.hidden = ![[[_itemCupon objectForKey:@"oferta"] objectForKey:@"isFlash"] boolValue];

        NSString *descripcionTipo;
        switch ( [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"tipo"] intValue] )
        {
            case 0:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO0", ""), [[_itemCupon objectForKey:@"oferta"] objectForKey:@"param1"], [[_itemCupon objectForKey:@"oferta"] objectForKey:@"param2"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 1:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO1", ""), [[_itemCupon objectForKey:@"oferta"] objectForKey:@"param1"], [[_itemCupon objectForKey:@"oferta"] objectForKey:@"param2"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 2:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO2", ""), [[_itemCupon objectForKey:@"oferta"] objectForKey:@"param1"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 3:
                descripcionTipo = @"";
                cellToConfigure.imgDescripcionTipo.hidden = YES;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 4:
                descripcionTipo = @"";
                cellToConfigure.imgDescripcionTipo.hidden = YES;
                cellToConfigure.lblPrecio.hidden = YES;
                cellToConfigure.lblPrecioOld.hidden = YES;
                cellToConfigure.imgOfertaRegalo.hidden = NO;
                break;
        }
        cellToConfigure.lblDescripcionTipo.text = descripcionTipo;
        
        CLLocationDistance distance = [_datosAplicacion.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[[[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue] longitude:[[[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue]]];
        
        if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
            cellToConfigure.lblDistancia.text = ( (long)distance < 1000L ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (long)distance] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), distance / 1000.0];
        else
            cellToConfigure.lblDistancia.text = ( distance < POINT_ONE_MILE_METERS ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), distance * METERS_TO_FEET] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), distance * METERS_TO_MILES];
        
        cellToConfigure.imgBtnRuta.image = [UIImage imageNamed:@"Btn_Ruta_Add"];
        cellToConfigure.lblBtnRuta.text = NSLocalizedString(@"RUTA_ADD", @"");
        
        NSDateFormatter *formatoFecha1 = [[NSDateFormatter alloc] init];
        NSDateFormatter *formatoFecha2 = [[NSDateFormatter alloc] init];
        NSDateFormatter *formatoFecha3 = [[NSDateFormatter alloc] init];
        [formatoFecha1 setDateFormat:@"dd MM yyyy HH:mm:ss"];
        [formatoFecha2 setDateStyle:NSDateFormatterMediumStyle];
        [formatoFecha3 setDateFormat:@"HH:mm"];
        
        cellToConfigure.lblDisponibilidad1.text = NSLocalizedString(@"CUPONES_FECHACANJEO", @"");
        cellToConfigure.lblDisponibilidad2.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_FECHA", @""), [formatoFecha2 stringFromDate:[formatoFecha1 dateFromString:[_itemCupon objectForKey:@"fechaCaducidad"]]], [formatoFecha3 stringFromDate:[formatoFecha1 dateFromString:[_itemCupon objectForKey:@"fechaCaducidad"]]]];

        cellToConfigure.lblDescripcion.text = [[_itemCupon objectForKey:@"oferta"] objectForKey:@"descripcion"];
        cellToConfigure.lblCondiciones.text = [[_itemCupon objectForKey:@"oferta"] objectForKey:@"condiciones"];
        
        cellToConfigure.lblNombre.text = [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"nombre"];
        
        if ( [_itemImagen objectForKey:@"fotoImgLogo"] != [NSNull null] )
            cellToConfigure.imgLogo.image = [UIImage imageWithData:[_itemImagen objectForKey:@"fotoImgLogo"]];

        if ( [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"hasWifi"] == nil )
            cellToConfigure.imgWifi.hidden = YES;
        else
            cellToConfigure.imgWifi.hidden = [[[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"hasWifi"] boolValue];
        
        [cellToConfigure.mapView removeAnnotations:cellToConfigure.mapView.annotations];
        MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:@""
                                                                   andCoordinate:CLLocationCoordinate2DMake([[[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue], [[[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue])];
        annotation.tag = [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"id_empresa"] intValue];
        annotation.imagePinData = [_itemImagen objectForKey:@"fotoImgMapa"];
        [cellToConfigure.mapView addAnnotation:annotation];
        
        [cellToConfigure.mapView setRegion:[cellToConfigure.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake([[[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue], [[[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue]), 1500, 1500)]];
        
        cellToConfigure.lblDireccion.text = [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"empresa"] objectForKey:@"direccion"];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        [self configureCell:self.prototypeCellCupon1 inTableView:tableView forRowAtIndexPath:indexPath];
        
        _prototypeCellCupon1.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellCupon1.bounds));
        [_prototypeCellCupon1 layoutIfNeeded];
        
        CGSize size = [_prototypeCellCupon1.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1;
    }
    else if ( indexPath.section == 1 )
    {
        [self configureCell:self.prototypeCellCupon2 inTableView:tableView forRowAtIndexPath:indexPath];
        
        _prototypeCellCupon2.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellCupon2.bounds));
        [_prototypeCellCupon2 layoutIfNeeded];
        
        CGSize size = [_prototypeCellCupon2.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        Cupones_MisCuponesDetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellCupones_MisCuponesDetalle1"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        
        if ( [_itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
            cell.imgFoto.image = [UIImage imageWithData:[_itemImagen objectForKey:@"fotoImgLista"]];
        else
        {
            cell.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
        
            NSString *urlImagenComprimida = [[NSString stringWithFormat:@"https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=%d&refresh=31536000&url=%@", (int)self.view.frame.size.width, [[[_itemCupon objectForKey:@"oferta"] objectForKey:@"foto"] objectForKey:@"foto_medium"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [cell.activityIndicator startAnimating];
            [cell.imgFoto sd_setImageWithURL:[NSURL URLWithString:urlImagenComprimida]
                                   completed:^(UIImage *downloadedImage, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                       [cell.activityIndicator stopAnimating];
                                       [_itemImagen setValue:UIImagePNGRepresentation(downloadedImage) forKey:@"fotoImgLista"];
                                   }];
        }
        return cell;
    }
    else if ( indexPath.section == 1 )
    {
        Cupones_MisCuponesDetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellCupones_MisCuponesDetalle2"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    
    return nil;
}

#pragma mark -
#pragma mark MapView

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ( [annotation isKindOfClass:[MapViewAnnotation class]] )
    {
        MapViewAnnotation *mapAnnotation = annotation;
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
        
        if ( !pinView )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
            
            pinView.animatesDrop = NO;
            pinView.canShowCallout = NO;
            pinView.draggable = NO;
            if ( [_itemImagen objectForKey:@"fotoImgMapa"] != [NSNull null] )
                pinView.image = [UIImage imageWithData:mapAnnotation.imagePinData];
            else
                pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[[UIImage imageNamed:@"Img_NoFotoEmpresa"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
        }
        else pinView.annotation = annotation;
        
        return pinView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
}

@end