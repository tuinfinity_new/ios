//
//  Cupones_MisCuponesListadoVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 21/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CuponesVC.h"

@interface Cupones_MisCuponesListadoVC : UIViewController

@property (nonatomic, weak) IBOutlet CuponesVC *originView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *listadoDatos;
@property (nonatomic, strong) NSMutableArray *listadoDatos_Imagenes;

@property (nonatomic, readwrite) BOOL reloadDataAtAppear;

@end
