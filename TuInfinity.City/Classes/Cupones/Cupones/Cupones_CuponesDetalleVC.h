//
//  Cupones_CuponesDetalleVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 18/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Cupones_CuponesListadoVC.h"
#import "Empresas_DetalleVC.h"

@interface Cupones_CuponesDetalleVC : UIViewController

@property (nonatomic, strong) NSMutableDictionary *itemOferta;
@property (nonatomic, strong) NSMutableDictionary *itemImagen;
@property (nonatomic, strong) NSMutableDictionary *itemImagenEmpresa;

@property (nonatomic, readwrite) int typeOriginView;

@property (nonatomic, weak) IBOutlet Cupones_CuponesListadoVC *originViewCupones;
@property (nonatomic, weak) IBOutlet Empresas_DetalleVC *originViewEmpresas;

@end