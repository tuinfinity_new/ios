//
//  Cupones_CuponesDetalleVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 18/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import <Social/Social.h>

#import "AFNetworking.h"
#import "UIAlertView+Blocks.h"
#import "UIImage_Blur.h"
#import "UIImageView+WebCache.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"
#import "MapViewAnnotation.h"
#import "UIImage_Misc.h"

#import "Cupones_CuponesDetalleCell.h"

#import "Empresas_ComoLlegarVC.h"

#import "Cupones_CuponesDetalleVC.h"

@interface Cupones_CuponesDetalleVC () <MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) Cupones_CuponesDetalleCell *prototypeCellCupon1;
@property (nonatomic, strong) Cupones_CuponesDetalleCell *prototypeCellCupon2;

@property (nonatomic, weak) IBOutlet UIImageView *imgFinOferta;
@property (nonatomic, weak) IBOutlet UILabel *lblFinOferta;

@property (nonatomic, weak) IBOutlet UILabel *lblPrecio;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecioOld;

@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaRegalo;

@property (nonatomic, weak) IBOutlet UIImageView *imgBlur;
@property (nonatomic, weak) IBOutlet UIImageView *imgFondo;

@property (nonatomic, weak) IBOutlet UIView *viewMenuCompartir;
@property (nonatomic, weak) IBOutlet UIView *viewOpcionesCupon;

@end

@implementation Cupones_CuponesDetalleVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    [self configuraNavigationBar];
    [self menuCompartirVisible:NO];

    if ( [[_itemOferta objectForKey:@"max_unidades"] intValue] > 0 )
    {
        _imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Cantidad"];
        _lblFinOferta.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_UNIDADESDISPONIBLES", @""), ([[_itemOferta objectForKey:@"max_unidades"] intValue] - [[_itemOferta objectForKey:@"unidades_vendidas"] intValue])];
    }
    else
    {
        NSDateFormatter *formatoFecha = [[NSDateFormatter alloc] init];
        [formatoFecha setDateFormat:@"dd MM yyyy HH:mm:ss"];
        
        _imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Caducidad"];
        _lblFinOferta.text = [Globales showDateDifferenceBetween:[NSDate date] and:[formatoFecha dateFromString:[_itemOferta objectForKey:@"fecha_caducidad"]]];
    }
    
    if ( [[_itemOferta objectForKey:@"precio"] doubleValue] - floor([[_itemOferta objectForKey:@"precio"] doubleValue]) == 0.0 )
        _lblPrecio.text = [NSString stringWithFormat:@"%d€", [[_itemOferta objectForKey:@"precio"] intValue]];
    else
        _lblPrecio.text = [NSString stringWithFormat:@"%.2f€", [[_itemOferta objectForKey:@"precio"] doubleValue]];
    
    NSDictionary *attributedStringSettings = @{ NSStrikethroughStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle] };
    
    if ( [[_itemOferta objectForKey:@"precio_anterior"] doubleValue] - floor([[_itemOferta objectForKey:@"precio_anterior"] doubleValue]) == 0.0 )
        _lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d€ ", [[_itemOferta objectForKey:@"precio_anterior"] intValue]] attributes:attributedStringSettings];
    else
        _lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f€", [[_itemOferta objectForKey:@"precio_anterior"] doubleValue]] attributes:attributedStringSettings];

    switch ( [[_itemOferta objectForKey:@"tipo"] intValue] )
    {
        case 0:
            _imgOfertaRegalo.hidden = YES;
            break;
        case 1:
            _imgOfertaRegalo.hidden = YES;
            break;
        case 2:
            _imgOfertaRegalo.hidden = YES;
            break;
        case 3:
            _imgOfertaRegalo.hidden = YES;
            break;
        case 4:
            _lblPrecio.hidden = YES;
            _lblPrecioOld.hidden = YES;
            break;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Prototype cells

- (Cupones_CuponesDetalleCell *)prototypeCellCupon1
{
    if ( !_prototypeCellCupon1 )
        _prototypeCellCupon1 = [_tableView dequeueReusableCellWithIdentifier:@"CellCupones_CuponesDetalle1"];
    return _prototypeCellCupon1;
}

- (Cupones_CuponesDetalleCell *)prototypeCellCupon2
{
    if ( !_prototypeCellCupon2 )
        _prototypeCellCupon2 = [_tableView dequeueReusableCellWithIdentifier:@"CellCupones_CuponesDetalle2"];
    return _prototypeCellCupon2;
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - Misc

- (void)menuCompartirVisible:(BOOL)stateVisible
{
    _tableView.userInteractionEnabled = !stateVisible;
    _viewOpcionesCupon.userInteractionEnabled = !stateVisible;
    
    _imgBlur.hidden = !stateVisible;
    _imgFondo.hidden = !stateVisible;
    _viewMenuCompartir.hidden = !stateVisible;
    _viewMenuCompartir.userInteractionEnabled = stateVisible;
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnComoLlegar_Pressed:(id)sender
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Empresas_ComoLlegarVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Empresas_ComoLlegar"];
    [pantallaDestino setLatitud:[[[_itemOferta objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue]];
    [pantallaDestino setLongitud:[[[_itemOferta objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue]];
    [pantallaDestino setItemImagen:_itemImagen];
    [self.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnAdquirirCupon_Pressed:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [[userDefaults objectForKey:@"usrInvitado"] boolValue] )
        [UIAlertView showConfirmationDialogWithTitle:@""
                                             message:NSLocalizedString(@"OPCION_REQUERIDOLOGIN", @"")
                                        buttonCancel:NSLocalizedString(@"OPC_MASTARDE", @"")
                                            buttonOk:NSLocalizedString(@"OPC_LOGIN", @"")
                                             handler:^(UIAlertView *alertView, NSInteger buttonIndex){
                                                 if ( buttonIndex == 1 )
                                                 {
                                                     UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
                                                     [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Logout"] animated:NO];
                                                 }
                                             }];
    else
    {
        if ( ( [[_itemOferta objectForKey:@"limite_cupones"] intValue] > 0 ) && !( [[_itemOferta objectForKey:@"limite_cupones"] intValue] > [[_itemOferta objectForKey:@"veces_adquirido"] intValue] ) )
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:NSLocalizedString(@"CUPONES_CUPONAGOTADO", @"")
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        else
        {
            [_activityIndicator startAnimating];
            
            NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
            
            AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
            [operationManager.securityPolicy setAllowInvalidCertificates:YES];
            [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
            [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
            [operationManager POST:[NSString stringWithFormat:@"oferta/%d/adquirir", [[_itemOferta objectForKey:@"id_oferta"] intValue]]
                        parameters:nil
                           success:^(AFHTTPRequestOperation *operation, id responseObject){
                               NSMutableDictionary *receivedData = [[NSJSONSerialization JSONObjectWithData:[[operation responseString] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] mutableCopy];
                               
                               int numRow;
                               switch ( _typeOriginView )
                               {
                                   case PANTALLAORIGEN_CUPONESLISTADO:
                                       _originViewCupones.reloadDataAtAppear = YES;
                                       break;
                                   case PANTALLAORIGEN_EMPRESASDETALLE:
                                       numRow = (int)[_originViewEmpresas.listadoDatos_Cupones indexOfObject:_itemOferta];
                                       [[_originViewEmpresas.listadoDatos_Cupones objectAtIndex:numRow] setValue:[NSNumber numberWithBool:YES] forKeyPath:@"cupon_adquirido"];
                                       [[_originViewEmpresas.listadoDatos_Cupones objectAtIndex:numRow] setValue:[[receivedData objectForKey:@"oferta"] objectForKey:@"cupon"] forKey:@"cupon"];
                                       [[_originViewEmpresas.listadoDatos_Cupones objectAtIndex:numRow] setValue:[NSNumber numberWithInt:[[[_originViewEmpresas.listadoDatos_Cupones objectAtIndex:numRow] objectForKey:@"veces_adquirido"] intValue] + 1] forKey:@"veces_adquirido"];
                                       if ( [[[_originViewEmpresas.listadoDatos_Cupones objectAtIndex:numRow] objectForKey:@"max_unidades"] intValue] > 0 )
                                           [[_originViewEmpresas.listadoDatos_Cupones objectAtIndex:numRow] setValue:[NSNumber numberWithInt:[[[_originViewEmpresas.listadoDatos_Cupones objectAtIndex:numRow] objectForKey:@"unidades_vendidas"] intValue] + 1] forKey:@"unidades_vendidas"];
                                       break;
                               }
                               [_activityIndicator stopAnimating];
                               [UIAlertView showWithTitle:@""
                                                  message:NSLocalizedString(@"CUPONES_CUPONGUARDADO", @"")
                                                  handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                      [self.navigationController popViewControllerAnimated:NO];
                                                  }];
                           }
                           failure:^(AFHTTPRequestOperation *operation, NSError *error){
                               NSLog(@"%@",[error description]);
                               [_activityIndicator stopAnimating];
                           }];
        }
    }
}

- (IBAction)btnRuta_Pressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@""
                                message:NSLocalizedString(@"OPCION_PROXIMAMENTEDISPONIBLE", @"")
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

- (IBAction)btnCompartir_Pressed:(id)sender
{
    UIImage *screenShotImage = [self.view screenshotWitWidth:self.view.frame.size.width andHeight:self.view.frame.size.height];
    UIImage *blurredImage = [screenShotImage blurredImageWithRadius:10.0f iterations:5 tintColor:nil];
    
    _imgBlur.image = blurredImage;

    [self menuCompartirVisible:YES];
}

- (IBAction)btnCompartir_Mail_Pressed:(id)sender
{
    if ( ![MFMailComposeViewController canSendMail] )
        [UIAlertView showWithTitle:@""
                           message:NSLocalizedString(@"COMPARTIR_MAIL_ERROR", @"")
                           handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                               [self menuCompartirVisible:NO];
                           }];
    else
    {
        MFMailComposeViewController *mailView = [[MFMailComposeViewController alloc] init];
        
        mailView.mailComposeDelegate = self;
        
        [mailView setSubject:NSLocalizedString(@"COMPARTIR_MAIL_OFERTA_TITULO", "")];
        
        NSString *mailBody = [NSString stringWithFormat:
                              @"<html>"
                                "<head>"
                                  "<style type=\"text/css\">"
                                    "body { font-family:Helvetica; font-size:14px; color:#000000; background-color:#FFFFFF;}"
                                  "</style>"
                                "</head>"
                                "<body>"
                                  "%@"
                                  "<br><br>"
                                  "<a href='%@'>%@</a>"
                                  "<br><br>"
                                  "%@"
                                "</body>"
                              "</html>",
                              NSLocalizedString(@"COMPARTIR_MAIL_OFERTA_MENSAJE1", ""),
                              [NSString stringWithFormat:@"%@%@",URL_SERVIDOR_OFERTA, [_itemOferta objectForKey:@"id_oferta"]], [_itemOferta objectForKey:@"titulo"],
                              NSLocalizedString(@"COMPARTIR_MAIL_OFERTA_MENSAJE2", "")];
        [mailView setMessageBody:mailBody isHTML:YES];
        
        [self presentViewController:mailView animated:YES completion:nil];
    }
}

- (IBAction)btnCompartir_Facebook_Pressed:(id)sender
{
    if ( ![SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook] )
        [UIAlertView showWithTitle:@""
                           message:NSLocalizedString(@"COMPARTIR_FACEBOOK_ERROR", @"")
                           handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                               [self menuCompartirVisible:NO];
                           }];
    else
    {
        NSString *urlDestino = [NSString stringWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@", [NSString stringWithFormat:@"%@%@",URL_SERVIDOR_OFERTA, [_itemOferta objectForKey:@"id_oferta"]]]]
                                                        encoding:NSUTF8StringEncoding
                                                           error:nil];

        SLComposeViewController *composeView = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [composeView setInitialText:[NSString stringWithFormat:NSLocalizedString(@"COMPARTIR_FACEBOOK_OFERTA_MENSAJE", ""), [_itemOferta objectForKey:@"titulo"]]];
        [composeView addURL:[NSURL URLWithString:urlDestino]];
        if ( [_itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
            [composeView addImage:[UIImage imageWithData:[_itemImagen objectForKey:@"fotoImgLista"]]];
        [self presentViewController:composeView animated:YES completion:nil];
    }
    
    [self menuCompartirVisible:NO];
}

- (IBAction)btnCompartir_Twitter_Pressed:(id)sender
{
    if ( ![SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter] )
        [UIAlertView showWithTitle:@""
                           message:NSLocalizedString(@"COMPARTIR_TWITTER_ERROR", @"")
                           handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                               [self menuCompartirVisible:NO];
                           }];
    else
    {
        NSString *urlDestino = [NSString stringWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://tinyurl.com/api-create.php?url=%@", [NSString stringWithFormat:@"%@%@",URL_SERVIDOR_OFERTA, [_itemOferta objectForKey:@"id_oferta"]]]]
                                                        encoding:NSUTF8StringEncoding
                                                           error:nil];

        SLComposeViewController *composeView = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [composeView setInitialText:[NSString stringWithFormat:NSLocalizedString(@"COMPARTIR_TWITTER_OFERTA_MENSAJE", ""), urlDestino]];
        if ( [_itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
            [composeView addImage:[UIImage imageWithData:[_itemImagen objectForKey:@"fotoImgLista"]]];
        
        [self presentViewController:composeView animated:YES completion:nil];
    }
    [self menuCompartirVisible:NO];
}

- (IBAction)btnCompartir_Cancelar_Pressed:(id)sender
{
    [self menuCompartirVisible:NO];
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Cupones_CuponesDetalleCell class]] )
    {
        if ( indexPath.section == 0 )
        {
            Cupones_CuponesDetalleCell *cellToConfigure = (Cupones_CuponesDetalleCell *)cell;
            
            cellToConfigure.lblBtnRuta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];
            
            cellToConfigure.imgOfertaExclusiva.hidden = YES;
            cellToConfigure.imgUltimaHora.hidden = YES;
            cellToConfigure.lblUltimaHora.hidden = YES;

            cellToConfigure.lblTitulo.text = [_itemOferta objectForKey:@"titulo"];
            
            if ( [[_itemOferta objectForKey:@"precio"] doubleValue] - floor([[_itemOferta objectForKey:@"precio"] doubleValue]) == 0.0 )
                cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%d€", [[_itemOferta objectForKey:@"precio"] intValue]];
            else
                cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%.2f€", [[_itemOferta objectForKey:@"precio"] doubleValue]];
            
            NSDictionary *attributedStringSettings = @{ NSStrikethroughStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle] };
            
            if ( [[_itemOferta objectForKey:@"precio_anterior"] doubleValue] - floor([[_itemOferta objectForKey:@"precio_anterior"] doubleValue]) == 0.0 )
                cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d€ ", [[_itemOferta objectForKey:@"precio_anterior"] intValue]] attributes:attributedStringSettings];
            else
                cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f€", [[_itemOferta objectForKey:@"precio_anterior"] doubleValue]] attributes:attributedStringSettings];
            
            cellToConfigure.imgUltimaHora.hidden = ![[_itemOferta objectForKey:@"isFlash"] boolValue];
            cellToConfigure.lblUltimaHora.hidden = ![[_itemOferta objectForKey:@"isFlash"] boolValue];
            
            NSString *descripcionTipo;
            switch ( [[_itemOferta objectForKey:@"tipo"] intValue] )
            {
                case 0:
                    descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO0", ""), [_itemOferta objectForKey:@"param1"], [_itemOferta objectForKey:@"param2"]];
                    cellToConfigure.imgDescripcionTipo.hidden = NO;
                    cellToConfigure.lblPrecio.hidden = NO;
                    cellToConfigure.lblPrecioOld.hidden = NO;
                    cellToConfigure.imgOfertaRegalo.hidden = YES;
                    break;
                case 1:
                    descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO1", ""), [_itemOferta objectForKey:@"param1"], [_itemOferta objectForKey:@"param2"]];
                    cellToConfigure.imgDescripcionTipo.hidden = NO;
                    cellToConfigure.lblPrecio.hidden = NO;
                    cellToConfigure.lblPrecioOld.hidden = NO;
                    cellToConfigure.imgOfertaRegalo.hidden = YES;
                    break;
                case 2:
                    descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO2", ""), [_itemOferta objectForKey:@"param1"]];
                    cellToConfigure.imgDescripcionTipo.hidden = NO;
                    cellToConfigure.lblPrecio.hidden = NO;
                    cellToConfigure.lblPrecioOld.hidden = NO;
                    cellToConfigure.imgOfertaRegalo.hidden = YES;
                    break;
                case 3:
                    descripcionTipo = @"";
                    cellToConfigure.imgDescripcionTipo.hidden = YES;
                    cellToConfigure.lblPrecio.hidden = NO;
                    cellToConfigure.lblPrecioOld.hidden = NO;
                    cellToConfigure.imgOfertaRegalo.hidden = YES;
                    break;
                case 4:
                    descripcionTipo = @"";
                    cellToConfigure.imgDescripcionTipo.hidden = YES;
                    cellToConfigure.lblPrecio.hidden = YES;
                    cellToConfigure.lblPrecioOld.hidden = YES;
                    cellToConfigure.imgOfertaRegalo.hidden = NO;
                    break;
            }
            cellToConfigure.lblDescripcionTipo.text = descripcionTipo;
            CLLocationDistance distance = [_datosAplicacion.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[[[_itemOferta objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue] longitude:[[[_itemOferta objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue]]];
            
            if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
                cellToConfigure.lblDistancia.text = ( (long)distance < 1000L ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (long)distance] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), distance / 1000.0];
            else
                cellToConfigure.lblDistancia.text = ( distance < POINT_ONE_MILE_METERS ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), distance * METERS_TO_FEET] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), distance * METERS_TO_MILES];
            
            cellToConfigure.imgBtnRuta.image = [UIImage imageNamed:@"Btn_Ruta_Add"];
            cellToConfigure.lblBtnRuta.text = NSLocalizedString(@"RUTA_ADD", @"");
            
            NSDateFormatter *formatoFecha1 = [[NSDateFormatter alloc] init];
            NSDateFormatter *formatoFecha2 = [[NSDateFormatter alloc] init];
            NSDateFormatter *formatoFecha3 = [[NSDateFormatter alloc] init];
            [formatoFecha1 setDateFormat:@"dd MM yyyy HH:mm:ss"];
            [formatoFecha2 setDateStyle:NSDateFormatterMediumStyle];
            [formatoFecha3 setDateFormat:@"HH:mm"];
            
            if ( [[_itemOferta objectForKey:@"max_unidades"] intValue] > 0 )
            {
                cellToConfigure.imgDisponibilidad1.hidden = YES;
                cellToConfigure.imgDisponibilidad2.hidden = YES;
                cellToConfigure.lblDisponibilidad1.hidden = YES;
                cellToConfigure.lblDisponibilidad2.hidden = YES;
                
                cellToConfigure.lblDisponibilidad1.text = @"";
                cellToConfigure.lblDisponibilidad2.text = @"";
            }
            else
            {
                cellToConfigure.imgDisponibilidad1.hidden = NO;
                cellToConfigure.imgDisponibilidad2.hidden = NO;
                cellToConfigure.lblDisponibilidad1.hidden = NO;
                cellToConfigure.lblDisponibilidad2.hidden = NO;
                
                cellToConfigure.lblDisponibilidad1.text = NSLocalizedString(@"CUPONES_FECHACANJEO", @"");
                cellToConfigure.lblDisponibilidad2.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_FECHA", @""), [formatoFecha2 stringFromDate:[formatoFecha1 dateFromString:[_itemOferta objectForKey:@"fecha_caducidad"]]], [formatoFecha3 stringFromDate:[formatoFecha1 dateFromString:[_itemOferta objectForKey:@"fecha_caducidad"]]]];
            }
        }
        else if ( indexPath.section == 1 )
        {
            Cupones_CuponesDetalleCell *cellToConfigure = (Cupones_CuponesDetalleCell *)cell;

            cellToConfigure.imgLogo.layer.borderColor = [COLOR_AZUL1 CGColor];
            
            cellToConfigure.lblTituloDescripcion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
            cellToConfigure.lblTituloCondiciones.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
            cellToConfigure.lblTituloLocalizacion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
            
            cellToConfigure.lblNombre.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:20];
            cellToConfigure.lblDescripcion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
            cellToConfigure.lblCondiciones.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
            cellToConfigure.lblDireccion.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:13];
            cellToConfigure.lblBtnComoLlegar.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:12];

            cellToConfigure.lblDescripcion.text = [_itemOferta objectForKey:@"descripcion"];
            cellToConfigure.lblCondiciones.text = [_itemOferta objectForKey:@"condiciones"];
            
            cellToConfigure.lblNombre.text = [[_itemOferta objectForKey:@"empresa"] objectForKey:@"nombre"];
            
            if ( [[_itemOferta objectForKey:@"empresa"] objectForKey:@"hasWifi"] == nil )
                cellToConfigure.imgWifi.hidden = YES;
            else
                cellToConfigure.imgWifi.hidden = [[[_itemOferta objectForKey:@"empresa"] objectForKey:@"hasWifi"] boolValue];
            
            [cellToConfigure.mapView removeAnnotations:cellToConfigure.mapView.annotations];
            MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:@""
                                                                       andCoordinate:CLLocationCoordinate2DMake([[[_itemOferta objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue], [[[_itemOferta objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue])];
            annotation.tag = [[_itemOferta objectForKey:@"id_empresa"] intValue];
            annotation.imagePinData = [_itemImagenEmpresa objectForKey:@"fotoImgMapa"];
            [cellToConfigure.mapView addAnnotation:annotation];
            
            [cellToConfigure.mapView setRegion:[cellToConfigure.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake([[[_itemOferta objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue], [[[_itemOferta objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue]), 1500, 1500)]];
            
            cellToConfigure.lblDireccion.text = [[_itemOferta objectForKey:@"empresa"] objectForKey:@"direccion"];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        [self configureCell:self.prototypeCellCupon1 inTableView:tableView forRowAtIndexPath:indexPath];
        
        _prototypeCellCupon1.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellCupon1.bounds));
        [_prototypeCellCupon1 layoutIfNeeded];
        
        CGSize size = [_prototypeCellCupon1.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1;
    }
    else if ( indexPath.section == 1 )
    {
        [self configureCell:self.prototypeCellCupon2 inTableView:tableView forRowAtIndexPath:indexPath];
        
        _prototypeCellCupon2.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellCupon2.bounds));
        [_prototypeCellCupon2 layoutIfNeeded];
        
        CGSize size = [_prototypeCellCupon2.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        Cupones_CuponesDetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellCupones_CuponesDetalle1"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];

        if ( [_itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
            cell.imgFoto.image = [UIImage imageWithData:[_itemImagen objectForKey:@"fotoImgLista"]];
        else
        {
            cell.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
            
            NSString *urlImagenComprimida = [[NSString stringWithFormat:@"https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=%d&refresh=31536000&url=%@", (int)self.view.frame.size.width, [[_itemOferta objectForKey:@"foto"] objectForKey:@"foto_tmedium"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [cell.activityIndicator startAnimating];
            [cell.imgFoto sd_setImageWithURL:[NSURL URLWithString:urlImagenComprimida]
                                   completed:^(UIImage *downloadedImage, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                       [cell.activityIndicator stopAnimating];
                                       [_itemImagen setValue:UIImagePNGRepresentation(downloadedImage) forKey:@"fotoImgLista"];
                                   }];
        }
        
        return cell;
    }
    else if ( indexPath.section == 1 )
    {
        Cupones_CuponesDetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellCupones_CuponesDetalle2"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];

        if ( [_itemImagenEmpresa objectForKey:@"fotoImgLogo"] != [NSNull null] )
            cell.imgLogo.image = [UIImage imageWithData:[_itemImagenEmpresa objectForKey:@"fotoImgLogo"]];
        
        return cell;
    }
    
    return nil;
}

#pragma mark -
#pragma mark MapView

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ( [annotation isKindOfClass:[MapViewAnnotation class]] )
    {
        MapViewAnnotation *mapAnnotation = annotation;
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
        
        if ( !pinView )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
            
            pinView.animatesDrop = NO;
            pinView.canShowCallout = NO;
            pinView.draggable = NO;
            if ( [_itemImagenEmpresa objectForKey:@"fotoImgMapa"] != [NSNull null] )
                pinView.image = [UIImage imageWithData:mapAnnotation.imagePinData];
            else
                pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[[UIImage imageNamed:@"Img_NoFotoEmpresa"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
        }
        else pinView.annotation = annotation;
        
        return pinView;
    }
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate

- (void) mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch ( result )
	{
		case MFMailComposeResultCancelled:
            break;
		case MFMailComposeResultSaved:
            break;
		case MFMailComposeResultSent:
            break;
		case MFMailComposeResultFailed:
            break;
		default:
            break;
	}
    [self dismissViewControllerAnimated:YES completion:nil];
    [self menuCompartirVisible:NO];
}

@end
