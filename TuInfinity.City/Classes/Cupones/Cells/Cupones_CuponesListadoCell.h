//
//  Cupones_CuponesListadoCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 22/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cupones_CuponesListadoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *viewOferta;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;
@property (nonatomic, weak) IBOutlet UILabel *lblDescripcionTipo;
@property (nonatomic, weak) IBOutlet UILabel *lblFinOferta;
@property (nonatomic, weak) IBOutlet UILabel *lblDistancia;
@property (nonatomic, weak) IBOutlet UILabel *lblUltimaHora;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecio;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecioOld;

@property (nonatomic, weak) IBOutlet UIImageView *imgFoto;
@property (nonatomic, weak) IBOutlet UIImageView *imgDescripcionTipo;
@property (nonatomic, weak) IBOutlet UIImageView *imgUltimaHora;
@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaExclusiva;
@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaRegalo;
@property (nonatomic, weak) IBOutlet UIImageView *imgFinOferta;

@property (nonatomic, weak) IBOutlet UIButton *btnRuta;
@property (nonatomic, weak) IBOutlet UIButton *btnOferta;

@property (nonatomic, weak) IBOutlet UIImageView *imgBtnRuta;
@property (nonatomic, weak) IBOutlet UIImageView *imgBtnOferta;

@property (nonatomic, weak) IBOutlet UILabel *lblBtnRuta;
@property (nonatomic, weak) IBOutlet UILabel *lblBtnOferta;

@end