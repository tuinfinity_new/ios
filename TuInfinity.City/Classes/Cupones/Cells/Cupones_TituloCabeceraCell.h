//
//  Cupones_TituloCabeceraCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 14/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Cupones_TituloCabeceraCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;

@end
