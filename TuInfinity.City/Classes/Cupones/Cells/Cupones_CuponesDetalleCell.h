//
//  Cupones_CuponesDetalleCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 22/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@interface Cupones_CuponesDetalleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UILabel *lblTituloDescripcion;
@property (nonatomic, weak) IBOutlet UILabel *lblTituloCondiciones;
@property (nonatomic, weak) IBOutlet UILabel *lblTituloLocalizacion;

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;
@property (nonatomic, weak) IBOutlet UILabel *lblDescripcionTipo;
@property (nonatomic, weak) IBOutlet UILabel *lblUltimaHora;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecio;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecioOld;
@property (nonatomic, weak) IBOutlet UILabel *lblDistancia;
@property (nonatomic, weak) IBOutlet UILabel *lblDisponibilidad1;
@property (nonatomic, weak) IBOutlet UILabel *lblDisponibilidad2;
@property (nonatomic, weak) IBOutlet UILabel *lblDescripcion;
@property (nonatomic, weak) IBOutlet UILabel *lblCondiciones;
@property (nonatomic, weak) IBOutlet UILabel *lblNombre;
@property (nonatomic, weak) IBOutlet UILabel *lblDireccion;

@property (nonatomic, weak) IBOutlet UIImageView *imgFoto;
@property (nonatomic, weak) IBOutlet UIImageView *imgDescripcionTipo;
@property (nonatomic, weak) IBOutlet UIImageView *imgUltimaHora;
@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaExclusiva;
@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaRegalo;
@property (nonatomic, weak) IBOutlet UIImageView *imgDisponibilidad1;
@property (nonatomic, weak) IBOutlet UIImageView *imgDisponibilidad2;
@property (nonatomic, weak) IBOutlet UIImageView *imgLogo;
@property (nonatomic, weak) IBOutlet UIImageView *imgWifi;

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@property (nonatomic, weak) IBOutlet UIImageView *imgBtnRuta;

@property (nonatomic, weak) IBOutlet UILabel *lblBtnRuta;

@property (nonatomic, weak) IBOutlet UILabel *lblBtnComoLlegar;

@end