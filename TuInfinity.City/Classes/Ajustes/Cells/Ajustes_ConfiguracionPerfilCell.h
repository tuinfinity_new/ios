//
//  Ajustes_ConfiguracionPerfilCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 28/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ajustes_ConfiguracionPerfilCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UITextField *txfNombre;
@property (nonatomic, weak) IBOutlet UITextField *txfPassword1;
@property (nonatomic, weak) IBOutlet UITextField *txfPassword2;
@property (nonatomic, weak) IBOutlet UITextField *txfPasswordOriginal;

@end
