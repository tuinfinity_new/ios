//
//  Ajustes_ConfiguracionPerfilCell.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 28/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "Ajustes_ConfiguracionPerfilCell.h"

@implementation Ajustes_ConfiguracionPerfilCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
