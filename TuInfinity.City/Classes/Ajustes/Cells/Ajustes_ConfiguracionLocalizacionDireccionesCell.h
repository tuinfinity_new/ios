//
//  Ajustes_ConfiguracionLocalizacionDireccionesCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 28/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ajustes_ConfiguracionLocalizacionDireccionesCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblDescripcion;

@end
