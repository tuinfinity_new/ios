//
//  Ajustes_ConfiguracionLocalizacionCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 28/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@interface Ajustes_ConfiguracionLocalizacionCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UISlider *sldRadioBusqueda;
@property (nonatomic, weak) IBOutlet UILabel *lblRadioBusqueda;

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo1;
@property (nonatomic, weak) IBOutlet UILabel *lblTitulo2;

@property (nonatomic, weak) IBOutlet UISwitch *swtGeoLocalizacion;

@property (nonatomic, weak) IBOutlet UITextField *txfBusquedaPoblacion;

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@property (nonatomic, weak) IBOutlet UIButton *btnBusquedaPoblacion;
@property (nonatomic, weak) IBOutlet UIButton *btnGeoLocalizacion1;
@property (nonatomic, weak) IBOutlet UIButton *btnGeoLocalizacion2;

@property (nonatomic, weak) IBOutlet UITableView *tableViewDirecciones;

@end
