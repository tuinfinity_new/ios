//
//  Ajustes_LocalizacionVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 11/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "DatosAplicacionSingleton.h"
#import "MapViewAnnotation.h"
#import "UIImage_Misc.h"

#import "Ajustes_ConfiguracionLocalizacionDireccionesCell.h"

#import "Ajustes_LocalizacionVC.h"

@interface Ajustes_LocalizacionVC ()

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet UILabel *lblPoblacion;
@property (nonatomic, weak) IBOutlet UILabel *lblInformacion;

@property (nonatomic, weak) IBOutlet UISwitch *swtGeoLocalizacion;

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@property (nonatomic, weak) IBOutlet UIButton *btnGuardar;
@property (nonatomic, weak) IBOutlet UIButton *btnBusquedaPoblacion;

@property (nonatomic, weak) IBOutlet UITextField *txfBusquedaPoblacion;

@property (nonatomic, weak) IBOutlet UITableView *tableViewDirecciones;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longPressGesture;

@property (nonatomic, strong) NSMutableArray *listadoDatos_Direcciones;

@end

@implementation Ajustes_LocalizacionVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];

    _listadoDatos_Direcciones = [NSMutableArray array];

    _btnGuardar.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
    _btnBusquedaPoblacion.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];

    [_tableViewDirecciones setHidden:YES];

    _lblPoblacion.text = _datosAplicacion.poblacion;

    [_swtGeoLocalizacion setOn:_datosAplicacion.geolocalizacionActiva];
    
    _longPressGesture.enabled = ![_swtGeoLocalizacion isOn];
    _txfBusquedaPoblacion.enabled = ![_swtGeoLocalizacion isOn];
    _btnBusquedaPoblacion.enabled = ![_swtGeoLocalizacion isOn];
    _btnGuardar.enabled = ![_swtGeoLocalizacion isOn];

    [_lblInformacion setText:( [_swtGeoLocalizacion isOn] ) ? NSLocalizedString(@"AJUSTES_GEOLOCALIZACION_ON", @"") : NSLocalizedString(@"AJUSTES_GEOLOCALIZACION_OFF", @"")];
    
    if ( [_swtGeoLocalizacion isOn] )
    {
        [_mapView removeAnnotations:_mapView.annotations];
        _mapView.showsUserLocation = YES;
        [_mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
        
        if ( ( _latitud != 0.0 ) && ( _longitud != 0.0 ) )
            [_mapView setRegion:[_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(_latitud, _longitud), 1000, 1000)] animated:YES];
    }
    else
    {
        [_mapView removeAnnotations:_mapView.annotations];
        _mapView.showsUserLocation = NO;
        [_mapView setUserTrackingMode:MKUserTrackingModeNone];
        [self addAnotacion:CLLocationCoordinate2DMake(_latitud, _longitud)];
        [self calculateAdress:CLLocationCoordinate2DMake(_latitud, _longitud)];
    }

    [self configuraNavigationBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Misc

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnGuardar_Pressed:(id)sender
{
    [self.view endEditing:YES];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    if ( !_swtGeoLocalizacion.isOn )
    {
        if ( [_mapView.annotations count] > 0 )
        {
            MapViewAnnotation *annotation = [_mapView.annotations objectAtIndex:0];
            
            _datosAplicacion.latitud = annotation.coordinate.latitude;
            _datosAplicacion.longitud = annotation.coordinate.longitude;
            _datosAplicacion.poblacion = _lblPoblacion.text;
            
            [userDefaults setValue:[NSNumber numberWithDouble:_latitud] forKey:@"locLatitud"];
            [userDefaults setValue:[NSNumber numberWithDouble:_longitud] forKey:@"locLongitud"];
        }
    }
    
    [userDefaults setValue:[NSNumber numberWithBool:_swtGeoLocalizacion.isOn] forKeyPath:@"gelocalizacionActiva"];
    [userDefaults synchronize];
    
    _datosAplicacion.geolocalizacionActiva = _swtGeoLocalizacion.isOn;

    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnBusquedaPoblacion_Pressed:(id)sender
{
    [self.view endEditing:YES];
    
    _tableViewDirecciones.hidden = YES;
    [_listadoDatos_Direcciones removeAllObjects];
    
    MKLocalSearchRequest *localSearchRequest = [[MKLocalSearchRequest alloc] init];
    localSearchRequest.naturalLanguageQuery = _txfBusquedaPoblacion.text;
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:localSearchRequest];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        _txfBusquedaPoblacion.text = @"";
        
        if ( !error )
        {
            if ( [response.mapItems count] == 0 )
                [[[UIAlertView alloc] initWithTitle:@""
                                            message:NSLocalizedString(@"AJUSTES_BUSQUEDADIRECCIONERROR", @"")
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
            else
            {
                _tableViewDirecciones.hidden = NO;
                [_listadoDatos_Direcciones addObjectsFromArray:[response mapItems]];
                [_tableViewDirecciones reloadData];
            }
        }
        else
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:NSLocalizedString(@"AJUSTES_BUSQUEDADIRECCIONERROR", @"")
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
    }];
}

#pragma mark -
#pragma mark - UIGestureRecognizerDelegate

- (IBAction)singleTapGesture:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)longPressGesture:(id)sender
{
    [self.view endEditing:YES];
    
    if ( [sender state] == UIGestureRecognizerStateBegan )
    {
        CGPoint point = [sender locationInView:_mapView];
        CLLocationCoordinate2D coordinate = [_mapView convertPoint:point toCoordinateFromView:_mapView];
        
        [self addAnotacion:coordinate];
        [self calculateAdress:coordinate];
    }
}

#pragma mark -
#pragma mark - UISwitch

- (IBAction)swtGeoLocalizacion_ValueChanged:(id)sender
{
    [self.view endEditing:YES];
    
    _longPressGesture.enabled = ![sender isOn];
    _txfBusquedaPoblacion.enabled = ![sender isOn];
    _btnBusquedaPoblacion.enabled = ![sender isOn];
    _btnGuardar.enabled = ![sender isOn];

    if ( [sender isOn] )
    {
        [_mapView removeAnnotations:_mapView.annotations];
        [_datosAplicacion.locationManager startUpdatingLocation];
        _mapView.showsUserLocation = YES;
        [_mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
        
        if ( ( _latitud != 0.0 ) && ( _longitud != 0.0 ) )
            [_mapView setRegion:[_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(_latitud, _longitud), 1000, 1000)] animated:YES];
        
        [self calculateAdress:CLLocationCoordinate2DMake(_latitud, _longitud)];
    }
    else
    {
        [_datosAplicacion.locationManager stopUpdatingLocation];
        _mapView.showsUserLocation = NO;
        [_mapView setUserTrackingMode:MKUserTrackingModeNone];
    }

    _longPressGesture.enabled = ![_swtGeoLocalizacion isOn];

    [_lblInformacion setText:( [_swtGeoLocalizacion isOn] ) ? NSLocalizedString(@"AJUSTES_GEOLOCALIZACION_ON", @"") : NSLocalizedString(@"AJUSTES_GEOLOCALIZACION_OFF", @"")];

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setValue:[NSNumber numberWithBool:[sender isOn]] forKeyPath:@"gelocalizacionActiva"];
    [userDefaults synchronize];
}

#pragma mark -
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self btnBusquedaPoblacion_Pressed:self];
    return [textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [_listadoDatos_Direcciones removeAllObjects];
    [_tableViewDirecciones reloadData];
    _tableViewDirecciones.hidden = YES;
    
    _tapGesture.enabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _tapGesture.enabled = NO;
}

#pragma mark -
#pragma mark MapView

- (void)addAnotacion:(CLLocationCoordinate2D)coordinate
{
    [_mapView removeAnnotations:_mapView.annotations];

    if ( ![_swtGeoLocalizacion isOn] )
    {
        MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithCoordinate:coordinate];
        annotation.tag = 1;
        annotation.imagePinData = UIImagePNGRepresentation([[UIImage imageNamed:@"Img_PunteroLocalizacion1"] imageByScalingProportionallyToSize:CGSizeMake(19, 33)]);
        [_mapView addAnnotation:annotation];
        
        [_mapView setRegion:[_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(coordinate, 1000, 1000)] animated:YES];
    }
}

- (void)calculateAdress:(CLLocationCoordinate2D)coordinate
{
    CLLocation *newLocation = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    [_datosAplicacion updateLocation];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if ( error == nil && [placemarks count] > 0 )
            _lblPoblacion.text = [[placemarks lastObject] locality];
    }];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ( [annotation isKindOfClass:[MapViewAnnotation class]] )
    {
        MapViewAnnotation *mapAnnotation = annotation;

        MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
        
        if ( !pinView )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
            
            pinView.animatesDrop = NO;
            pinView.canShowCallout = NO;
            pinView.draggable = NO;
            pinView.image = [UIImage imageWithData:mapAnnotation.imagePinData];
        }
        else pinView.annotation = annotation;
        
        return pinView;
    }
    return nil;
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Ajustes_ConfiguracionLocalizacionDireccionesCell class]] )
    {
        MKMapItem *mapItem = [_listadoDatos_Direcciones objectAtIndex:indexPath.row];
        Ajustes_ConfiguracionLocalizacionDireccionesCell *cellToConfigure = (Ajustes_ConfiguracionLocalizacionDireccionesCell *)cell;
        
        cellToConfigure.lblDescripcion.text = [[mapItem placemark] title];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_listadoDatos_Direcciones count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Ajustes_ConfiguracionLocalizacionDireccionesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellAjustes_ConfiguracionLocalizacionDirecciones"];
    [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _tableViewDirecciones.hidden = YES;
    
    MKMapItem *mapItem = [_listadoDatos_Direcciones objectAtIndex:indexPath.row];
    MKCoordinateRegion adjustedRegion = [_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake([[mapItem placemark] coordinate].latitude, [[mapItem placemark] coordinate].longitude), 500, 500)];
    [_mapView setRegion:adjustedRegion animated:YES];
    
    [self addAnotacion:CLLocationCoordinate2DMake([[mapItem placemark] coordinate].latitude, [[mapItem placemark] coordinate].longitude)];
    _lblPoblacion.text = [[mapItem placemark] locality];
}

@end