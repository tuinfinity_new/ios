//
//  AjustesVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 28/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "BTSimpleSideMenu.h"

#import "Globales.h"

#import "Ajustes_ConfiguracionVC.h"

#import "AjustesVC.h"

@interface AjustesVC ()

@property (nonatomic) BTSimpleSideMenu *sideMenu;

@property (nonatomic, weak) IBOutlet UIScrollView *viewContainer;

@property UIViewController *currentDetailViewController;

@property (nonatomic, strong) IBOutlet Ajustes_ConfiguracionVC *ajustes_Configuracion;

@end

@implementation AjustesVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    _ajustes_Configuracion = [storyboard instantiateViewControllerWithIdentifier:@"Ajustes_Configuracion"];
    
    [_ajustes_Configuracion setOriginView:self];

    [self configuraNavigationBar];
    [self configuraMenuLateral];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = COLOR_AZUL1;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self presentDetailController:_ajustes_Configuracion];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_MenuPrincipal"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnMenuPrincipal_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    btnL1.selected = NO;
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - Misc

- (void)configuraMenuLateral
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    BTSimpleMenuItem *menuItem1 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_1", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Cupones"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Cupones"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem2 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_2", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Ofertas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Ofertas"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem3 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_3", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Empresas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Empresas"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem4 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_4", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Rutas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Rutas"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem5 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_5", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Lugares"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Lugares"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem6 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_6", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Wifi"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Wifi"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem0 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_0", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Ajustes"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 _viewContainer.userInteractionEnabled = YES;
                                                             }];

    _sideMenu = [[BTSimpleSideMenu alloc] initWithItem:@[menuItem1, menuItem2, menuItem3, menuItem4, menuItem5, menuItem6, menuItem0] addToViewController:self];
}

#pragma mark -
#pragma mark - View Controller

- (void)presentDetailController:(UIViewController*)detailVC
{
    if ( _currentDetailViewController )
        [self removeCurrentDetailViewController];
    
    _currentDetailViewController = detailVC;
    _currentDetailViewController.view.frame = _viewContainer.bounds;
    [_viewContainer addSubview:_currentDetailViewController.view];
}

- (void)removeCurrentDetailViewController
{
    [_currentDetailViewController.view removeFromSuperview];
    _currentDetailViewController = nil;
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnMenuPrincipal_Pressed:(id)sender
{
    [_sideMenu toggleMenu];
    _viewContainer.userInteractionEnabled = ![_sideMenu isOpen];
}

@end