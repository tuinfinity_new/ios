//
//  Ajustes_ConfiguracionVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 28/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AjustesVC.h"

@interface Ajustes_ConfiguracionVC : UIViewController

@property (nonatomic, weak) IBOutlet AjustesVC *originView;

@end
