//
//  Ajustes_LocalizacionVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 11/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ajustes_LocalizacionVC : UIViewController

@property (nonatomic, readwrite) double latitud;
@property (nonatomic, readwrite) double longitud;

@end
