//
//  Ajustes_ConfiguracionVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 28/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"
#import "MapViewAnnotation.h"
#import "UIImage_Misc.h"

#import "Ajustes_ConfiguracionLocalizacionCell.h"
#import "Ajustes_ConfiguracionLocalizacionDireccionesCell.h"
#import "Ajustes_ConfiguracionNotificacionesCell.h"
#import "Ajustes_ConfiguracionPerfilCell.h"
#import "Ajustes_ConfiguracionOtrosCell.h"

#import "TutorialVC.h"
#import "Ajustes_LocalizacionVC.h"

#import "Ajustes_ConfiguracionVC.h"

@interface Ajustes_ConfiguracionVC () <UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, readwrite) NSMutableArray *sectionHidden;

@property (nonatomic, weak) IBOutlet UIButton *btnGuardar;
@property (nonatomic, weak) IBOutlet UILabel *lblLogout;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;

@property (nonatomic, strong) NSMutableArray *listadoDatos_Direcciones;

@property (nonatomic, retain) NSString *poblacion;

@end

@implementation Ajustes_ConfiguracionVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _originView.navigationController.delegate = self;

    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];

    _listadoDatos_Direcciones = [NSMutableArray array];

    _sectionHidden = [[NSMutableArray alloc] initWithArray:@[[NSNumber numberWithBool:YES],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO],[NSNumber numberWithBool:NO]]];

    _btnGuardar.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];

    _lblLogout.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
    
    _poblacion = _datosAplicacion.poblacion;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [_tableView reloadData];

    Ajustes_ConfiguracionLocalizacionCell *cell = (Ajustes_ConfiguracionLocalizacionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    if ( ![cell.swtGeoLocalizacion isOn] )
        [self addAnotacion:CLLocationCoordinate2DMake(_datosAplicacion.latitud, _datosAplicacion.longitud)];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Misc

- (void)logoutAplicacion
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;

    [_originView.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Logout"] animated:NO];
}

- (void)updateUserProfileWithName:(NSString *)userName andPassword:(NSString *)userPassword
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    [_activityIndicator startAnimating];
    
    NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
    if ( [userName length] > 0 )
        [itemData setValue:userName forKey:@"nombre"];
    if ( [userPassword length] > 0 )
        [itemData setValue:userPassword forKey:@"pass"];
    
    NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
    
    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager.securityPolicy setAllowInvalidCertificates:YES];
    operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
    operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
    [operationManager PUT:[NSString stringWithFormat:@"usuario/%ld", [[userDefaults objectForKey:@"usrIdUsuario"] longValue]]
               parameters:itemData
                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                      [_activityIndicator stopAnimating];
                      
                      if ( [userPassword length] > 0 )
                      {
                          [userDefaults setObject:userPassword forKey:@"usrPassword"];
                          [userDefaults setValue:[NSNumber numberWithBool:YES] forKeyPath:@"gelocalizacionActiva"];
                      }
 
                      [[[UIAlertView alloc] initWithTitle:@""
                                                  message:NSLocalizedString(@"AJUSTES_CAMBIOPERFILOK", @"")
                                                 delegate:self
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil] show];
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error){
                      NSLog(@"%@",error.description);
                      [_activityIndicator stopAnimating];
                  }];
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnLogout_Pressed:(id)sender
{
    [_activityIndicator startAnimating];
    [self performSelector:@selector(logoutAplicacion) withObject:self afterDelay:1.0];
}

- (IBAction)btnGuardar_Pressed:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    Ajustes_ConfiguracionLocalizacionCell *cellLocalizacion = (Ajustes_ConfiguracionLocalizacionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    _datosAplicacion.geolocalizacionActiva = cellLocalizacion.swtGeoLocalizacion.isOn;
    
    if ( !cellLocalizacion.swtGeoLocalizacion.isOn )
    {
        if ( [cellLocalizacion.mapView.annotations count] > 0 )
        {
            MapViewAnnotation *annotation = [cellLocalizacion.mapView.annotations objectAtIndex:0];
            
            _datosAplicacion.latitud = annotation.coordinate.latitude;
            _datosAplicacion.longitud = annotation.coordinate.longitude;
            _datosAplicacion.poblacion = _poblacion;
            
            [userDefaults setValue:[NSNumber numberWithDouble:_datosAplicacion.latitud] forKey:@"locLatitud"];
            [userDefaults setValue:[NSNumber numberWithDouble:_datosAplicacion.longitud] forKey:@"locLongitud"];
        }
    }
    
    [userDefaults setValue:[NSNumber numberWithBool:cellLocalizacion.swtGeoLocalizacion.isOn] forKeyPath:@"gelocalizacionActiva"];
    [userDefaults setObject:[NSNumber numberWithDouble:[[NSString stringWithFormat:@"%.2f", cellLocalizacion.sldRadioBusqueda.value] doubleValue]] forKey:@"radioBusqueda"];

    [userDefaults synchronize];
    
    if ( ![[userDefaults objectForKey:@"usrInvitado"] boolValue] )
    {
        Ajustes_ConfiguracionPerfilCell *cellPerfil = (Ajustes_ConfiguracionPerfilCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        
        if ( ( [cellPerfil.txfNombre.text length] > 0 ) || ( [cellPerfil.txfPassword1.text length] > 0 ) )
        {
            if ( [cellPerfil.txfPassword1.text length] > 0 )
            {
                if ( [cellPerfil.txfPassword1.text length] < 5 )
                    [[[UIAlertView alloc] initWithTitle:@""
                                                message:NSLocalizedString(@"AJUSTES_CAMBIOPASSWORDERROR1", @"")
                                               delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil] show];
                else if ( ![cellPerfil.txfPassword1.text isEqualToString:cellPerfil.txfPassword2.text] )
                    [[[UIAlertView alloc] initWithTitle:@""
                                                message:NSLocalizedString(@"AJUSTES_CAMBIOPASSWORDERROR2", @"")
                                               delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil] show];
                else if ( [cellPerfil.txfPasswordOriginal.text length] == 0 )
                    [[[UIAlertView alloc] initWithTitle:@""
                                                message:NSLocalizedString(@"AJUSTES_CAMBIOPASSWORDERROR3", @"")
                                               delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil] show];
                else if ( ![cellPerfil.txfPasswordOriginal.text isEqualToString:[userDefaults objectForKey:@"usrPassword"]] )
                    [[[UIAlertView alloc] initWithTitle:@""
                                                message:NSLocalizedString(@"AJUSTES_CAMBIOPASSWORDERROR4", @"")
                                               delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil] show];
                else
                    [self updateUserProfileWithName:cellPerfil.txfNombre.text andPassword:cellPerfil.txfPassword1.text];
            }
            else
                [self updateUserProfileWithName:cellPerfil.txfNombre.text andPassword:@""];
        }
    }
}

- (IBAction)btnBusquedaPoblacion_Pressed:(id)sender
{
    [self.view endEditing:YES];
    
    Ajustes_ConfiguracionLocalizacionCell *cellLocalizacion = (Ajustes_ConfiguracionLocalizacionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    cellLocalizacion.tableViewDirecciones.hidden = YES;
    [_listadoDatos_Direcciones removeAllObjects];
    
    MKLocalSearchRequest *localSearchRequest = [[MKLocalSearchRequest alloc] init];
    localSearchRequest.naturalLanguageQuery = cellLocalizacion.txfBusquedaPoblacion.text;

    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:localSearchRequest];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        cellLocalizacion.txfBusquedaPoblacion.text = @"";

        if ( !error )
        {
            if ( [response.mapItems count] == 0 )
                [[[UIAlertView alloc] initWithTitle:@""
                                            message:NSLocalizedString(@"AJUSTES_BUSQUEDADIRECCIONERROR", @"")
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
            else
            {
                cellLocalizacion.tableViewDirecciones.hidden = NO;
                [_listadoDatos_Direcciones addObjectsFromArray:[response mapItems]];
                [cellLocalizacion.tableViewDirecciones reloadData];
            }
        }
        else
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:NSLocalizedString(@"AJUSTES_BUSQUEDADIRECCIONERROR", @"")
                                       delegate:self
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
    }];
}

- (IBAction)btnGeoLocalizacion_Pressed:(id)sender
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;

    Ajustes_ConfiguracionLocalizacionCell *cellLocalizacion = (Ajustes_ConfiguracionLocalizacionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    Ajustes_LocalizacionVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Ajustes_Localizacion"];

    MapViewAnnotation *annotation = [cellLocalizacion.mapView.annotations objectAtIndex:0];
    [pantallaDestino setLatitud:annotation.coordinate.latitude];
    [pantallaDestino setLongitud:annotation.coordinate.longitude];

    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnSeccion_Pressed:(id)sender
{
    [self.view endEditing:YES];
    
    int section = (int)[(UIButton*)sender tag];
    
    NSIndexSet *indexSection = [[NSIndexSet alloc] initWithIndex:section];

    [_sectionHidden replaceObjectAtIndex:section withObject:[NSNumber numberWithBool:![[_sectionHidden objectAtIndex:section] boolValue]]];
    [_tableView reloadSections:indexSection withRowAnimation:UITableViewRowAnimationFade];

    if ( [[_sectionHidden objectAtIndex:section] boolValue] )
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:section] atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (IBAction)btnVerTutorial_Pressed:(id)sender
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;

    UIViewController *activeController = [UIApplication sharedApplication].keyWindow.rootViewController;
    if ( [activeController isKindOfClass:[UINavigationController class]] )
        activeController = [(UINavigationController*) activeController visibleViewController];
    else if ( activeController.presentedViewController )
        activeController = activeController.presentedViewController;

    TutorialVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Tutorial"];
    [pantallaDestino setOrigenInicioAplicacion:NO];
    [activeController presentViewController:pantallaDestino animated:NO completion:nil];
}

#pragma mark -
#pragma mark - UIGestureRecognizerDelegate

- (IBAction)singleTapGesture:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark - UISlider

- (IBAction)sldOpcionesBusqueda_Radio_ValueChanged:(id)sender
{
    [self.view endEditing:YES];

    Ajustes_ConfiguracionLocalizacionCell *cellLocalizacion = (Ajustes_ConfiguracionLocalizacionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
    {
        if ( cellLocalizacion.sldRadioBusqueda.value < 1000 )
            cellLocalizacion.lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (int)cellLocalizacion.sldRadioBusqueda.value];
        else
            cellLocalizacion.lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), (int)cellLocalizacion.sldRadioBusqueda.value / 1000.0];
    }
    else
    {
        if ( cellLocalizacion.sldRadioBusqueda.value < POINT_ONE_MILE_METERS )
            cellLocalizacion.lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), cellLocalizacion.sldRadioBusqueda.value * METERS_TO_FEET];
        else
            cellLocalizacion.lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), cellLocalizacion.sldRadioBusqueda.value * METERS_TO_MILES];
    }
}

#pragma mark -
#pragma mark - UISwitch

- (IBAction)swtGeoLocalizacion_ValueChanged:(id)sender
{
    [self.view endEditing:YES];
    
    Ajustes_ConfiguracionLocalizacionCell *cellLocalizacion = (Ajustes_ConfiguracionLocalizacionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    _datosAplicacion.geolocalizacionActiva = cellLocalizacion.swtGeoLocalizacion.isOn;

    cellLocalizacion.txfBusquedaPoblacion.enabled = ![sender isOn];
    cellLocalizacion.btnBusquedaPoblacion.enabled = ![sender isOn];
    cellLocalizacion.btnGeoLocalizacion1.enabled = ![sender isOn];
    cellLocalizacion.btnGeoLocalizacion2.enabled = ![sender isOn];

    if ( [sender isOn] )
    {
        [cellLocalizacion.mapView removeAnnotations:cellLocalizacion.mapView.annotations];
        [_datosAplicacion.locationManager startUpdatingLocation];
        cellLocalizacion.mapView.showsUserLocation = YES;
        [cellLocalizacion.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
        
        if ( ( _datosAplicacion.latitud != 0.0 ) && ( _datosAplicacion.longitud != 0.0 ) )
            [cellLocalizacion.mapView setRegion:[cellLocalizacion.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(_datosAplicacion.latitud, _datosAplicacion.longitud), 500, 500)] animated:YES];
    }
    else
    {
        [_datosAplicacion.locationManager stopUpdatingLocation];
        cellLocalizacion.mapView.showsUserLocation = NO;
        [cellLocalizacion.mapView setUserTrackingMode:MKUserTrackingModeNone];
        [self addAnotacion:CLLocationCoordinate2DMake(_datosAplicacion.latitud, _datosAplicacion.longitud)];
    }

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:[NSNumber numberWithBool:[sender isOn]] forKeyPath:@"gelocalizacionActiva"];
    [userDefaults synchronize];
}

#pragma mark -
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    Ajustes_ConfiguracionLocalizacionCell *cellLocalizacion = (Ajustes_ConfiguracionLocalizacionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    if ( textField == cellLocalizacion.txfBusquedaPoblacion )
    {
        [self btnBusquedaPoblacion_Pressed:self];
        return [textField resignFirstResponder];
    }
    else
    {
        int nextTag = (int)textField.tag + 1;
        UIResponder *nextResponder = [self.view viewWithTag:nextTag];
        
        if ( nextResponder )
            [nextResponder becomeFirstResponder];
        else
            [textField resignFirstResponder];
        
        return [textField resignFirstResponder];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    Ajustes_ConfiguracionLocalizacionCell *cellLocalizacion = (Ajustes_ConfiguracionLocalizacionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    Ajustes_ConfiguracionPerfilCell *cellPerfil = (Ajustes_ConfiguracionPerfilCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    
    if ( textField == cellLocalizacion.txfBusquedaPoblacion )
    {
        [_listadoDatos_Direcciones removeAllObjects];
        [cellLocalizacion.tableViewDirecciones reloadData];
        cellLocalizacion.tableViewDirecciones.hidden = YES;
    }
    else
    {
        if ( textField == cellPerfil.txfNombre )
            [Globales scrollCurrentView:_tableView withScroll:60];
        else if ( textField == cellPerfil.txfPassword1 )
            [Globales scrollCurrentView:_tableView withScroll:95];
        else if ( textField == cellPerfil.txfPassword2 )
            [Globales scrollCurrentView:_tableView withScroll:130];
        else if ( textField == cellPerfil.txfPasswordOriginal )
            [Globales scrollCurrentView:_tableView withScroll:165];
    }

    _tapGesture.enabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    Ajustes_ConfiguracionPerfilCell *cellPerfil = (Ajustes_ConfiguracionPerfilCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    
    if ( textField == cellPerfil.txfNombre )
        [Globales scrollCurrentView:_tableView withScroll:-60];
    else if ( textField == cellPerfil.txfPassword1 )
        [Globales scrollCurrentView:_tableView withScroll:-95];
    else if ( textField == cellPerfil.txfPassword2 )
        [Globales scrollCurrentView:_tableView withScroll:-130];
    else if ( textField == cellPerfil.txfPasswordOriginal )
        [Globales scrollCurrentView:_tableView withScroll:-165];
    
    _tapGesture.enabled = NO;
}

#pragma mark -
#pragma mark MapView

- (void)addAnotacion:(CLLocationCoordinate2D)coordinate
{
    Ajustes_ConfiguracionLocalizacionCell *cellLocalizacion = (Ajustes_ConfiguracionLocalizacionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cellLocalizacion.mapView removeAnnotations:cellLocalizacion.mapView.annotations];
    
    if ( ![cellLocalizacion.swtGeoLocalizacion isOn] )
    {
        MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithCoordinate:coordinate];
        annotation.tag = 1;
        annotation.imagePinData = UIImagePNGRepresentation([[UIImage imageNamed:@"Img_PunteroLocalizacion1"] imageByScalingProportionallyToSize:CGSizeMake(19, 33)]);
        [cellLocalizacion.mapView addAnnotation:annotation];
        [cellLocalizacion.mapView setRegion:[cellLocalizacion.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(coordinate, 500, 500)] animated:YES];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ( [annotation isKindOfClass:[MapViewAnnotation class]] )
    {
        MapViewAnnotation *mapAnnotation = annotation;
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
        
        if ( !pinView )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
            
            pinView.animatesDrop = NO;
            pinView.canShowCallout = NO;
            pinView.draggable = NO;
            pinView.image = [UIImage imageWithData:mapAnnotation.imagePinData];
        }
        else pinView.annotation = annotation;
        
        return pinView;
    }
    return nil;
}

#pragma mark -
#pragma mark - TableView

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return ( tableView == _tableView ) ? 25 : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ( tableView == _tableView )
    {
        UILabel *lblTitulo = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, tableView.frame.size.width - 30, 30)];
        [lblTitulo setFont:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:15]];
        [lblTitulo setTextColor:COLOR_BLANCO];
        
        UIImageView *imgSeccion = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 15, 15)];
        
        switch ( section )
        {
            case 0:
                [imgSeccion setImage:[UIImage imageNamed:( [[_sectionHidden objectAtIndex:section] boolValue] ) ? @"Img_SeccionON" : @"Img_SeccionOFF"]];
                [lblTitulo setText:NSLocalizedString(@"AJUSTES_LOCALIZACION", @"")];
                break;
            case 1:
                [imgSeccion setImage:[UIImage imageNamed:( [[_sectionHidden objectAtIndex:section] boolValue] ) ? @"Img_SeccionON" : @"Img_SeccionOFF"]];
                [lblTitulo setText:NSLocalizedString(@"AJUSTES_NOTIFICACIONES", @"")];
                break;
            case 2:
                [imgSeccion setImage:[UIImage imageNamed:( [[_sectionHidden objectAtIndex:section] boolValue] ) ? @"Img_SeccionON" : @"Img_SeccionOFF"]];
                [lblTitulo setText:NSLocalizedString(@"AJUSTES_PERFIL", @"")];
                break;
            case 3:
                [imgSeccion setImage:[UIImage imageNamed:( [[_sectionHidden objectAtIndex:section] boolValue] ) ? @"Img_SeccionON" : @"Img_SeccionOFF"]];
                [lblTitulo setText:NSLocalizedString(@"AJUSTES_OTROS", @"")];
                break;
            default:
                break;
        }
        
        UIButton *btnSeccion = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSeccion.frame = CGRectMake(0, 0, tableView.frame.size.width, 25);
        btnSeccion.tag = section;
        [btnSeccion addTarget:self action:@selector(btnSeccion_Pressed:) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *headerView = [[UIView alloc] init];
        headerView.backgroundColor = COLOR_AZUL1;
        
        [headerView addSubview:imgSeccion];
        [headerView addSubview:lblTitulo];
        [headerView addSubview:btnSeccion];
        
        return headerView;
    }
    else
        return nil;
}

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( [cell isKindOfClass:[Ajustes_ConfiguracionLocalizacionCell class]] )
    {
        Ajustes_ConfiguracionLocalizacionCell *cellToConfigure = (Ajustes_ConfiguracionLocalizacionCell *)cell;
     
        cellToConfigure.lblTitulo1.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:16];
        cellToConfigure.lblTitulo2.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:16];
        cellToConfigure.btnBusquedaPoblacion.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
        cellToConfigure.btnGeoLocalizacion2.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];

        cellToConfigure.tableViewDirecciones.hidden = YES;

        [cellToConfigure.swtGeoLocalizacion setOn:_datosAplicacion.geolocalizacionActiva];

        cellToConfigure.mapView.showsUserLocation = [cellToConfigure.swtGeoLocalizacion isOn];
        cellToConfigure.txfBusquedaPoblacion.enabled = ![cellToConfigure.swtGeoLocalizacion isOn];
        cellToConfigure.btnBusquedaPoblacion.enabled = ![cellToConfigure.swtGeoLocalizacion isOn];
        cellToConfigure.btnGeoLocalizacion1.enabled = ![cellToConfigure.swtGeoLocalizacion isOn];
        cellToConfigure.btnGeoLocalizacion2.enabled = ![cellToConfigure.swtGeoLocalizacion isOn];

        if ( ![userDefaults objectForKey:@"radioBusqueda"] )
            cellToConfigure.sldRadioBusqueda = 0;
        else
            cellToConfigure.sldRadioBusqueda.value = [[userDefaults objectForKey:@"radioBusqueda"] doubleValue];
        
        if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
        {
            if ( cellToConfigure.sldRadioBusqueda.value < 1000 )
                cellToConfigure.lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (int)cellToConfigure.sldRadioBusqueda.value];
            else
                cellToConfigure.lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), (int)cellToConfigure.sldRadioBusqueda.value / 1000.0];
        }
        else
        {
            if ( cellToConfigure.sldRadioBusqueda.value < POINT_ONE_MILE_METERS )
                cellToConfigure.lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), cellToConfigure.sldRadioBusqueda.value * METERS_TO_FEET];
            else
                cellToConfigure.lblRadioBusqueda.text = [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), cellToConfigure.sldRadioBusqueda.value * METERS_TO_MILES];
        }
        
        if ( ( _datosAplicacion.latitud != 0.0 ) && ( _datosAplicacion.longitud != 0.0 ) )
            [cellToConfigure.mapView setRegion:[cellToConfigure.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(_datosAplicacion.latitud, _datosAplicacion.longitud), 500, 500)] animated:YES];
    }
    else if ( [cell isKindOfClass:[Ajustes_ConfiguracionNotificacionesCell class]] )
    {
        Ajustes_ConfiguracionNotificacionesCell *cellToConfigure = (Ajustes_ConfiguracionNotificacionesCell *)cell;
        
    }
    else if ( [cell isKindOfClass:[Ajustes_ConfiguracionPerfilCell class]] )
    {
        Ajustes_ConfiguracionPerfilCell *cellToConfigure = (Ajustes_ConfiguracionPerfilCell *)cell;
        
    }
    else if ( [cell isKindOfClass:[Ajustes_ConfiguracionLocalizacionDireccionesCell class]] )
    {
        MKMapItem *mapItem = [_listadoDatos_Direcciones objectAtIndex:indexPath.row];
        Ajustes_ConfiguracionLocalizacionDireccionesCell *cellToConfigure = (Ajustes_ConfiguracionLocalizacionDireccionesCell *)cell;
        
        cellToConfigure.lblDescripcion.text = [[mapItem placemark] title];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( tableView == _tableView )
    {
        switch ( indexPath.section )
        {
            case 0: return 380; break;
            case 1: return 0; break;
            case 2: return 145; break;
            case 3: return 41; break;
        }
        
        return 0;
    }
    else
        return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ( tableView == _tableView ) ? 4 : 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( tableView == _tableView )
        return ( ![[_sectionHidden objectAtIndex:section] boolValue] ) ? 0 : 1;
    else
        return [_listadoDatos_Direcciones count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( tableView == _tableView )
    {
        if ( indexPath.section == 0 )
        {
            Ajustes_ConfiguracionLocalizacionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellAjustes_ConfiguracionLocalizacion"];
            [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
            return cell;
        }
        else if ( indexPath.section == 1 )
        {
            Ajustes_ConfiguracionNotificacionesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellAjustes_ConfiguracionNotificaciones"];
            [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
            return cell;
        }
        else if ( indexPath.section == 2 )
        {
            Ajustes_ConfiguracionPerfilCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellAjustes_ConfiguracionPerfil"];
            [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
            return cell;
        }
        else if ( indexPath.section == 3 )
        {
            Ajustes_ConfiguracionOtrosCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellAjustes_ConfiguracionOtros"];
            [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
            return cell;
        }
    }
    else
    {
        Ajustes_ConfiguracionLocalizacionDireccionesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellAjustes_ConfiguracionLocalizacionDirecciones"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }

    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Ajustes_ConfiguracionLocalizacionCell *cellLocalizacion = (Ajustes_ConfiguracionLocalizacionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

    if ( tableView == cellLocalizacion.tableViewDirecciones )
    {
        cellLocalizacion.tableViewDirecciones.hidden = YES;
        
        MKMapItem *mapItem = [_listadoDatos_Direcciones objectAtIndex:indexPath.row];
        MKCoordinateRegion adjustedRegion = [cellLocalizacion.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake([[mapItem placemark] coordinate].latitude, [[mapItem placemark] coordinate].longitude), 500, 500)];
        [cellLocalizacion.mapView setRegion:adjustedRegion animated:YES];
        
        [self addAnotacion:CLLocationCoordinate2DMake([[mapItem placemark] coordinate].latitude, [[mapItem placemark] coordinate].longitude)];
        _poblacion = [[mapItem placemark] locality];
    }
}

@end