//
//  Wifi_ListadoCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 30/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Wifi_ListadoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *viewEmpresa;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UILabel *lblNombre;
@property (nonatomic, weak) IBOutlet UILabel *lblActividad;
@property (nonatomic, weak) IBOutlet UILabel *lblDistancia;

@property (nonatomic, weak) IBOutlet UIImageView *imgFoto;

@end