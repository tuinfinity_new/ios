//
//  WifiVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 10/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "BTSimpleSideMenu.h"
#import "SDWebImageManager.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"

#import "Ajustes_LocalizacionVC.h"
#import "Wifi_ListadoVC.h"

#import "WifiVC.h"

@interface WifiVC () <BTSimpleSideMenuDelegate>

@property (nonatomic) BTSimpleSideMenu *sideMenu;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet UIScrollView *viewContainer;
@property (weak, nonatomic) IBOutlet UIView *viewModoVista;

@property UIViewController *currentDetailViewController;

@property (nonatomic, strong) IBOutlet Wifi_ListadoVC *wifi_ListadoVC;

@end

@implementation WifiVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];

    _viewModoVista.hidden = YES;

    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    _wifi_ListadoVC = [storyboard instantiateViewControllerWithIdentifier:@"Wifi_Listado"];

    [_wifi_ListadoVC setOriginView:self];

    [self configuraNavigationBarWithBtnModoVista:YES andBtnGeolocalizacion:YES];
    [self configuraMenuLateral];
    
    [self presentDetailController:_wifi_ListadoVC];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = COLOR_AZUL1;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self presentDetailController:_wifi_ListadoVC];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[SDWebImageManager sharedManager] cancelAll];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBarWithBtnModoVista:(BOOL)btnModoVistaVisible andBtnGeolocalizacion:(BOOL)btnGeolocalizacionVisible;
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *btnL2 = [UIButton buttonWithType:UIButtonTypeCustom];
    UIButton *btnR1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_MenuPrincipal"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnMenuPrincipal_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnL2 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL2 setImage:[UIImage imageNamed:@"Btn_Geolocalizacion"] forState:UIControlStateNormal];
    [btnL2 addTarget:self action:@selector(btnGeolocalizacion_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnR1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnR1 setImage:[UIImage imageNamed:@"Btn_ModosVista"] forState:UIControlStateNormal];
    [btnR1 addTarget:self action:@selector(btnModosVista_Pressed:) forControlEvents:UIControlEventTouchUpInside];

    btnL1.selected = NO;
    btnL2.selected = NO;
    btnR1.selected = NO;
    
    NSMutableArray *buttonItemsL = [[NSMutableArray alloc] init];
    NSMutableArray *buttonItemsR = [[NSMutableArray alloc] init];
    
    [buttonItemsL addObject:[[UIBarButtonItem alloc] initWithCustomView:btnL1]];
    if ( btnGeolocalizacionVisible ) [buttonItemsL addObject:[[UIBarButtonItem alloc] initWithCustomView:btnL2]];

    if ( btnModoVistaVisible ) [buttonItemsR addObject:[[UIBarButtonItem alloc] initWithCustomView:btnR1]];
    
    self.navigationItem.leftBarButtonItems = buttonItemsL;
    self.navigationItem.rightBarButtonItems = buttonItemsR;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - Misc

- (void)configuraMenuLateral
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    BTSimpleMenuItem *menuItem1 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_1", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Cupones"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Cupones"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem2 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_2", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Ofertas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Ofertas"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem4 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_4", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Rutas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Rutas"] animated:NO];
                                                             }];
    
    BTSimpleMenuItem *menuItem3 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_3", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Empresas"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Empresas"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem5 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_5", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Lugares"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Lugares"] animated:NO];
                                                             }];
    BTSimpleMenuItem *menuItem6 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_6", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Wifi"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 _viewContainer.userInteractionEnabled = YES;
                                                             }];
    BTSimpleMenuItem *menuItem0 = [[BTSimpleMenuItem alloc] initWithTitle:NSLocalizedString(@"MENU_OPC_0", @"")
                                                                    image:[UIImage imageNamed:@"Btn_MenuPrincipal_Ajustes"]
                                                             onCompletion:^(BOOL success, BTSimpleMenuItem *item) {
                                                                 [self.navigationController pushViewController:[storyboard instantiateViewControllerWithIdentifier:@"Ajustes"] animated:NO];
                                                             }];

    _sideMenu = [[BTSimpleSideMenu alloc] initWithItem:@[menuItem1, menuItem2, menuItem3, menuItem4, menuItem5, menuItem6, menuItem0] addToViewController:self];
}

#pragma mark -
#pragma mark - View Controller

- (void)presentDetailController:(UIViewController*)detailVC
{
    if ( _currentDetailViewController )
        [self removeCurrentDetailViewController];

    _currentDetailViewController = detailVC;
    _currentDetailViewController.view.frame = _viewContainer.bounds;
    [_viewContainer addSubview:_currentDetailViewController.view];
}

- (void)removeCurrentDetailViewController
{
    [_currentDetailViewController.view removeFromSuperview];
    _currentDetailViewController = nil;
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnMenuPrincipal_Pressed:(id)sender
{
    [_sideMenu toggleMenu];
    _viewContainer.userInteractionEnabled = ![_sideMenu isOpen];
}

- (IBAction)btnModosVista_Pressed:(id)sender
{
    CGRect frameShown = CGRectMake([[UIScreen mainScreen] bounds].size.width - _viewModoVista.frame.size.width, 0, _viewModoVista.frame.size.width, _viewModoVista.frame.size.height);
    CGRect frameHidden = CGRectMake([[UIScreen mainScreen] bounds].size.width - _viewModoVista.frame.size.width, 0 - _viewModoVista.frame.size.height, _viewModoVista.frame.size.width, _viewModoVista.frame.size.height);
    
    if ( _viewModoVista.hidden )
    {
        [_viewModoVista setHidden:!_viewModoVista.hidden];
        [_viewModoVista setFrame:frameHidden];
        [UIView animateWithDuration:0.5 animations:^{ [_viewModoVista setFrame:frameShown]; }];
    }
    else
        [_viewModoVista setHidden:!_viewModoVista.hidden];
    
    _viewContainer.userInteractionEnabled = _viewModoVista.hidden;
}

- (IBAction)btnGeolocalizacion_Pressed:(id)sender
{
    [_sideMenu hide];
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Ajustes_LocalizacionVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Ajustes_Localizacion"];
    [pantallaDestino setLatitud:_datosAplicacion.latitud];
    [pantallaDestino setLongitud:_datosAplicacion.longitud];
    [self.navigationController pushViewController:pantallaDestino animated:NO];
}

#pragma mark -
#pragma mark - TableView


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = [NSString stringWithFormat:@"CellTipoVista%d", indexPath.row + 1];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [_viewModoVista setHidden:!_viewModoVista.hidden];
    _viewContainer.userInteractionEnabled = YES;
    
    if ( ( indexPath.row == 2 ) && ( !_datosAplicacion.geolocalizacionActiva ) )
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:NSLocalizedString(@"GEOLOCALIZACION_OFF_ERROR_RA", @"")
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    else
    {
        _wifi_ListadoVC.previousSelectedView = _wifi_ListadoVC.selectedView;
        _wifi_ListadoVC.selectedView = indexPath.row + 1;
        [_wifi_ListadoVC showSelectedView];
    }
}

@end