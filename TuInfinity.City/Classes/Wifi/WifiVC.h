//
//  WifiVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 10/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WifiVC : UIViewController

- (void)configuraNavigationBarWithBtnModoVista:(BOOL)btnModoVistaVisible andBtnGeolocalizacion:(BOOL)btnGeolocalizacionVisible;

@end