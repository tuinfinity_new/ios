//
//  Wifi_ListadoVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 29/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"
#import "UIImageView+WebCache.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"
#import "MapViewAnnotation.h"
#import "UIImage_Misc.h"

#import "Wifi_ListadoCell.h"

#import "Empresas_DetalleVC.h"

#import "Wifi_ListadoVC.h"

@interface Wifi_ListadoVC () <UINavigationControllerDelegate, UIScrollViewDelegate, MKMapViewDelegate, ARLocationDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet UIView *containerView1;
@property (nonatomic, weak) IBOutlet UIView *containerView2;
@property (nonatomic, weak) IBOutlet UIView *containerView3;

@property (nonatomic, strong) Wifi_ListadoCell *prototypeCellEmpresa;

@property (nonatomic, weak) IBOutlet UIToolbar *toolBar;

@property (nonatomic, strong) NSMutableArray *listadoDatos_Categorias;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, readwrite) int itemsInicio;
@property (nonatomic, readwrite) int itemsNum;
@property (nonatomic, readwrite) BOOL itemsDownload;

@property (nonatomic, readwrite) float scrollOfsset;

@property (nonatomic, readwrite) NSUInteger mapLastZoomLevel;

@property (nonatomic, weak) IBOutlet UILabel *lblNoResultados;

@end

@implementation Wifi_ListadoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _originView.navigationController.delegate = self;
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _listadoDatos = [NSMutableArray array];
    _listadoDatos_Imagenes = [NSMutableArray array];
    _listadoDatos_CoordenadasMapa = [NSMutableArray array];
    _listadoDatos_CoordenadasAR = [NSMutableArray array];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width * 3, _scrollView.frame.size.height);
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"TABLEVIEW_REFRESCO", @"")
                                                                     attributes:[NSDictionary dictionaryWithObject:[UIFont fontWithName:@"Arial" size:20] forKey:NSFontAttributeName]];
    [refreshControl addTarget:self action:@selector(reloadDataTableview:) forControlEvents:UIControlEventValueChanged];
    
    [_tableView addSubview:refreshControl];
    
    _mapView.showsUserLocation = YES;
    _mapView.clusteringEnabled = YES;
    
    _previousSelectedView = 0;
    _selectedView = 1;
    [[_toolBar.items objectAtIndex:1] setSelected:YES];
    [[_toolBar.items objectAtIndex:3] setSelected:NO];
    [[_toolBar.items objectAtIndex:5] setSelected:NO];
    
    _listadoDatos_Categorias = [[NSMutableArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"ListadoCategorias" ofType:@"plist"]];

    _itemsInicio = 0;
    _itemsNum = 20;
    _itemsDownload = NO;
    
    [self showSelectedView];
    [self downloadData];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    _scrollView.frame = self.view.frame;
    _tableView.frame = self.view.frame;
    _mapView.frame = self.view.frame;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_activityIndicator stopAnimating];
    [self arView_Oculta];
    [[SDWebImageManager sharedManager] cancelAll];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Prototype cells

- (Wifi_ListadoCell *)prototypeCellEmpresa
{
    if ( !_prototypeCellEmpresa )
        _prototypeCellEmpresa = [_tableView dequeueReusableCellWithIdentifier:@"CellWifi_Listado"];
    return _prototypeCellEmpresa;
}

#pragma mark -
#pragma mark - Misc

- (void)showSelectedView
{
    [[_toolBar.items objectAtIndex:1] setSelected:NO];
    [[_toolBar.items objectAtIndex:3] setSelected:NO];
    [[_toolBar.items objectAtIndex:5] setSelected:NO];
    
    [_activityIndicator stopAnimating];
    
    if ( _previousSelectedView == 3 )
        [self arView_Oculta];
    
    switch ( _selectedView )
    {
        case 1:
            [[_toolBar.items objectAtIndex:1] setSelected:YES];
            [_scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
            break;
        case 2:
            [[_toolBar.items objectAtIndex:3] setSelected:YES];
            [_mapView setRegion:[_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(_datosAplicacion.latitud, _datosAplicacion.longitud), 4000, 4000)]];
            [self mapView_CargaCoordenadas];
            [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width, 0) animated:NO];
            break;
        case 3:
            [[_toolBar.items objectAtIndex:5] setSelected:YES];
            [self arView_Muestra];
            [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width * 2, 0) animated:NO];
            break;
    }
}

- (void)downloadData
{
    [_activityIndicator startAnimating];
    _lblNoResultados.hidden = YES;
    
    _itemsDownload = YES;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
    
    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager.securityPolicy setAllowInvalidCertificates:YES];
    [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
    [operationManager GET:[NSString stringWithFormat:@"empresa/%f/%f/%d/%d", _datosAplicacion.latitud, _datosAplicacion.longitud, _itemsInicio, _itemsNum]
               parameters:nil
                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                      NSMutableArray *receivedData = [[NSJSONSerialization JSONObjectWithData:[[operation responseString] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] mutableCopy];
                      
                      if ( [receivedData count] > 0 )
                      {
                          for ( int numItem = 0; numItem < [receivedData count]; numItem++ )
                          {
                              NSMutableDictionary *itemEmpresa = [[receivedData objectAtIndex:numItem] mutableCopy];
                              
                              if ( [itemEmpresa objectForKey:@"hasWifi"] != nil )
                              {
                                  if ( [[itemEmpresa objectForKey:@"hasWifi"] boolValue] )
                                  {
                                      [_listadoDatos addObject:itemEmpresa];
                                      
                                      int numItemInserted = (int)[_listadoDatos count] - 1;
                                      
                                      NSMutableDictionary *itemImagen = [[NSMutableDictionary alloc] init];
                                      
                                      [itemImagen setValue:[NSNull null] forKey:@"fotoImgLista"];
                                      [itemImagen setValue:[NSNull null] forKey:@"fotoImgLogo"];
                                      [itemImagen setValue:[NSNull null] forKey:@"fotoImgMapa"];
                                      [itemImagen setValue:[NSNull null] forKey:@"fotoImgRA"];
                                      [itemImagen setValue:[NSNumber numberWithBool:NO] forKey:@"loading"];
                                      
                                      [_listadoDatos_Imagenes addObject:itemImagen];
                                      
                                      MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:[itemEmpresa objectForKey:@"nombre"]
                                                                                                 andCoordinate:CLLocationCoordinate2DMake([[itemEmpresa objectForKey:@"latitud"] doubleValue], [[itemEmpresa objectForKey:@"longitud"] doubleValue])];
                                      annotation.tag = numItemInserted;
                                      annotation.imagePinData = UIImagePNGRepresentation([UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[[UIImage imageNamed:@"Img_NoFotoEmpresa"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] atPositionX:12 andPositionY:6]);
                                      [_listadoDatos_CoordenadasMapa addObject:annotation];
                                      
                                      NSDictionary *point = @{
                                                              @"id" : @(numItemInserted),
                                                              @"title" : [itemEmpresa objectForKey:@"nombre"],
                                                              @"lat" : @([[itemEmpresa objectForKey:@"latitud"] doubleValue]),
                                                              @"lon" : @([[itemEmpresa objectForKey:@"longitud"] doubleValue]),
                                                              @"dist" : @([_datosAplicacion.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[[itemEmpresa objectForKey:@"lat"] doubleValue] longitude:[[itemEmpresa objectForKey:@"lon"] doubleValue]]]),
                                                              @"image" : UIImagePNGRepresentation([UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroRA"] withFront:[UIImage roundedImage:[UIImage imageWithImage:[UIImage imageNamed:@"Img_NoFotoEmpresa"] scaledToSize:CGSizeMake(84, 83)] radius:180] atPositionX:24 andPositionY:14])
                                                              };
                                      [_listadoDatos_CoordenadasAR addObject:point];
                                      
                                      if ( [itemEmpresa objectForKey:@"id_logo"] != nil )
                                      {
                                          dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
                                          dispatch_async(queue, ^{
                                              UIImage *imgFoto = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[itemEmpresa objectForKey:@"id_logo"] objectForKey:@"foto_medium"]]]];
                                              dispatch_sync(dispatch_get_main_queue(), ^{
                                                  UIImage *imgFotoLogo = imgFoto;
                                                  UIImage *imgFotoMapa = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[UIImage imageWithImage:imgFoto scaledToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
                                                  UIImage *imgFotoRA = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroRA"] withFront:[UIImage roundedImage:[UIImage imageWithImage:imgFoto scaledToSize:CGSizeMake(84, 83)] radius:180] atPositionX:24 andPositionY:14];
                                                  
                                                  [[_listadoDatos_Imagenes objectAtIndex:numItemInserted] setObject:UIImagePNGRepresentation(imgFotoLogo) forKey:@"fotoImgLogo"];
                                                  [[_listadoDatos_Imagenes objectAtIndex:numItemInserted] setObject:UIImagePNGRepresentation(imgFotoMapa) forKey:@"fotoImgMapa"];
                                                  [[_listadoDatos_Imagenes objectAtIndex:numItemInserted] setObject:UIImagePNGRepresentation(imgFotoRA) forKey:@"fotoImgRA"];
                                                  
                                                  MapViewAnnotation *annotation = [_listadoDatos_CoordenadasMapa objectAtIndex:numItemInserted];
                                                  annotation.imagePinData = UIImagePNGRepresentation(imgFotoMapa);
                                                  [_listadoDatos_CoordenadasMapa replaceObjectAtIndex:numItemInserted withObject:annotation];
                                                  
                                                  NSMutableDictionary *point = [[_listadoDatos_CoordenadasAR objectAtIndex:numItemInserted] mutableCopy];
                                                  [point setValue:UIImagePNGRepresentation(imgFotoRA) forKeyPath:@"image"];
                                                  [_listadoDatos_CoordenadasAR replaceObjectAtIndex:numItemInserted withObject:point];
                                              });
                                          });
                                      }
                                  }
                              }
                          }
                          
                          _itemsInicio += _itemsNum;
                          [_tableView reloadData];
                      }
                      _itemsDownload = NO;
                      _lblNoResultados.hidden = ( [_listadoDatos count] > 0 ) ? YES : NO;
                      [_activityIndicator stopAnimating];
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error){
                      NSLog(@"%@",[error description]);
                      _lblNoResultados.hidden = ( [_listadoDatos count] > 0 ) ? YES : NO;
                      [_activityIndicator stopAnimating];
                  }];
}

- (IBAction)reloadDataTableview:(id)sender
{
    [sender endRefreshing];
    
    if ( !_itemsDownload )
    {
        [_listadoDatos removeAllObjects];
        [_listadoDatos_Imagenes removeAllObjects];
        [_listadoDatos_CoordenadasMapa removeAllObjects];
        [_listadoDatos_CoordenadasAR removeAllObjects];
        
        [_tableView reloadData];
        
        _itemsInicio = 0;
        _itemsNum = 20;
        
        [self downloadData];
    }
}

#pragma mark -
#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ( viewController == _originView )
        [self viewWillAppear:animated];
}

#pragma mark -
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ( (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height )
    {
        float scrollOfsset = ( scrollView.contentOffset.y + scrollView.frame.size.height ) - scrollView.contentSize.height;
        if ( scrollOfsset > 60.0 )
        {
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [activityIndicator startAnimating];
            activityIndicator.frame = CGRectMake(0, 0, 320, 44);
            _tableView.tableFooterView = activityIndicator;
            
            _scrollOfsset = scrollOfsset;
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ( !_itemsDownload )
    {
        if ( _scrollOfsset > 60.0 )
        {
            if ( [_listadoDatos count] > 0 )
                [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_listadoDatos count] - 1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            
            _tableView.tableFooterView = nil;
            
            [self downloadData];
        }
    }
    _scrollOfsset = 0;
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Wifi_ListadoCell class]] )
    {
        if ( [_listadoDatos_Imagenes count] > indexPath.row )
        {
            Wifi_ListadoCell *cellToConfigure = (Wifi_ListadoCell *)cell;
            
            NSMutableDictionary *itemEmpresa = [_listadoDatos objectAtIndex:indexPath.row];
            
            cellToConfigure.viewEmpresa.layer.shadowOffset = CGSizeMake(3,3);
            cellToConfigure.viewEmpresa.layer.shadowColor = [COLOR_NEGRO CGColor];
            cellToConfigure.viewEmpresa.layer.shadowRadius = 10.0;
            cellToConfigure.viewEmpresa.layer.shadowOpacity = 0.5;
            cellToConfigure.viewEmpresa.layer.shadowPath = [UIBezierPath bezierPathWithRect:cellToConfigure.viewEmpresa.bounds].CGPath;
            
            cellToConfigure.lblNombre.text = [itemEmpresa objectForKey:@"nombre"];
            
            if ( [[itemEmpresa objectForKey:@"subCategorias"] count] == 0 )
                cellToConfigure.lblActividad.text = @"";
            else
            {
                if ( [[_listadoDatos_Categorias filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"IdCategoria=%d", [[[itemEmpresa objectForKey:@"subCategorias"] objectAtIndex:0] intValue]]] count] == 0 )
                    cellToConfigure.lblActividad.text = @"";
                else
                    cellToConfigure.lblActividad.text = [[[_listadoDatos_Categorias filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"IdCategoria=%d", [[[itemEmpresa objectForKey:@"subCategorias"] objectAtIndex:0] intValue]]] objectAtIndex:0] objectForKey:@"Nombre"];
            }
            
            CLLocationDistance distance = [_datosAplicacion.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[[itemEmpresa objectForKey:@"latitud"] doubleValue] longitude:[[itemEmpresa objectForKey:@"longitud"] doubleValue]]];
            
            if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
                cellToConfigure.lblDistancia.text = ( (long)distance < 1000L ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (long)distance] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), distance / 1000.0];
            else
                cellToConfigure.lblDistancia.text = ( distance < POINT_ONE_MILE_METERS ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), distance * METERS_TO_FEET] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), distance * METERS_TO_MILES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCellEmpresa inTableView:tableView forRowAtIndexPath:indexPath];
    
    _prototypeCellEmpresa.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellEmpresa.bounds));
    [_prototypeCellEmpresa layoutIfNeeded];
    
    CGSize size = [_prototypeCellEmpresa.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1;

}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_listadoDatos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Wifi_ListadoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellWifi_Listado"];
    [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
    
    NSMutableDictionary *itemEmpresa = [_listadoDatos objectAtIndex:indexPath.row];
    NSMutableDictionary *itemImagen = [_listadoDatos_Imagenes objectAtIndex:indexPath.row];

    if ( [itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
        cell.imgFoto.image = [UIImage imageWithData:[itemImagen objectForKey:@"fotoImgLista"]];
    else
    {
        if ( [_listadoDatos_Imagenes count] > indexPath.row )
        {
            cell.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
            
            if ( [[itemEmpresa objectForKey:@"fotos"] count] > 0 )
            {
                NSString *urlImagenComprimida = [[NSString stringWithFormat:@"https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=%d&refresh=31536000&url=%@", (int)self.view.frame.size.width, [[[itemEmpresa objectForKey:@"fotos"] objectAtIndex:0] objectForKey:@"foto_medium"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                [cell.activityIndicator startAnimating];
                [cell.imgFoto sd_setImageWithURL:[NSURL URLWithString:urlImagenComprimida]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                           [cell.activityIndicator stopAnimating];
                                           if ( [_listadoDatos_Imagenes count] > indexPath.row )
                                               [[_listadoDatos_Imagenes objectAtIndex:indexPath.row] setValue:UIImagePNGRepresentation(image) forKey:@"fotoImgLista"];
                                       }];
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Empresas_DetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Empresas_Detalle"];
    [pantallaDestino setItemEmpresa:[_listadoDatos objectAtIndex:indexPath.row]];
    [pantallaDestino setItemImagen:[_listadoDatos_Imagenes objectAtIndex:indexPath.row]];
    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

#pragma mark -
#pragma mark MapView

- (NSUInteger)zoomLevelForMapRect:(MKMapRect)mRect withMapViewSizeInPixels:(CGSize)viewSizeInPixels
{
    NSUInteger zoomLevel = MAPKIT_MAXZOOMLEVEL;
    
    MKZoomScale zoomScale = mRect.size.width / viewSizeInPixels.width;
    double zoomExponent = log2(zoomScale);
    zoomLevel = (NSUInteger)(MAPKIT_MAXZOOMLEVEL - ceil(zoomExponent));
    
    return zoomLevel;
}

- (void)mapView_CargaCoordenadas
{
    [_mapView setRegion:[_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(_datosAplicacion.latitud, _datosAplicacion.longitud), 1500, 1500)]];
    _mapLastZoomLevel = [self zoomLevelForMapRect:_mapView.visibleMapRect withMapViewSizeInPixels:_mapView.frame.size];
    [_mapView removeAnnotations:_mapView.annotations];
    [_mapView addAnnotations:_listadoDatos_CoordenadasMapa];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ( [annotation isKindOfClass:[OCAnnotation class]] )
    {
        OCAnnotation *clusterAnnotation = (OCAnnotation *)annotation;
        
        MKAnnotationView *pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterView"];
        if ( !pinView )
        {
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterView"];
            
            pinView.canShowCallout = NO;
            pinView.draggable = NO;
        }
        
        pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"]
                                            withFront:[UIImage ImageWithBurnedText:[NSString stringWithFormat:@"%lu", (unsigned long)[clusterAnnotation.annotationsInCluster count]]
                                                                      withFontSize:22
                                                                      andFontColor:COLOR_AZUL1
                                                                           atImage:[[UIImage roundedImage:[UIImage imageNamed:@"Img_FondoTransparente"] radius:180] imageByScalingProportionallyToSize:CGSizeMake(42, 42)]]
                                          atPositionX:12
                                         andPositionY:10];
        
        return pinView;
    }
    else if ( [annotation isKindOfClass:[MapViewAnnotation class]] )
    {
        MapViewAnnotation *mapAnnotation = annotation;
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
        
        if ( !pinView )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
            
            pinView.animatesDrop = NO;
            pinView.canShowCallout = YES;
            pinView.draggable = NO;
            
            if ( [[_listadoDatos_Imagenes objectAtIndex:mapAnnotation.tag] objectForKey:@"fotoImgMapa"] != [NSNull null] )
                pinView.image = [UIImage imageWithData:mapAnnotation.imagePinData];
            else
                pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[[UIImage imageNamed:@"Img_NoFotoEmpresa"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
            
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [rightButton setTitle:annotation.title forState:UIControlStateNormal];
            [rightButton setTag:mapAnnotation.tag];
            
            pinView.rightCalloutAccessoryView = rightButton;
            pinView.tag = mapAnnotation.tag;
            
        }
        else pinView.annotation = annotation;
        
        return pinView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Empresas_DetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Empresas_Detalle"];
    [pantallaDestino setItemEmpresa:[_listadoDatos objectAtIndex:view.tag]];
    [pantallaDestino setItemImagen:[_listadoDatos_Imagenes objectAtIndex:view.tag]];
    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if ( _mapLastZoomLevel != [self zoomLevelForMapRect:_mapView.visibleMapRect withMapViewSizeInPixels:_mapView.frame.size] )
    {
        _mapLastZoomLevel = [self zoomLevelForMapRect:_mapView.visibleMapRect withMapViewSizeInPixels:_mapView.frame.size];
        if ( _mapView.region.span.longitudeDelta < 0.0075 )
            _mapView.clusteringEnabled = NO;
        else
        {
            _mapView.clusteringEnabled = YES;
            _mapView.clusterSize = _mapView.region.span.longitudeDelta * 0.25;
        }
    }
    else
    {
        [_mapView removeAnnotations:_mapView.annotations];
        [_mapView addAnnotations:_listadoDatos_CoordenadasMapa];
    }
    [_mapView doClustering];
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views;
{
}

#pragma mark -
#pragma mark - ARViewController

- (void)arView_Oculta
{
    for ( UIView *viewToRemove in [_containerView3 subviews] )
    {
        [viewToRemove removeFromSuperview];
    }
}

- (void)arView_Muestra
{
    [self arView_Oculta];
    
    _arView = nil;
    _arView = [[ARViewController alloc] initWithDelegate:self andFrame:CGRectMake(0, 0, _containerView3.frame.size.width, _containerView3.frame.size.height)];
    _arView.showsCloseButton = false;
    [_arView setRadarRange:10.0];
    [_arView setScaleViewsBasedOnDistance:YES];
    [_arView setOnlyShowItemsWithinRadarRange:YES];
    [_arView setRadarBackgroundColour:[UIColor colorWithRed:71.0/255.0 green:152.0/255.0 blue:176.0/255.0 alpha:.4]];
    [_arView setRadarViewportColour:[UIColor colorWithRed:71.0/255.0 green:152.0/255.0 blue:176.0/255.0 alpha:.8]];
    [_arView setRadarPointColour:COLOR_BLANCO];
    
    [_containerView3 addSubview:_arView.view];
}

#pragma mark -
#pragma mark - ARLocationDelegate

- (NSMutableArray *)geoLocations
{
    NSMutableArray *locationArray = [[NSMutableArray alloc] init];
    ARGeoCoordinate *arCoordinate;
    
    int numItem = 0;
    
    while ( ( [locationArray count] < 100 ) && ( numItem < [_listadoDatos_CoordenadasAR count] ) )
    {
        NSMutableDictionary *item = [_listadoDatos_CoordenadasAR objectAtIndex:numItem];
        
        arCoordinate = [ARGeoCoordinate coordinateWithLocation:[[CLLocation alloc] initWithLatitude:[[item objectForKey:@"lat"] doubleValue]
                                                                                          longitude:[[item objectForKey:@"lon"] doubleValue]]
                                              locationDistance:[[item objectForKey:@"dist"] doubleValue]
                                                 locationTitle:@""
                                                   locationTag:[[item objectForKey:@"id"] intValue]
                                                     iconImage:[UIImage imageWithData:[item objectForKey:@"image"]]];
        [locationArray addObject:arCoordinate];
        
        numItem++;
    }
    return locationArray;
}

- (void)locationClicked:(ARGeoCoordinate *)coordinate
{
    int idItem = (int)coordinate.tag;
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Empresas_DetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Empresas_Detalle"];
    [pantallaDestino setItemEmpresa:[_listadoDatos objectAtIndex:idItem]];
    [pantallaDestino setItemImagen:[_listadoDatos_Imagenes objectAtIndex:idItem]];
    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

@end