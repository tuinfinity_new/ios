//
//  Wifi_ListadoVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 29/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ARKit.h"
#import "OCMapView.h"

#import "WifiVC.h"

@interface Wifi_ListadoVC : UIViewController

@property (nonatomic, weak) IBOutlet WifiVC *originView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet OCMapView *mapView;
@property (nonatomic, strong) IBOutlet ARViewController *arView;

@property (nonatomic, strong) NSMutableArray *listadoDatos;
@property (nonatomic, strong) NSMutableArray *listadoDatos_Imagenes;
@property (nonatomic, strong) NSMutableArray *listadoDatos_CoordenadasMapa;
@property (nonatomic, strong) NSMutableArray *listadoDatos_CoordenadasAR;

@property (nonatomic, readwrite) int previousSelectedView;
@property (nonatomic, readwrite) int selectedView;

- (void)showSelectedView;

@end
