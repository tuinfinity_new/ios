//
//  CondicionesLegalesVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 27/08/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "CondicionesLegalesVC.h"

@interface CondicionesLegalesVC ()

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;

@property (nonatomic, weak) IBOutlet UIButton *btnVolver;

@property (nonatomic, weak) IBOutlet UIWebView *webView;

@end

@implementation CondicionesLegalesVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _lblTitulo.font = [UIFont fontWithName:@"Asenine" size:40];

    _btnVolver.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:16];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"CondicionesLegales" ofType:@"html"]]];
    [_webView loadRequest:urlRequest];
    _webView.scrollView.bounces = NO;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark -
#pragma mark UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_activityIndicator stopAnimating];
}

@end