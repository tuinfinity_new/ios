//
//  InicioVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"

#import "TutorialVC.h"

#import "InicioVC.h"

@interface InicioVC ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet UIImageView *imgFondo;

@end

@implementation InicioVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ( DEVICE_IPHONE )
    {
        if ( DEVICE_IPHONE_5 )
            [_imgFondo setImage:[UIImage imageNamed:@"Img_Inicio_iPhone2"]];
        else
            [_imgFondo setImage:[UIImage imageNamed:@"Img_Inicio_iPhone1"]];
    }
    else
        [_imgFondo setImage:[UIImage imageNamed:@"Img_Inicio_iPad"]];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if ( ![userDefaults objectForKey:@"usrIdUsuario"] )
        [self performSegueWithIdentifier:@"segLogin" sender:self];
    else
    {
        if ( ![userDefaults objectForKey:@"radioBusqueda"] )
            [userDefaults setObject:[NSNumber numberWithDouble:2500.0] forKey:@"radioBusqueda"];
        [userDefaults synchronize];

        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        if ( [[userDefaults objectForKey:@"tutorialActivo"] boolValue] )
        {
            TutorialVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Tutorial"];
            [pantallaDestino setOrigenInicioAplicacion:YES];
            [self presentViewController:pantallaDestino animated:NO completion:nil];
        }
        else
        {
            if ( [[userDefaults objectForKey:@"usrInvitado"] boolValue] )
            {
                _datosAplicacion.datosUsuario = nil;
                [self presentViewController:[storyboard instantiateViewControllerWithIdentifier:@"NavCupones"] animated:NO completion:nil];
            }
            else
            {
                [_activityIndicator startAnimating];
                
                NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
                
                AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
                [operationManager.securityPolicy setAllowInvalidCertificates:YES];
                [operationManager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
                [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
                [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
                [operationManager GET:[NSString stringWithFormat:@"usuario/%ld/perfil", [[userDefaults objectForKey:@"usrIdUsuario"] longValue]]
                           parameters:nil
                              success:^(AFHTTPRequestOperation *operation, id responseObject){
                                  [_activityIndicator stopAnimating];

                                  NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
                                  _datosAplicacion.datosUsuario = [receivedData mutableCopy];
                                  
                                  [self presentViewController:[storyboard instantiateViewControllerWithIdentifier:@"NavCupones"] animated:NO completion:nil];
                              }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                  [_activityIndicator stopAnimating];
                                  
                                  _datosAplicacion.datosUsuario = nil;
                                  [self presentViewController:[storyboard instantiateViewControllerWithIdentifier:@"NavCupones"] animated:NO completion:nil];
                              }];
            }
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end