//
//  RecuperarPasswordVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"

#import "Globales.h"

#import "RecuperarPasswordVC.h"

@interface RecuperarPasswordVC ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;

@property (nonatomic, weak) IBOutlet UITextField *txfEMail;

@property (nonatomic, weak) IBOutlet UIButton *btnEnviar;
@property (nonatomic, weak) IBOutlet UIButton *btnVolver;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;

@end

@implementation RecuperarPasswordVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _lblTitulo.font = [UIFont fontWithName:@"Asenine" size:40];

    _btnEnviar.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:20];
    _btnVolver.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:20];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnEnviar_Pressed:(id)sender
{
    [self.view endEditing:YES];
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *predicateMail = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];

    if ( ![predicateMail evaluateWithObject:_txfEMail.text] )
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:NSLocalizedString(@"RECUPERARPASSWORD_ERRORMAIL", @"")
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    else
    {
        [_activityIndicator startAnimating];
    
        NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
        [itemData setValue:[NSString stringWithString:_txfEMail.text] forKey:@"mailRestore"];

        NSURL *url = [NSURL URLWithString:@"http://www.tuinfinity.com/server/restorePass.php"];
        
        AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
        [operationManager POST:@""
                    parameters:itemData
                       success:^(AFHTTPRequestOperation *operation, id responseObject) {
                           [_activityIndicator stopAnimating];
                           [[[UIAlertView alloc] initWithTitle:@""
                                                       message:NSLocalizedString(@"RECUPERARPASSWORD_OK", nil)
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil] show];
                           
                           [self performSegueWithIdentifier:@"segVolver" sender:self];
                       }
                       failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                           [_activityIndicator stopAnimating];
                           [[[UIAlertView alloc] initWithTitle:@""
                                                       message:NSLocalizedString(@"RECUPERARPASSWORD_OK", nil)
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil] show];
                           
                           [self performSegueWithIdentifier:@"segVolver" sender:self];
                       }];
    }
}

#pragma mark -
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _tapGesture.enabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _tapGesture.enabled = NO;
}

#pragma mark -
#pragma mark - UIGestureRecognizerDelegate

- (IBAction)singleTapGesture:(id)sender
{
    [self.view endEditing:YES];
}

@end