//
//  LoginVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"

#import "Globales.h"

#import "LoginVC.h"

@interface LoginVC ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;

@property (nonatomic, weak) IBOutlet UITextField *txfLogin;
@property (nonatomic, weak) IBOutlet UITextField *txfPassword;

@property (nonatomic, weak) IBOutlet UILabel *lblRecuperarPassword;
@property (nonatomic, weak) IBOutlet UIButton *btnRecuperarPassword;

@property (nonatomic, weak) IBOutlet UIImageView *imgRecordarLogin;
@property (nonatomic, weak) IBOutlet UILabel *lblRecordarLogin;
@property (nonatomic, weak) IBOutlet UIButton *btnRecordarLogin;

@property (nonatomic, weak) IBOutlet UIButton *btnLogin;
@property (nonatomic, weak) IBOutlet UIButton *btnLoginInvitado;
@property (nonatomic, weak) IBOutlet UIButton *btnRegistro;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;

@end

@implementation LoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _lblTitulo.font = [UIFont fontWithName:@"Asenine" size:40];
    _lblRecordarLogin.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
    _lblRecuperarPassword.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
    
    _btnLogin.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:30];
    _btnLoginInvitado.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:16];
    _btnRegistro.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:20];

    [_btnRecordarLogin setSelected:NO];
    [_imgRecordarLogin setImage:[UIImage imageNamed:( _btnRecordarLogin.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
    
    _tapGesture.enabled = NO;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnRecuperarPassword_Pressed:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnRecordarLogin_Pressed:(id)sender
{
    [self.view endEditing:YES];
    [_btnRecordarLogin setSelected:!_btnRecordarLogin.selected];
    [_imgRecordarLogin setImage:[UIImage imageNamed:( _btnRecordarLogin.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
}

- (IBAction)btnRegistro_Pressed:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnLogin_Pressed:(id)sender
{
    [self.view endEditing:YES];

    if ( ( [_txfLogin.text length] == 0 ) || ( [_txfPassword.text length] == 0 ) )
    {
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:NSLocalizedString(@"INICIO_ERRORLOGIN", @"")
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
    else
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        [userDefaults setObject:_txfLogin.text forKey:@"usrLogin"];
        [userDefaults setObject:_txfPassword.text forKey:@"usrPassword"];
        [userDefaults setValue:[NSNumber numberWithBool:YES] forKeyPath:@"gelocalizacionActiva"];
        [userDefaults synchronize];

        [_activityIndicator startAnimating];
        
        NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
        AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
        [operationManager.securityPolicy setAllowInvalidCertificates:YES];
        [operationManager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
        [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
        [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
        [operationManager GET:@"usuario/login"
                   parameters:nil
                      success:^(AFHTTPRequestOperation *operation, id responseObject){
                          [_activityIndicator stopAnimating];
                          
                          NSString *receivedData = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                          long idUsuario = (long)[receivedData longLongValue];
                          
                          [userDefaults setObject:[NSNumber numberWithBool:NO] forKey:@"usrInvitado"];
                          [userDefaults setObject:[NSNumber numberWithLong:idUsuario] forKey:@"usrIdUsuario"];
                          [userDefaults setObject:[NSNumber numberWithBool:_btnRecordarLogin.selected] forKey:@"usrRecordarDatos"];
                          [userDefaults synchronize];
                          
                          [self performSegueWithIdentifier:@"segInicio" sender:self];
                      }
                      failure:^(AFHTTPRequestOperation *operation, NSError *error){
                          [_activityIndicator stopAnimating];

                          [userDefaults setObject:[NSNumber numberWithBool:NO] forKey:@"usrInvitado"];
                          [userDefaults setObject:@"" forKey:@"usrLogin"];
                          [userDefaults setObject:@"" forKey:@"usrPassword"];
                          [userDefaults synchronize];
                          
                          switch ( operation.response.statusCode )
                          {
                              case 403:
                                  [[[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"INICIO_ERRORLOGIN", nil)
                                                             delegate:self
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil] show];
                                  break;
                              default:
                                  [[[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"HTP_CONNECTIONERROR", nil)
                                                             delegate:self
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil] show];
                                  break;
                          }
                      }];
    }
}

- (IBAction)btnLoginInvitado_Pressed:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setObject:@"" forKey:@"usrLogin"];
    [userDefaults setObject:@"" forKey:@"usrPassword"];
    [userDefaults setObject:[NSNumber numberWithBool:YES] forKey:@"usrInvitado"];
    [userDefaults setObject:[NSNumber numberWithLong:0L] forKey:@"usrIdUsuario"];
    [userDefaults setObject:[NSNumber numberWithBool:NO] forKey:@"usrRecordarDatos"];
    [userDefaults synchronize];
    
    [self performSegueWithIdentifier:@"segInicio" sender:self];
}

#pragma mark -
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ( textField == _txfLogin )
        [_txfPassword becomeFirstResponder];
    
    return [textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _tapGesture.enabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _tapGesture.enabled = NO;
}

#pragma mark -
#pragma mark - UIGestureRecognizerDelegate

- (IBAction)singleTapGesture:(id)sender
{
    [self.view endEditing:YES];
}

@end