//
//  TutorialVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 10/09/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialVC : UIViewController

@property (nonatomic, readwrite) BOOL origenInicioAplicacion;

@end