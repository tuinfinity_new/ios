//
//  RegistroVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"

#import "Globales.h"

#import "RegistroVC.h"

@interface RegistroVC ()

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;

@property (nonatomic, weak) IBOutlet UITextField *txfNombre;
@property (nonatomic, weak) IBOutlet UITextField *txfEMail;
@property (nonatomic, weak) IBOutlet UITextField *txfPassword1;
@property (nonatomic, weak) IBOutlet UITextField *txfPassword2;

@property (nonatomic, weak) IBOutlet UILabel *lblCondicionesLegales;

@property (nonatomic, weak) IBOutlet UIImageView *imgCondicionesLegales;

@property (nonatomic, weak) IBOutlet UIButton *btnCondicionesLegales1;
@property (nonatomic, weak) IBOutlet UIButton *btnCondicionesLegales2;
@property (nonatomic, weak) IBOutlet UIButton *btnRegistro;
@property (nonatomic, weak) IBOutlet UIButton *btnVolver;

@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapGesture;

@property (nonatomic, retain) NSString *nombre;
@property (nonatomic, retain) NSString *eMail;
@property (nonatomic, retain) NSString *password1;
@property (nonatomic, retain) NSString *password2;

@end

@implementation RegistroVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    _lblTitulo.font = [UIFont fontWithName:@"Asenine" size:40];

    _btnRegistro.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:20];
    _btnVolver.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:16];

    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"REGISTRO_CONDICIONESLEGALES1", @""), NSLocalizedString(@"REGISTRO_CONDICIONESLEGALES2", @"")]];

    [attributedString addAttribute:NSFontAttributeName
                             value:[UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14]
                             range:NSMakeRange(0, [attributedString length])];
    
    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:COLOR_NEGRO
                             range:NSMakeRange(0, [NSLocalizedString(@"REGISTRO_CONDICIONESLEGALES1", @"") length])];

    [attributedString addAttribute:NSForegroundColorAttributeName
                             value:COLOR_AZUL1
                             range:NSMakeRange([NSLocalizedString(@"REGISTRO_CONDICIONESLEGALES1", @"") length] + 1, [NSLocalizedString(@"REGISTRO_CONDICIONESLEGALES2", @"") length])];

    [attributedString addAttribute:NSUnderlineStyleAttributeName
                             value:[NSNumber numberWithInt:NSUnderlineStyleSingle]
                             range:NSMakeRange([NSLocalizedString(@"REGISTRO_CONDICIONESLEGALES1", @"") length] + 1, [NSLocalizedString(@"REGISTRO_CONDICIONESLEGALES2", @"") length])];

    _lblCondicionesLegales.attributedText = attributedString;
    
    _btnCondicionesLegales1.selected = NO;
    [_imgCondicionesLegales setImage:[UIImage imageNamed:( _btnCondicionesLegales1.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];

    _tapGesture.enabled = NO;

    _nombre = @"";
    _eMail = @"";
    _password1 = @"";
    _password2 = @"";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _txfNombre.text = _nombre;
    _txfEMail.text = _eMail;
    _txfPassword1.text = _password1;
    _txfPassword2.text = _password2;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Misc

- (BOOL)compruebaDatosRegistro
{
    BOOL datosRegistroOk = NO;
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *predicateMail = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ( ( [_txfNombre.text length] == 0 ) || ( [_txfEMail.text length] == 0 ) || ( [_txfPassword1.text length] == 0 ) || ( [_txfPassword2.text length] == 0 ) )
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:NSLocalizedString(@"REGISTRO_ERRORDATOSINCOMPLETOS", @"")
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    else if ( ![predicateMail evaluateWithObject:_txfEMail.text] )
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:NSLocalizedString(@"REGISTRO_ERRORMAIL1", @"")
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    else if ( [_txfPassword1.text length]  < 5 )
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:NSLocalizedString(@"REGISTRO_ERRORPASSWORD1", @"")
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    else if ( ![_txfPassword1.text isEqualToString:_txfPassword2.text] )
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:NSLocalizedString(@"REGISTRO_ERRORPASSWORD2", @"")
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    else if ( !_btnCondicionesLegales1.selected )
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:NSLocalizedString(@"REGISTRO_ERRORCONDICIONESLEGALES", @"")
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    else
        datosRegistroOk = YES;
    
    return datosRegistroOk;
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnCondicionesLegales1_Pressed:(id)sender
{
    [self.view endEditing:YES];

    _btnCondicionesLegales1.selected = !_btnCondicionesLegales1.selected;
    [_imgCondicionesLegales setImage:[UIImage imageNamed:( _btnCondicionesLegales1.selected ) ? @"Img_Seleccion1ON" : @"Img_Seleccion1OFF"]];
}

- (IBAction)btnCondicionesLegales2_Pressed:(id)sender
{
    [self.view endEditing:YES];
    _nombre = _txfNombre.text;
    _eMail = _txfEMail.text;
    _password1 = _txfPassword1.text;
    _password2 = _txfPassword2.text;
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    [self presentViewController:[storyboard instantiateViewControllerWithIdentifier:@"CondicionesLegales"] animated:NO completion:nil];
}

- (IBAction)btnRegistro_Pressed:(id)sender
{
    [self.view endEditing:YES];
    
    if ( [self compruebaDatosRegistro ] )
    {
        [_activityIndicator startAnimating];

        NSMutableDictionary *itemData = [[NSMutableDictionary alloc] init];
        [itemData setValue:[NSString stringWithString:_txfNombre.text] forKey:@"nombre"];
        [itemData setValue:[NSString stringWithString:_txfEMail.text] forKey:@"email"];
        [itemData setValue:[NSString stringWithString:_txfPassword1.text] forKey:@"pass"];

        NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTP];
        
        AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
        [operationManager.securityPolicy setAllowInvalidCertificates:YES];
        operationManager.requestSerializer = [AFJSONRequestSerializer serializer];
        operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [operationManager POST:@"usuario"
                    parameters:itemData
                       success:^(AFHTTPRequestOperation *operation, id responseObject){
                           NSString *receivedData = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                           long idUsuario = (long)[receivedData longLongValue];

                           [_activityIndicator stopAnimating];
                           [[[UIAlertView alloc] initWithTitle:@""
                                                       message:NSLocalizedString(@"REGISTRO_OK", @"")
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil] show];
                           
                           NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

                           [userDefaults setObject:_txfEMail.text forKey:@"usrLogin"];
                           [userDefaults setObject:_txfPassword1.text forKey:@"usrPassword"];
                           [userDefaults setObject:[NSNumber numberWithBool:NO] forKey:@"usrInvitado"];
                           [userDefaults setObject:[NSNumber numberWithLong:idUsuario] forKey:@"usrIdUsuario"];
                           [userDefaults setObject:[NSNumber numberWithBool:NO] forKey:@"usrRecordarDatos"];
                           [userDefaults setValue:[NSNumber numberWithBool:YES] forKeyPath:@"gelocalizacionActiva"];
                           [userDefaults synchronize];
                           
                           [self performSegueWithIdentifier:@"segInicio" sender:self];
                       }
                       failure:^(AFHTTPRequestOperation *operation, NSError *error){
                           [_activityIndicator stopAnimating];
                           switch ( operation.response.statusCode )
                           {
                               case 409:
                                   [[[UIAlertView alloc] initWithTitle:@""
                                                               message:NSLocalizedString(@"REGISTRO_ERRORMAIL2", @"")
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil] show];
                                   break;
                               default:
                                   [[[UIAlertView alloc] initWithTitle:@""
                                                               message:NSLocalizedString(@"HTP_CONNECTIONERROR", nil)
                                                              delegate:self
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil] show];
                                   break;
                           }
                       }];
    }
}

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.view endEditing:YES];
}

#pragma mark -
#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    int nextTag = (int)textField.tag + 1;
    UIResponder *nextResponder = [self.view viewWithTag:nextTag];
    
    if ( nextResponder )
        [nextResponder becomeFirstResponder];
    else
        [textField resignFirstResponder];

    return [textField resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _tapGesture.enabled = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    _tapGesture.enabled = NO;
}

#pragma mark -
#pragma mark - UIGestureRecognizerDelegate

- (IBAction)singleTapGesture:(id)sender
{
    [self.view endEditing:YES];
}

@end