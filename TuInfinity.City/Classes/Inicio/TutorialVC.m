//
//  TutorialVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 10/09/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "Globales.h"

#import "TutorialVC.h"

@interface TutorialVC ()

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet UIPageControl *pageControl;

@property (nonatomic, readwrite) BOOL pageControlBeingUsed;

@property (nonatomic, weak) IBOutlet UILabel *lblTutorial1_1;
@property (nonatomic, weak) IBOutlet UILabel *lblTutorial1_2;
@property (nonatomic, weak) IBOutlet UILabel *lblTutorial1_3;
@property (nonatomic, weak) IBOutlet UILabel *lblTutorial2_1;
@property (nonatomic, weak) IBOutlet UILabel *lblTutorial2_2;
@property (nonatomic, weak) IBOutlet UILabel *lblTutorial3_1;
@property (nonatomic, weak) IBOutlet UILabel *lblTutorial3_2;
@property (nonatomic, weak) IBOutlet UILabel *lblTutorial3_3;
@property (nonatomic, weak) IBOutlet UILabel *lblTutorial4_1;
@property (nonatomic, weak) IBOutlet UILabel *lblTutorial4_2;
@property (nonatomic, weak) IBOutlet UILabel *lblTutorial4_3;

@end

@implementation TutorialVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSMutableParagraphStyle *paragrahStyleL = [[NSMutableParagraphStyle alloc] init];
    [paragrahStyleL setMinimumLineHeight:25];
    [paragrahStyleL setAlignment:NSTextAlignmentLeft];

    NSMutableParagraphStyle *paragrahStyleC = [[NSMutableParagraphStyle alloc] init];
    [paragrahStyleC setMinimumLineHeight:25];
    [paragrahStyleC setAlignment:NSTextAlignmentCenter];

    NSMutableParagraphStyle *paragrahStyleR = [[NSMutableParagraphStyle alloc] init];
    [paragrahStyleR setMinimumLineHeight:25];
    [paragrahStyleR setAlignment:NSTextAlignmentRight];

    NSMutableAttributedString *tutorial1_1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@%@%@", NSLocalizedString(@"TUTORIAL_PAGINA1_1_1", @""), NSLocalizedString(@"TUTORIAL_PAGINA1_1_2", @""), NSLocalizedString(@"TUTORIAL_PAGINA1_1_3", @"")]];
    NSMutableAttributedString *tutorial1_2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TUTORIAL_PAGINA1_2", @"")];
    NSMutableAttributedString *tutorial1_3 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TUTORIAL_PAGINA1_3", @"")];
    NSMutableAttributedString *tutorial2_1 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TUTORIAL_PAGINA2_1", @"")];
    NSMutableAttributedString *tutorial2_2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TUTORIAL_PAGINA2_2", @"")];
    NSMutableAttributedString *tutorial3_1 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TUTORIAL_PAGINA3_1", @"")];
    NSMutableAttributedString *tutorial3_2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TUTORIAL_PAGINA3_2", @"")];
    NSMutableAttributedString *tutorial3_3 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TUTORIAL_PAGINA3_3", @"")];
    NSMutableAttributedString *tutorial4_1 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TUTORIAL_PAGINA4_1", @"")];
    NSMutableAttributedString *tutorial4_2 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TUTORIAL_PAGINA4_2", @"")];
    NSMutableAttributedString *tutorial4_3 = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"TUTORIAL_PAGINA4_3", @"")];
    
    [tutorial1_1 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:18] range:NSMakeRange(0, [tutorial1_1 length])];
    [tutorial1_1 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Asenine" size:36] range:NSMakeRange([NSLocalizedString(@"TUTORIAL_PAGINA1_1_1", @"") length], [NSLocalizedString(@"TUTORIAL_PAGINA1_1_2", @"") length])];
    [tutorial1_2 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:18] range:NSMakeRange(0, [tutorial1_2 length])];
    [tutorial1_3 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:18] range:NSMakeRange(0, [tutorial1_3 length])];
    [tutorial2_1 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:16] range:NSMakeRange(0, [tutorial2_1 length])];
    [tutorial2_2 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:16] range:NSMakeRange(0, [tutorial2_2 length])];
    [tutorial3_1 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:16] range:NSMakeRange(0, [tutorial3_1 length])];
    [tutorial3_2 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:16] range:NSMakeRange(0, [tutorial3_2 length])];
    [tutorial3_3 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:16] range:NSMakeRange(0, [tutorial3_3 length])];
    [tutorial4_1 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:16] range:NSMakeRange(0, [tutorial4_1 length])];
    [tutorial4_2 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:16] range:NSMakeRange(0, [tutorial4_2 length])];
    [tutorial4_3 addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Hand Of Sean (Demo)" size:16] range:NSMakeRange(0, [tutorial4_3 length])];

    [tutorial1_1 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleC range:NSMakeRange(0, [tutorial1_1 length])];
    [tutorial1_2 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleL range:NSMakeRange(0, [tutorial1_2 length])];
    [tutorial1_3 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleL range:NSMakeRange(0, [tutorial1_3 length])];
    [tutorial2_1 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleL range:NSMakeRange(0, [tutorial2_1 length])];
    [tutorial2_2 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleL range:NSMakeRange(0, [tutorial2_2 length])];
    [tutorial3_1 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleR range:NSMakeRange(0, [tutorial3_1 length])];
    [tutorial3_2 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleR range:NSMakeRange(0, [tutorial3_2 length])];
    [tutorial3_3 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleR range:NSMakeRange(0, [tutorial3_3 length])];
    [tutorial4_1 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleL range:NSMakeRange(0, [tutorial4_1 length])];
    [tutorial4_2 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleC range:NSMakeRange(0, [tutorial4_2 length])];
    [tutorial4_3 addAttribute:NSParagraphStyleAttributeName value:paragrahStyleR range:NSMakeRange(0, [tutorial4_3 length])];
    
    _lblTutorial1_1.attributedText = tutorial1_1;
    _lblTutorial1_2.attributedText = tutorial1_2;
    _lblTutorial1_3.attributedText = tutorial1_3;
    _lblTutorial2_1.attributedText = tutorial2_1;
    _lblTutorial2_2.attributedText = tutorial2_2;
    _lblTutorial3_1.attributedText = tutorial3_1;
    _lblTutorial3_2.attributedText = tutorial3_2;
    _lblTutorial3_3.attributedText = tutorial3_3;
    _lblTutorial4_1.attributedText = tutorial4_1;
    _lblTutorial4_2.attributedText = tutorial4_2;
    _lblTutorial4_3.attributedText = tutorial4_3;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width * 4, _scrollView.frame.size.height)];
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width * _pageControl.currentPage, 0) animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnCerrarTutorial_Pressed:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:[NSNumber numberWithBool:NO] forKeyPath:@"tutorialActivo"];
    [userDefaults synchronize];
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    if ( _origenInicioAplicacion )
        [self presentViewController:[storyboard instantiateViewControllerWithIdentifier:@"Inicio"] animated:NO completion:nil];
    else
        [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark -
#pragma mark - UIPageControl

- (IBAction)pageControl_ValueChanged:(id)sender
{
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width * _pageControl.currentPage, 0) animated:YES];
    _pageControlBeingUsed = YES;
}

#pragma mark -
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
	if ( !_pageControlBeingUsed )
    {
		CGFloat pageWidth = _scrollView.frame.size.width;
		int page = floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        if ( _pageControl.currentPage != page )
        {
            _pageControl.currentPage = page;
            [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width * _pageControl.currentPage, 0) animated:YES];
        }
	}
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
	_pageControlBeingUsed = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	_pageControlBeingUsed = NO;
}

@end