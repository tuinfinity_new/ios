//
//  Lugares_DetalleVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 16/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Lugares_ListadoVC.h"

@interface Lugares_DetalleVC : UIViewController

@property (nonatomic, weak) IBOutlet Lugares_ListadoVC *originView;

@property (nonatomic, readwrite) long numItem;

@end
