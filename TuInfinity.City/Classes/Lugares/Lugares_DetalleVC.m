//
//  Lugares_DetalleVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 16/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "Globales.h"

#import "UIImage_Misc.h"

#import "Lugares_ComoLlegarVC.h"
#import "Lugares_DetalleWebVC.h"

#import "Lugares_DetalleVC.h"

@interface Lugares_DetalleVC ()

@property (nonatomic, weak) IBOutlet UIImageView *imgFoto;
@property (nonatomic, weak) IBOutlet UIImageView *imgNoFoto;

@property (nonatomic, weak) IBOutlet UILabel *lblNombre;
@property (nonatomic, weak) IBOutlet UILabel *lblDistancia;

@property (nonatomic, weak) IBOutlet UIButton *btnMasInformacion;
@property (nonatomic, weak) IBOutlet UIButton *btnWikipedia;
@property (nonatomic, weak) IBOutlet UIButton *btnComoLlegar;
@property (nonatomic, weak) IBOutlet UIButton *btnRuta;

@property (nonatomic, weak) IBOutlet UILabel *lblBtnMasInformacion;
@property (nonatomic, weak) IBOutlet UILabel *lblBtnWikipedia;
@property (nonatomic, weak) IBOutlet UILabel *lblBtnComoLlegar;
@property (nonatomic, weak) IBOutlet UILabel *lblBtnRuta;

@end

@implementation Lugares_DetalleVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configuraNavigationBar];
    
    NSMutableDictionary *item = [_originView.listadoDatosMostrados objectAtIndex:_numItem];
    NSMutableDictionary *itemImagen = [_originView.listadoDatos_Imagenes objectAtIndex:_numItem];

    _lblNombre.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:16];
    _lblBtnMasInformacion.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:14];
    _lblBtnWikipedia.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:14];
    _lblBtnComoLlegar.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:14];
    _lblBtnRuta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:14];

    _lblNombre.text = [item objectForKey:@"name"];
    
    if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
        _lblDistancia.text = ( (long)[[item objectForKey:@"dist"] doubleValue] < 1000L ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (long)[[item objectForKey:@"dist"] doubleValue]] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), [[item objectForKey:@"dist"] doubleValue] / 1000.0];
    else
        _lblDistancia.text = ( [[item objectForKey:@"dist"] doubleValue] < POINT_ONE_MILE_METERS ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), [[item objectForKey:@"dist"] doubleValue] * METERS_TO_FEET] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), [[item objectForKey:@"dist"] doubleValue] * METERS_TO_MILES];

    _btnMasInformacion.enabled = ( [[item objectForKey:@"registrant_url"] length] > 0 ) ? YES : NO;
    _btnWikipedia.enabled = ( [[item objectForKey:@"monument_article"] length] > 0 ) ? YES : NO;
    
    _lblBtnRuta.text = NSLocalizedString(@"RUTA_ADD", @"");

    if ( [itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
        _imgFoto.image = [UIImage imageWithData:[itemImagen objectForKey:@"fotoImgLista"]];
    else
    {
        _imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
        if ( [[item objectForKey:@"image"] length] == 0 )
            _imgNoFoto.hidden = NO;
        else
        {
            if ( [[item objectForKey:@"image"] length] > 0 )
            {
                dispatch_queue_t queueURL = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
                dispatch_async(queueURL, ^{
                    NSURL *urlConsulta = [NSURL URLWithString:[[NSString stringWithFormat:@"http://commons.wikimedia.org/w/api.php?action=query&list=allimages&aiprop=url&format=json&ailimit=1&aifrom=%@", [item objectForKey:@"image"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                    NSData *responseData = [NSData dataWithContentsOfURL:urlConsulta];
                    
                    if ( [responseData length] > 0 )
                    {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:nil];
                            
                            dispatch_queue_t queueImage = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
                            dispatch_async(queueImage, ^{
                                NSURL *urlImagen = [NSURL URLWithString:[[[[receivedData objectForKey:@"query"] objectForKey:@"allimages"] objectAtIndex:0] objectForKey:@"url"]];
                                
                                NSData *imageData = [NSData dataWithContentsOfURL:urlImagen];
                                dispatch_sync(dispatch_get_main_queue(), ^{
                                    UIImage *imgFotoLista = [UIImage imageWithImage:[UIImage imageWithData:imageData] scaledToWidth:self.view.frame.size.width];
                                    UIImage *imgFotoMapa = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[UIImage imageWithImage:[UIImage imageWithData:imageData] scaledToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
                                    UIImage *imgFotoRA = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroRA"] withFront:[UIImage roundedImage:[UIImage imageWithImage:[UIImage imageWithData:imageData] scaledToSize:CGSizeMake(84, 83)] radius:180] atPositionX:24 andPositionY:14];

                                    [[_originView.listadoDatos_Imagenes objectAtIndex:_numItem] setValue:UIImagePNGRepresentation(imgFotoLista) forKey:@"fotoImgLista"];
                                    [[_originView.listadoDatos_Imagenes objectAtIndex:_numItem] setValue:UIImagePNGRepresentation(imgFotoMapa) forKey:@"fotoImgMapa"];
                                    [[_originView.listadoDatos_Imagenes objectAtIndex:_numItem] setValue:UIImagePNGRepresentation(imgFotoRA) forKey:@"fotoImgRA"];
                                    
                                    _imgFoto.image = [UIImage imageWithData:[itemImagen objectForKey:@"fotoImgLista"]];
                                });
                            });
                        });
                    }
                    else
                        _imgNoFoto.hidden = NO;
                });
            }
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.tintColor = COLOR_AZUL1;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnMasInformacion_Pressed:(id)sender
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Lugares_DetalleWebVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Lugares_DetalleWeb"];
    [pantallaDestino setUrlDestino:[[NSString stringWithFormat:@"%@", [[_originView.listadoDatosMostrados objectAtIndex:_numItem] objectForKey:@"registrant_url"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [self.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnWikipedia_Pressed:(id)sender
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Lugares_DetalleWebVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Lugares_DetalleWeb"];
    [pantallaDestino setUrlDestino:[[NSString stringWithFormat:@"https://%@.wikipedia.org/wiki/%@", [[_originView.listadoDatosMostrados objectAtIndex:_numItem] objectForKey:@"lang"], [[_originView.listadoDatosMostrados objectAtIndex:_numItem] objectForKey:@"monument_article"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [self.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnComoLlegar_Pressed:(id)sender
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;

    Lugares_ComoLlegarVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Lugares_ComoLlegar"];
    [pantallaDestino setLatitud:[[[_originView.listadoDatosMostrados objectAtIndex:_numItem] objectForKey:@"lat"] doubleValue]];
    [pantallaDestino setLongitud:[[[_originView.listadoDatosMostrados objectAtIndex:_numItem] objectForKey:@"lon"] doubleValue]];
    [pantallaDestino setItemImagen:[_originView.listadoDatos_Imagenes objectAtIndex:_numItem]];
    [self.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnRuta_Pressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@""
                                message:NSLocalizedString(@"OPCION_PROXIMAMENTEDISPONIBLE", @"")
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

@end