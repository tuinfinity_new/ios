//
//  Lugares_DetalleWebVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 30/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Lugares_DetalleWebVC : UIViewController

@property (nonatomic, readwrite) NSString *urlDestino;

@end
