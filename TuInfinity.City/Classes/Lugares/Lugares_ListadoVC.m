//
//  Lugares_ListadoVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 16/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"
#import "SDWebImageDownloader.h"
#import "UIImageView+WebCache.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"
#import "MapViewAnnotation.h"
#import "UIImage_Misc.h"

#import "Lugares_ListadoCell.h"

#import "Lugares_DetalleVC.h"
#import "Lugares_ComoLlegarVC.h"
#import "Lugares_DetalleWebVC.h"

#import "Lugares_ListadoVC.h"

@interface Lugares_ListadoVC () <UINavigationControllerDelegate, MKMapViewDelegate, ARLocationDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet UIView *containerView1;
@property (nonatomic, weak) IBOutlet UIView *containerView2;
@property (nonatomic, weak) IBOutlet UIView *containerView3;

@property (nonatomic, weak) IBOutlet UIToolbar *toolBar;

@property (nonatomic, strong) Lugares_ListadoCell *prototypeCellLugar;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, readwrite) int itemsInicio;
@property (nonatomic, readwrite) int itemsNum;
@property (nonatomic, readwrite) BOOL itemsDownload;

@property (nonatomic, readwrite) float scrollOfsset;

@property (nonatomic, readwrite) NSUInteger mapLastZoomLevel;

@property (nonatomic, weak) IBOutlet UILabel *lblNoResultados;

@property (nonatomic, readwrite) BOOL coordinatesLoaded;

@end

@implementation Lugares_ListadoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _originView.navigationController.delegate = self;
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _listadoDatosTotal = [NSMutableArray array];
    _listadoDatosMostrados = [NSMutableArray array];
    _listadoDatos_Imagenes = [NSMutableArray array];
    _listadoDatos_CoordenadasMapa = [NSMutableArray array];
    _listadoDatos_CoordenadasAR = [NSMutableArray array];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width * 3, _scrollView.frame.size.height);
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"TABLEVIEW_REFRESCO", @"")
                                                                     attributes:[NSDictionary dictionaryWithObject:[UIFont fontWithName:@"Arial" size:20] forKey:NSFontAttributeName]];
    [refreshControl addTarget:self action:@selector(reloadDataTableview:) forControlEvents:UIControlEventValueChanged];
    
    [_tableView addSubview:refreshControl];
    
    _mapView.showsUserLocation = YES;
    _mapView.clusteringEnabled = YES;
    
    _previousSelectedView = 0;
    _selectedView = 1;
    [[_toolBar.items objectAtIndex:1] setSelected:YES];
    [[_toolBar.items objectAtIndex:3] setSelected:NO];
    [[_toolBar.items objectAtIndex:5] setSelected:NO];

    _itemsInicio = 0;
    _itemsNum = 50;
    _itemsDownload = NO;

    [self showSelectedView];
    [self downloadData];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    _scrollView.frame = self.view.frame;
    _tableView.frame = self.view.frame;
    _mapView.frame = self.view.frame;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [_activityIndicator stopAnimating];
    [self arView_Oculta];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Prototype cells

- (Lugares_ListadoCell *)prototypeCellLugar
{
    if ( !_prototypeCellLugar )
        _prototypeCellLugar = [_tableView dequeueReusableCellWithIdentifier:@"CellLugares_Listado"];
    return _prototypeCellLugar;
}

#pragma mark -
#pragma mark - Misc

- (void)downloadImages
{
    if ( _itemsInicio < [_listadoDatosTotal count] )
    {
        int numItem = _itemsInicio;
        while ( numItem < ( _itemsInicio + _itemsNum ) && ( numItem < [_listadoDatosTotal count] ) )
        {
            NSMutableDictionary *item = [_listadoDatosTotal objectAtIndex:numItem];
            [_listadoDatosMostrados addObject:item];
            
            NSMutableDictionary *itemImagen = [[NSMutableDictionary alloc] init];
            
            [itemImagen setValue:[NSNull null] forKey:@"fotoImgLista"];
            [itemImagen setValue:[NSNull null] forKey:@"fotoImgMapa"];
            [itemImagen setValue:[NSNull null] forKey:@"fotoImgRA"];
            [itemImagen setValue:[NSNumber numberWithBool:NO] forKey:@"loading"];
            
            [_listadoDatos_Imagenes addObject:itemImagen];
            
            if ( [[item objectForKey:@"image"] length] > 0 )
            {
                if ( ![[[_listadoDatosMostrados objectAtIndex:numItem] objectForKey:@"loading"] boolValue] )
                {
                    [[_listadoDatos_Imagenes objectAtIndex:numItem] setValue:[NSNumber numberWithBool:YES] forKey:@"loading"];
                    
                    NSURL *urlConsulta = [NSURL URLWithString:[[NSString stringWithFormat:@"http://commons.wikimedia.org/w/api.php?action=query&list=allimages&aiprop=url&format=json&ailimit=1&aifrom=%@", [item objectForKey:@"image"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                    
                    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:urlConsulta];
                    [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
                    
                    [operationManager GET:@""
                               parameters:nil
                                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                                      NSDictionary *receivedData = [NSJSONSerialization JSONObjectWithData:operation.responseData options:0 error:nil];
                                      
                                      NSString *urlImagen = [[[[receivedData objectForKey:@"query"] objectForKey:@"allimages"] objectAtIndex:0] objectForKey:@"url"];
                                      NSString *urlImagenComprimida = [[NSString stringWithFormat:@"https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=%d&refresh=31536000&url=%@", (int)self.view.frame.size.width, urlImagen] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

                                      dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
                                      dispatch_async(queue, ^{
                                          UIImage *imgFoto = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlImagenComprimida]]];
                                          dispatch_sync(dispatch_get_main_queue(), ^{
                                              if ( [_listadoDatos_Imagenes count] > numItem )
                                              {
                                                  UIImage *imgFotoLista = [UIImage imageWithImage:imgFoto scaledToWidth:self.view.frame.size.width];
                                                  UIImage *imgFotoMapa = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[UIImage imageWithImage:imgFoto scaledToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
                                                  UIImage *imgFotoRA = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroRA"] withFront:[UIImage roundedImage:[UIImage imageWithImage:imgFoto scaledToSize:CGSizeMake(84, 83)] radius:180] atPositionX:24 andPositionY:14];
                                                  
                                                  [[_listadoDatos_Imagenes objectAtIndex:numItem] setValue:[NSNumber numberWithBool:NO] forKey:@"loading"];
                                                  [[_listadoDatos_Imagenes objectAtIndex:numItem] setValue:UIImagePNGRepresentation(imgFotoLista) forKey:@"fotoImgLista"];
                                                  [[_listadoDatos_Imagenes objectAtIndex:numItem] setValue:UIImagePNGRepresentation(imgFotoMapa) forKey:@"fotoImgMapa"];
                                                  [[_listadoDatos_Imagenes objectAtIndex:numItem] setValue:UIImagePNGRepresentation(imgFotoRA) forKey:@"fotoImgRA"];

                                                  MapViewAnnotation *annotation = [_listadoDatos_CoordenadasMapa objectAtIndex:numItem];
                                                  annotation.imagePinData = UIImagePNGRepresentation(imgFotoMapa);
                                                  [_listadoDatos_CoordenadasMapa replaceObjectAtIndex:numItem withObject:annotation];
                                                  
                                                  if ( _selectedView == 2 )
                                                  {
                                                      [_mapView removeAnnotations:_mapView.annotations];
                                                      [_mapView addAnnotations:_listadoDatos_CoordenadasMapa];
                                                  }

                                                  NSMutableDictionary *point = [[_listadoDatos_CoordenadasAR objectAtIndex:numItem] mutableCopy];
                                                  [point setValue:UIImagePNGRepresentation(imgFotoRA) forKeyPath:@"image"];
                                                  [_listadoDatos_CoordenadasAR replaceObjectAtIndex:numItem withObject:point];
                                                  
                                                  [_tableView beginUpdates];
                                                  [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:numItem inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
                                                  [_tableView endUpdates];
                                              }
                                          });
                                      });
                                  }
                                  failure:^(AFHTTPRequestOperation *operation, NSError *error){
                                  }];
                }
            }

            numItem++;
        }
        _itemsInicio += _itemsNum;

        [_tableView reloadData];
    }
}

- (void)downloadData
{
    _lblNoResultados.hidden = YES;
    
    [_activityIndicator startAnimating];

    _itemsDownload = YES;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://toolserver.org/~erfgoed/api/api.php?action=search&limit=10000&format=json&coord=%f,%f&radius=%f", _datosAplicacion.latitud, _datosAplicacion.longitud, [[userDefaults objectForKey:@"radioBusqueda"] doubleValue]]];

    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [operationManager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    
    [operationManager GET:@""
               parameters:nil
                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                      if ( [[NSJSONSerialization JSONObjectWithData:[[operation responseString] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] count] > 0 )
                      {
                          NSMutableArray *receivedData = [[NSJSONSerialization JSONObjectWithData:[[operation responseString] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] objectForKey:@"monuments"];
                          
                          if ( [receivedData count] > 0 )
                          {
                              [_listadoDatosTotal addObjectsFromArray:receivedData];
                              
                              _coordinatesLoaded = NO;
                              dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
                              dispatch_async(queue, ^{
                                  for ( int numItem = 0; numItem < [_listadoDatosTotal count]; numItem++ )
                                  {
                                      NSMutableDictionary *item = [_listadoDatosTotal objectAtIndex:numItem];
                                      
                                      MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:[item objectForKey:@"name"]
                                                                                                 andCoordinate:CLLocationCoordinate2DMake([[item objectForKey:@"lat"] doubleValue], [[item objectForKey:@"lon"] doubleValue])];
                                      annotation.tag = numItem;
                                      annotation.imagePinData = UIImagePNGRepresentation([UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[[UIImage imageNamed:@"Img_NoFotoMonumento"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] atPositionX:12 andPositionY:6]);
                                      [_listadoDatos_CoordenadasMapa addObject:annotation];
                                      
                                      NSDictionary *point = @{
                                                              @"id" : @(numItem),
                                                              @"title" : [item objectForKey:@"name"],
                                                              @"lat" : @([[item objectForKey:@"lat"] doubleValue]),
                                                              @"lon" : @([[item objectForKey:@"lon"] doubleValue]),
                                                              @"dist" : @([_datosAplicacion.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[[item objectForKey:@"lat"] doubleValue] longitude:[[item objectForKey:@"lon"] doubleValue]]]),
                                                              @"image" : UIImagePNGRepresentation([UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroRA"] withFront:[UIImage roundedImage:[UIImage imageWithImage:[UIImage imageNamed:@"Img_NoFotoMonumento"] scaledToSize:CGSizeMake(84, 83)] radius:180] atPositionX:24 andPositionY:14])
                                                              };
                                      [_listadoDatos_CoordenadasAR addObject:point];
                                  }
                                  _coordinatesLoaded = YES;
                              });
                              
                              _itemsDownload = NO;
                              
                              [_tableView reloadData];
                              [self downloadImages];
                          }
                      }
                      [_activityIndicator stopAnimating];
                      _lblNoResultados.hidden = ( [_listadoDatosMostrados count] > 0 ) ? YES : NO;
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error){
                      NSLog(@"%@",[error description]);
                      _itemsDownload = NO;
                      [_activityIndicator stopAnimating];
                  }];
}

- (IBAction)reloadDataTableview:(id)sender
{
    [sender endRefreshing];
    
    if ( !_itemsDownload )
    {
        [_listadoDatosTotal removeAllObjects];
        [_listadoDatosMostrados removeAllObjects];
        [_listadoDatos_Imagenes removeAllObjects];
        [_listadoDatos_CoordenadasMapa removeAllObjects];
        [_listadoDatos_CoordenadasAR removeAllObjects];
        
        [_tableView reloadData];
        
        _itemsInicio = 0;
        _itemsNum = 50;

        [self downloadData];
    }
}

- (void)showSelectedView
{
    [[_toolBar.items objectAtIndex:1] setSelected:NO];
    [[_toolBar.items objectAtIndex:3] setSelected:NO];
    [[_toolBar.items objectAtIndex:5] setSelected:NO];
    
    [_activityIndicator stopAnimating];
    
    if ( _previousSelectedView == 3 )
        [self arView_Oculta];
    
    switch ( _selectedView )
    {
        case 1:
            [_originView configuraNavigationBarWithBtnModoVista:YES andBtnGeolocalizacion:YES];
            [[_toolBar.items objectAtIndex:1] setSelected:YES];
            _scrollView.contentOffset = CGPointMake(0, 0);
            break;
        case 2:
            [_originView configuraNavigationBarWithBtnModoVista:YES andBtnGeolocalizacion:YES];
            [[_toolBar.items objectAtIndex:3] setSelected:YES];
            [_mapView setRegion:[_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(_datosAplicacion.latitud, _datosAplicacion.longitud), 4000, 4000)]];
            [self mapView_CargaCoordenadas];
            _scrollView.contentOffset = CGPointMake(_scrollView.frame.size.width, 0);
            break;
        case 3:
            [_originView configuraNavigationBarWithBtnModoVista:YES andBtnGeolocalizacion:NO];
            [[_toolBar.items objectAtIndex:5] setSelected:YES];
            [self arView_Muestra];
            _scrollView.contentOffset = CGPointMake(_scrollView.frame.size.width * 2, 0);
            break;
    }
}

#pragma mark -
#pragma mark - btnRuta

- (IBAction)btnMasInformacion_Pressed:(id)sender
{
    int numRow = (int)[(UIButton*)sender tag];

    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Lugares_DetalleWebVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Lugares_DetalleWeb"];
    [pantallaDestino setUrlDestino:[[NSString stringWithFormat:@"%@", [[_listadoDatosMostrados objectAtIndex:numRow] objectForKey:@"registrant_url"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnWikipedia_Pressed:(id)sender
{
    int numRow = (int)[(UIButton*)sender tag];
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Lugares_DetalleWebVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Lugares_DetalleWeb"];
    [pantallaDestino setUrlDestino:[[NSString stringWithFormat:@"https://%@.wikipedia.org/wiki/%@", [[_listadoDatosMostrados objectAtIndex:numRow] objectForKey:@"lang"], [[_listadoDatosMostrados objectAtIndex:numRow] objectForKey:@"monument_article"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnComoLlegar_Pressed:(id)sender
{
    int numRow = (int)[(UIButton*)sender tag];
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Lugares_ComoLlegarVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Lugares_ComoLlegar"];
    [pantallaDestino setLatitud:[[[_listadoDatosMostrados objectAtIndex:numRow] objectForKey:@"lat"] doubleValue]];
    [pantallaDestino setLongitud:[[[_listadoDatosMostrados objectAtIndex:numRow] objectForKey:@"lon"] doubleValue]];
    [pantallaDestino setItemImagen:[_listadoDatos_Imagenes objectAtIndex:numRow]];
    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

- (IBAction)btnRuta_Pressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@""
                                message:NSLocalizedString(@"OPCION_PROXIMAMENTEDISPONIBLE", @"")
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

#pragma mark -
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ( (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height )
    {
        float scrollOfsset = ( scrollView.contentOffset.y + scrollView.frame.size.height ) - scrollView.contentSize.height;
        if ( scrollOfsset > 60.0 )
        {
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [activityIndicator startAnimating];
            activityIndicator.frame = CGRectMake(0, 0, 320, 44);
            _tableView.tableFooterView = activityIndicator;
            
            _scrollOfsset = scrollOfsset;
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ( !_itemsDownload )
    {
        if ( _scrollOfsset > 60.0 )
        {
            if ( [_listadoDatosMostrados count] > 0 )
                [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_listadoDatosMostrados count] - 1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            
            _tableView.tableFooterView = nil;
            [self downloadImages];
        }
    }
    _scrollOfsset = 0;
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Lugares_ListadoCell class]] )
    {
        if ( [_listadoDatos_Imagenes count] > indexPath.row )
        {
            Lugares_ListadoCell *cellToConfigure = (Lugares_ListadoCell *)cell;
            
            NSMutableDictionary *item = [_listadoDatosMostrados objectAtIndex:indexPath.row];
            
            cellToConfigure.lblNombre.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
            cellToConfigure.lblBtnMasInformacion.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];
            cellToConfigure.lblBtnWikipedia.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];
            cellToConfigure.lblBtnComoLlegar.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];
            cellToConfigure.lblBtnRuta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];

            cellToConfigure.viewLugar.layer.shadowOffset = CGSizeMake(3,3);
            cellToConfigure.viewLugar.layer.shadowColor = [COLOR_NEGRO CGColor];
            cellToConfigure.viewLugar.layer.shadowRadius = 10.0;
            cellToConfigure.viewLugar.layer.shadowOpacity = 0.5;
            cellToConfigure.viewLugar.layer.shadowPath = [UIBezierPath bezierPathWithRect:cellToConfigure.viewLugar.bounds].CGPath;

            cellToConfigure.lblNombre.text = [item objectForKey:@"name"];
            if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
                cellToConfigure.lblDistancia.text = ( (long)[[item objectForKey:@"dist"] doubleValue] < 1000L ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (long)[[item objectForKey:@"dist"] doubleValue]] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), [[item objectForKey:@"dist"] doubleValue] / 1000.0];
            else
                cellToConfigure.lblDistancia.text = ( [[item objectForKey:@"dist"] doubleValue] < POINT_ONE_MILE_METERS ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), [[item objectForKey:@"dist"] doubleValue] * METERS_TO_FEET] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), [[item objectForKey:@"dist"] doubleValue] * METERS_TO_MILES];
            
            cellToConfigure.btnMasInformacion.tag = indexPath.row;
            cellToConfigure.btnWikipedia.tag = indexPath.row;
            cellToConfigure.btnComoLlegar.tag = indexPath.row;

            cellToConfigure.btnMasInformacion.enabled = ( [[item objectForKey:@"registrant_url"] length] > 0 ) ? YES : NO;
            cellToConfigure.btnWikipedia.enabled = ( [[item objectForKey:@"monument_article"] length] > 0 ) ? YES : NO;

            [cellToConfigure.btnRuta setBackgroundImage:[UIImage imageNamed:@"Btn_ColorAzul"] forState:UIControlStateNormal];
            cellToConfigure.lblBtnRuta.text = NSLocalizedString(@"RUTA_ADD", @"");
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCellLugar inTableView:tableView forRowAtIndexPath:indexPath];
    
    _prototypeCellLugar.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellLugar.bounds));
    [_prototypeCellLugar layoutIfNeeded];
    
    CGSize size = [_prototypeCellLugar.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 1;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_listadoDatosMostrados count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Lugares_ListadoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellLugares_Listado"];
    [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];

    if ( [_listadoDatos_Imagenes count] > indexPath.row )
    {
        NSMutableDictionary *itemMonumento = [_listadoDatosMostrados objectAtIndex:indexPath.row];
        NSMutableDictionary *itemImagen = [_listadoDatos_Imagenes objectAtIndex:indexPath.row];

        if ( [itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
        {
            [cell.activityIndicator stopAnimating];
            cell.imgFoto.image = [UIImage imageWithData:[itemImagen objectForKey:@"fotoImgLista"]];
        }
        else
        {
            cell.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
            [cell.activityIndicator startAnimating];
            if ( [[itemMonumento objectForKey:@"image"] length] == 0 )
            {
                [cell.activityIndicator stopAnimating];
                cell.imgFoto.image = [UIImage imageNamed:@"Img_NoFotoMonumento"];
            }
        }
    }
    else
    {
        cell.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
        [cell.activityIndicator startAnimating];
    }

    return cell;
}

#pragma mark -
#pragma mark MapView

- (NSUInteger)zoomLevelForMapRect:(MKMapRect)mRect withMapViewSizeInPixels:(CGSize)viewSizeInPixels
{
    NSUInteger zoomLevel = MAPKIT_MAXZOOMLEVEL;

    MKZoomScale zoomScale = mRect.size.width / viewSizeInPixels.width;
    double zoomExponent = log2(zoomScale);
    zoomLevel = (NSUInteger)(MAPKIT_MAXZOOMLEVEL - ceil(zoomExponent));
    
    return zoomLevel;
}

- (void)mapView_CargaCoordenadas
{
    [_mapView setRegion:[_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(_datosAplicacion.latitud, _datosAplicacion.longitud), 1500, 1500)]];
    _mapLastZoomLevel = [self zoomLevelForMapRect:_mapView.visibleMapRect withMapViewSizeInPixels:_mapView.frame.size];
    [_mapView removeAnnotations:_mapView.annotations];
    [_mapView addAnnotations:_listadoDatos_CoordenadasMapa];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ( [annotation isKindOfClass:[OCAnnotation class]] )
    {
        OCAnnotation *clusterAnnotation = (OCAnnotation *)annotation;
        
        MKAnnotationView *pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterView"];
        if ( !pinView )
        {
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterView"];
            
            pinView.canShowCallout = NO;
            pinView.draggable = NO;
        }

        pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"]
                                            withFront:[UIImage ImageWithBurnedText:[NSString stringWithFormat:@"%lu", (unsigned long)[clusterAnnotation.annotationsInCluster count]]
                                                                      withFontSize:22
                                                                      andFontColor:COLOR_AZUL1
                                                                           atImage:[[UIImage roundedImage:[UIImage imageNamed:@"Img_FondoTransparente"] radius:180] imageByScalingProportionallyToSize:CGSizeMake(42, 42)]]
                                          atPositionX:12
                                         andPositionY:10];

        return pinView;
    }
    else if ( [annotation isKindOfClass:[MapViewAnnotation class]] )
    {
        MapViewAnnotation *mapAnnotation = annotation;
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];

        if ( !pinView )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
            
            pinView.animatesDrop = NO;
            pinView.canShowCallout = YES;
            pinView.draggable = NO;
            
            if ( [_listadoDatos_Imagenes count] <= mapAnnotation.tag )
                pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[[UIImage imageNamed:@"Img_NoFotoMonumento"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
            else
            {
                if ( [[_listadoDatos_Imagenes objectAtIndex:mapAnnotation.tag] objectForKey:@"fotoImgMapa"] != [NSNull null] )
                    pinView.image = [UIImage imageWithData:mapAnnotation.imagePinData];
                else
                    pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[[UIImage imageNamed:@"Img_NoFotoMonumento"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
            }

            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [rightButton setTitle:annotation.title forState:UIControlStateNormal];
            [rightButton setTag:mapAnnotation.tag];
            
            pinView.rightCalloutAccessoryView = rightButton;
            pinView.tag = mapAnnotation.tag;
            
        }
        else pinView.annotation = annotation;
        
        return pinView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Lugares_DetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Lugares_Detalle"];
    [pantallaDestino setNumItem:view.tag];
    [pantallaDestino setOriginView:self];
    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if ( _mapLastZoomLevel != [self zoomLevelForMapRect:_mapView.visibleMapRect withMapViewSizeInPixels:_mapView.frame.size] )
    {
        _mapLastZoomLevel = [self zoomLevelForMapRect:_mapView.visibleMapRect withMapViewSizeInPixels:_mapView.frame.size];
        if ( _mapView.region.span.longitudeDelta < 0.0075 )
            _mapView.clusteringEnabled = NO;
        else
        {
            _mapView.clusteringEnabled = YES;
            _mapView.clusterSize = _mapView.region.span.longitudeDelta;
        }
    }
    else
    {
        [_mapView removeAnnotations:_mapView.annotations];
        [_mapView addAnnotations:_listadoDatos_CoordenadasMapa];
    }
    [_mapView doClustering];
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views;
{
}

#pragma mark -
#pragma mark - ARViewController

- (void)arView_Oculta
{
    for ( UIView *viewToRemove in [_containerView3 subviews] )
    {
        [viewToRemove removeFromSuperview];
    }
}

- (void)arView_Muestra
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [self arView_Oculta];
    
    _arView = nil;
    _arView = [[ARViewController alloc] initWithDelegate:self andFrame:CGRectMake(0, 0, _containerView3.frame.size.width, _containerView3.frame.size.height)];
    _arView.showsCloseButton = false;
    [_arView setRadarRange:( [[userDefaults objectForKey:@"radioBusqueda"] doubleValue] / 1000.0 )];
    [_arView setScaleViewsBasedOnDistance:YES];
    [_arView setOnlyShowItemsWithinRadarRange:YES];
    [_arView setRadarBackgroundColour:[UIColor colorWithRed:71.0/255.0 green:152.0/255.0 blue:176.0/255.0 alpha:.4]];
    [_arView setRadarViewportColour:[UIColor colorWithRed:71.0/255.0 green:152.0/255.0 blue:176.0/255.0 alpha:.8]];
    [_arView setRadarPointColour:COLOR_BLANCO];
    
    [_containerView3 addSubview:_arView.view];
}

#pragma mark -
#pragma mark - ARLocationDelegate

- (NSMutableArray *)geoLocations
{
    NSMutableArray *locationArray = [[NSMutableArray alloc] init];
    ARGeoCoordinate *arCoordinate;

    int numItem = 0;

    while ( ( [locationArray count] < 100 ) && ( numItem < [_listadoDatos_CoordenadasAR count] ) )
    {
        NSMutableDictionary *item = [_listadoDatos_CoordenadasAR objectAtIndex:numItem];

        arCoordinate = [ARGeoCoordinate coordinateWithLocation:[[CLLocation alloc] initWithLatitude:[[item objectForKey:@"lat"] doubleValue]
                                                                                          longitude:[[item objectForKey:@"lon"] doubleValue]]
                                              locationDistance:[[item objectForKey:@"dist"] doubleValue]
                                                 locationTitle:@""
                                                   locationTag:[[item objectForKey:@"id"] intValue]
                                                     iconImage:[UIImage imageWithData:[item objectForKey:@"image"]]];
        [locationArray addObject:arCoordinate];

        numItem++;
    }
    return locationArray;
}

- (void)locationClicked:(ARGeoCoordinate *)coordinate
{
    int idItem = (int)coordinate.tag;
    
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    Lugares_DetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Lugares_Detalle"];
    [pantallaDestino setNumItem:idItem];
    [pantallaDestino setOriginView:self];
    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

@end