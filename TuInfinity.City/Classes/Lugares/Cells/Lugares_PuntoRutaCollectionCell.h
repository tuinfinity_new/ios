//
//  Lugares_PuntoRutaCollectionCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 09/09/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Lugares_PuntoRutaCollectionCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblInstrucciones;

@end
