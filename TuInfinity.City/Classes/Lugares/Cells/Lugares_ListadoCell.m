//
//  Lugares_ListadoCell.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 16/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "Lugares_ListadoCell.h"

@implementation Lugares_ListadoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
