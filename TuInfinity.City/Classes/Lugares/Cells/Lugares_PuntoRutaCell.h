//
//  Lugares_PuntoRutaCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 09/09/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Lugares_PuntoRutaCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblDistancia;
@property (nonatomic, weak) IBOutlet UILabel *lblInstrucciones;

@end
