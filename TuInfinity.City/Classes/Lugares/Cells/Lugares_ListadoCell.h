//
//  Lugares_ListadoCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 16/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Lugares_ListadoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *viewLugar;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UIImageView *imgFoto;

@property (nonatomic, weak) IBOutlet UILabel *lblNombre;
@property (nonatomic, weak) IBOutlet UILabel *lblDistancia;

@property (nonatomic, weak) IBOutlet UILabel *lblBtnMasInformacion;
@property (nonatomic, weak) IBOutlet UILabel *lblBtnWikipedia;
@property (nonatomic, weak) IBOutlet UILabel *lblBtnComoLlegar;
@property (nonatomic, weak) IBOutlet UILabel *lblBtnRuta;

@property (nonatomic, weak) IBOutlet UIButton *btnMasInformacion;
@property (nonatomic, weak) IBOutlet UIButton *btnWikipedia;
@property (nonatomic, weak) IBOutlet UIButton *btnComoLlegar;
@property (nonatomic, weak) IBOutlet UIButton *btnRuta;

@end
