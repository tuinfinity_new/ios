//
//  Lugares_PuntoRutaCell.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 09/09/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "Lugares_PuntoRutaCell.h"

@implementation Lugares_PuntoRutaCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
