//
//  Lugares_ListadoVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 16/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ARKit.h"
#import "OCMapView.h"

#import "LugaresVC.h"

@interface Lugares_ListadoVC : UIViewController

@property (nonatomic, weak) IBOutlet LugaresVC *originView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, weak) IBOutlet OCMapView *mapView;
@property (nonatomic, strong) IBOutlet ARViewController *arView;

@property (nonatomic, strong) NSMutableArray *listadoDatosTotal;
@property (nonatomic, strong) NSMutableArray *listadoDatosMostrados;
@property (nonatomic, strong) NSMutableArray *listadoDatos_Imagenes;
@property (nonatomic, strong) NSMutableArray *listadoDatos_CoordenadasMapa;
@property (nonatomic, strong) NSMutableArray *listadoDatos_CoordenadasAR;

@property (nonatomic, readwrite) int previousSelectedView;
@property (nonatomic, readwrite) int selectedView;

- (void)showSelectedView;

@end