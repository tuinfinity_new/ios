//
//  UIImage_Misc.h
//  iPuzle
//
//  Created by Miquel Masip on 30/04/14.
//  Copyright (c) 2014 InfoStreetPoint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Misc)

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (UIImage *)imageWithImage:(UIImage *)image scaledToWidth:(CGFloat)newWidth;

+ (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)targetSize;

+ (UIImage *)imageByCombiningMargin:(UIImage *)imageOrigin withMargin:(UIImage *)imageMargin;
+ (UIImage *)imageByCombiningBack:(UIImage *)imageBack withFront:(UIImage *)imageFront;
+ (UIImage *)imageByCombiningBack:(UIImage *)imageBack withFront:(UIImage *)imageFront atPositionX:(CGFloat)positionX andPositionY:(CGFloat)positionY;

+ (UIImage *)roundedImage:(UIImage *)image radius:(float)radius;

+ (UIImage *)ImageWithBurnedText:(NSString *)text withFontSize:(int)sizeFont andFontColor:(UIColor *)textColor atImage:(UIImage *)imageBack;

- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;
- (UIImage *)scaleAndRotateImage;

@end