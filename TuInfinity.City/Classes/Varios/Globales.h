//
//  Globales.h
//  iPuzle
//
//  Created by Miquel Masip on 30/04/14.
//  Copyright (c) 2014 InfoStreetPoint. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Globales : NSObject

#define COLOR_AZUL1    [UIColor colorWithRed:71.0/255.0  green:152.0/255.0 blue:176.0/255.0 alpha:1.0]
#define COLOR_AZUL2    [UIColor colorWithRed:44.0/255.0  green:75.0/255.0  blue:104.0/255.0 alpha:1.0]
#define COLOR_BLANCO   [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]
#define COLOR_GRIS1    [UIColor colorWithRed:80.0/255.0  green:80.0/255.0  blue:80.0/255.0  alpha:1.0]
#define COLOR_GRIS2    [UIColor colorWithRed:118.0/255.0 green:118.0/255.0 blue:118.0/255.0 alpha:1.0]
#define COLOR_GRIS3    [UIColor colorWithRed:131.0/255.0 green:131.0/255.0 blue:131.0/255.0 alpha:1.0]
#define COLOR_GRIS4    [UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0]
#define COLOR_GRIS5    [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0]
#define COLOR_LILA1    [UIColor colorWithRed:204.0/255.0 green:20.0/255.0  blue:75.0/255.0  alpha:1.0]
#define COLOR_LILA2    [UIColor colorWithRed:178.0/255.0 green:9.0/255.0   blue:60.0/255.0  alpha:1.0]
#define COLOR_MARRON   [UIColor colorWithRed:206.0/255.0 green:137.0/255.0 blue:25.0/255.0  alpha:1.0]
#define COLOR_NEGRO    [UIColor colorWithRed:0.0/255.0   green:0.0/255.0   blue:0.0/255.0   alpha:1.0]
#define COLOR_ORO      [UIColor colorWithRed:255.0/255.0 green:212.0/255.0 blue:0.0/255.0   alpha:1.0]
#define COLOR_ROJO1    [UIColor colorWithRed:204.0/255.0 green:20.0/255.0  blue:75.0/255.0  alpha:1.0]
#define COLOR_ROJO2    [UIColor colorWithRed:178.0/255.0 green:9.0/255.0   blue:60.0/255.0  alpha:1.0]

#define DEVICE_IPAD     ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
#define DEVICE_IPHONE   ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
#define DEVICE_IPHONE_5 ( DEVICE_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f )

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define MAPKIT_MAXZOOMLEVEL 20

#define POINT_ONE_MILE_METERS   161
#define METERS_TO_MILES         0.00062
#define METERS_TO_FEET          3.28084

#define URL_SERVIDOR_HTTPS @"https://api.tuinfinity.com"
#define URL_SERVIDOR_HTTP @"http://api.tuinfinity.com"
#define URL_SERVIDOR_OFERTA @"http://www.tuinfinity.com/oferta.php?id="
#define URL_SERVIDOR_EMPRESA @"http://www.tuinfinity.com/empresa.php?id="

#define PAYPAL_URL @"https://www.sandbox.paypal.com/cgi-bin/webscr"
#define PAYPAL_BUSSINESCODE @"CUYGTC69D8DZQ"
//#define PAYPAL_URL @"https://www.paypal.com/cgi-bin/webscr"
//#define PAYPAL_BUSSINESCODE @"F8XVPG4ZEK2E4"

#define PAYPAL_URLRETURN_OK @"http://www.tuinfinity.com/exito_pago.php"
#define PAYPAL_URLRETURN_ERR @"http://www.tuinfinity.com/paypal/error_pago.php"

#define PANTALLAORIGEN_CUPONESLISTADO  1001
#define PANTALLAORIGEN_CUPONESDETALLE  1002
#define PANTALLAORIGEN_OFERTASLISTADO  1101
#define PANTALLAORIGEN_OFERTASDETALLE  1102
#define PANTALLAORIGEN_EMPRESASLISTADO 1201
#define PANTALLAORIGEN_EMPRESASDETALLE 1202

+ (NSString *)showDateDifferenceBetween:(NSDate *)dateNow and:(NSDate *)dateTo;

+ (void)scrollCurrentView:(UIView *)view withScroll:(int)scroll;

@end