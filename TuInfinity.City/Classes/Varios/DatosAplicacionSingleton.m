//
//  DatosAplicacionSingleton.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 11/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "DatosAplicacionSingleton.h"

@implementation DatosAplicacionSingleton

+ (DatosAplicacionSingleton *)sharedSingleton
{
    static DatosAplicacionSingleton *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if ( self = [super init] )
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

        _datosUsuario = [[NSMutableDictionary alloc] init];

        _geolocalizacionActiva = YES;
        
        _locationManager = [[CLLocationManager alloc] init];

        if ( [userDefaults objectForKey:@"locLatitud"] == nil )
            _latitud = 0.0;
        else
            _latitud = [[userDefaults objectForKey:@"locLatitud"] doubleValue];

        if ( [userDefaults objectForKey:@"locLongitud"] == nil )
            _longitud = 0.0;
        else
            _longitud = [[userDefaults objectForKey:@"locLongitud"] doubleValue];

        _lastUpdateLocation = [[NSDate alloc] init];
        
        _poblacion = [[NSString alloc] init];
        
        [self calculatePlacemark:[[CLLocation alloc] initWithLatitude:_latitud longitude:_longitud]];
    }
    return self;
}

- (void)updateLocation
{
    _latitud = _location.coordinate.latitude;
    _longitud = _location.coordinate.longitude;
    _lastUpdateLocation = [[NSDate alloc] init];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    [userDefaults setValue:[NSNumber numberWithDouble:_latitud] forKeyPath:@"locLatitud"];
    [userDefaults setValue:[NSNumber numberWithDouble:_longitud] forKeyPath:@"locLongitud"];
    [userDefaults synchronize];
    
    [self calculatePlacemark:[[CLLocation alloc] initWithLatitude:_latitud longitude:_longitud]];
}

- (void)calculatePlacemark:(CLLocation *)location
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if ( error == nil && [placemarks count] > 0 )
            _poblacion = [[placemarks lastObject] locality];
    }];
}

@end