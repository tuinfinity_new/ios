//
//  MapViewAnnotation.h
//  iPuzle
//
//  Created by Miquel Masip on 13/04/14.
//  Copyright (c) 2014 InfoStreetPoint. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapViewAnnotation : NSObject <MKAnnotation>

@property (nonatomic, readwrite) int tag;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *imagePin;
@property (nonatomic, retain) NSData *imagePinData;

- (id)initWithCoordinate:(CLLocationCoordinate2D)annotationCoordinate;
- (id)initWithCoordinate:(CLLocationCoordinate2D)annotationCoordinate andTag:(int)annotationTag;
- (id)initWithCoordinate:(CLLocationCoordinate2D)annotationCoordinate andTag:(int)annotationTag andImagePin:(NSString *)annotationPin;
- (id)initWithTitle:(NSString *)annotationTitle andCoordinate:(CLLocationCoordinate2D)annotationCoordinate;

@end