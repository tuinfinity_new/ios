//
//  MapViewAnnotation.m
//  iPuzle
//
//  Created by Miquel Masip on 13/04/14.
//  Copyright (c) 2014 InfoStreetPoint. All rights reserved.
//

#import "MapViewAnnotation.h"

@implementation MapViewAnnotation

- (id)initWithCoordinate:(CLLocationCoordinate2D)annotationCoordinate
{
    if ( self = [super init] )
    {
        _tag = 0;
        _title = @"";
        _coordinate = annotationCoordinate;
        _imagePin = @"";
        _imagePinData = UIImagePNGRepresentation([UIImage imageNamed:@"Img_FondoTransparente"]);
    }
	return self;
}

- (id)initWithCoordinate:(CLLocationCoordinate2D)annotationCoordinate andTag:(int)annotationTag
{
    if ( self = [super init] )
    {
        _tag = annotationTag;
        _title = @"";
        _coordinate = annotationCoordinate;
        _imagePin = @"";
        _imagePinData = UIImagePNGRepresentation([UIImage imageNamed:@"Img_FondoTransparente"]);
    }
	return self;
}

- (id)initWithCoordinate:(CLLocationCoordinate2D)annotationCoordinate andTag:(int)annotationTag andImagePin:(NSString *)annotationPin
{
    if ( self = [super init] )
    {
        _tag = annotationTag;
        _title = @"";
        _coordinate = annotationCoordinate;
        _imagePin = annotationPin;
        _imagePinData = UIImagePNGRepresentation([UIImage imageNamed:@"Img_FondoTransparente"]);
    }
	return self;
}

- (id)initWithTitle:(NSString *)annotationTitle andCoordinate:(CLLocationCoordinate2D)annotationCoordinate
{
    if ( self = [super init] )
    {
        _tag = 0;
        _title = annotationTitle;
        _coordinate = annotationCoordinate;
        _imagePin = @"";
        _imagePinData = UIImagePNGRepresentation([UIImage imageNamed:@"Img_FondoTransparente"]);
    }
	return self;
}

@end