//
//  Globales.m
//  iPuzle
//
//  Created by Miquel Masip on 30/04/14.
//  Copyright (c) 2014 InfoStreetPoint. All rights reserved.
//

#import "Globales.h"

@implementation Globales

#pragma mark -
#pragma mark - NSDate

+ (NSString *)showDateDifferenceBetween:(NSDate *)dateNow and:(NSDate *)dateTo
{
    NSDateFormatter *formatoFecha = [[NSDateFormatter alloc] init];
    [formatoFecha setDateStyle:NSDateFormatterMediumStyle];
    
    NSDateComponents *dateDifferenceSg = [[NSCalendar currentCalendar] components:NSSecondCalendarUnit fromDate:dateNow toDate:dateTo options:0];
    NSDateComponents *dateDifferenceMn = [[NSCalendar currentCalendar] components:NSMinuteCalendarUnit fromDate:dateNow toDate:dateTo options:0];
    NSDateComponents *dateDifferenceHr = [[NSCalendar currentCalendar] components:NSHourCalendarUnit fromDate:dateNow toDate:dateTo options:0];
    NSDateComponents *dateDifferenceDi = [[NSCalendar currentCalendar] components:NSDayCalendarUnit fromDate:dateNow toDate:dateTo options:0];
    NSDateComponents *dateDifferenceMs = [[NSCalendar currentCalendar] components:NSMonthCalendarUnit fromDate:dateNow toDate:dateTo options:0];
    
    if ( [dateDifferenceMs month] > 0 )
        return [formatoFecha stringFromDate:dateTo];
    else if ( [dateDifferenceDi day] > 0 )
        return [NSString stringWithFormat:NSLocalizedString(@"DIFERENCIAFECHA_DIAS", @""), (int)[dateDifferenceDi day]];
    else if ( [dateDifferenceHr hour] > 0 )
        return [NSString stringWithFormat:NSLocalizedString(@"DIFERENCIAFECHA_HORAS", @""), (int)[dateDifferenceHr hour]];
    else if ( [dateDifferenceMn minute] > 0 )
        return [NSString stringWithFormat:NSLocalizedString(@"DIFERENCIAFECHA_MINUTOS", @""), (int)[dateDifferenceMn minute]];
    else
        return [NSString stringWithFormat:NSLocalizedString(@"DIFERENCIAFECHA_SEGUNDOS", @""), (int)[dateDifferenceSg second]];
    
    return @"";
}

#pragma mark -
#pragma mark - UIView

+ (void)scrollCurrentView:(UIView *)view withScroll:(int)scroll
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.0];
    CGRect rect = view.frame;
    rect.origin.y -= scroll;
    [view setFrame:rect];
    [UIView commitAnimations];
}

@end