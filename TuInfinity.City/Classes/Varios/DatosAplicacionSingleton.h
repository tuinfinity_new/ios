//
//  DatosAplicacionSingleton.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 11/07/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <Foundation/Foundation.h>

@interface DatosAplicacionSingleton : NSObject

@property (nonatomic, strong) NSMutableDictionary *datosUsuario;

@property (nonatomic, readwrite) BOOL geolocalizacionActiva;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) CLLocation *lastLocation;
@property (nonatomic, strong) NSDate *lastUpdateLocation;

@property (nonatomic, strong) NSString *poblacion;

@property (nonatomic, readwrite) double latitud;
@property (nonatomic, readwrite) double longitud;

+ (DatosAplicacionSingleton *)sharedSingleton;

- (void)updateLocation;
- (void)calculatePlacemark:(CLLocation *)location;

@end