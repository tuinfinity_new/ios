//
//  Ofertas_MisComprasCanjeoVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 08/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ofertas_MisComprasCanjeoVC : UIViewController

@property (nonatomic, strong) NSMutableDictionary *itemOferta;

@end
