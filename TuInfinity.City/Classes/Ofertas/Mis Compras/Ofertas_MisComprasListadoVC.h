//
//  Ofertas_MisComprasListadoVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 18/09/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OfertasVC.h"

@interface Ofertas_MisComprasListadoVC : UIViewController

@property (nonatomic, weak) IBOutlet OfertasVC *originView;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *listadoDatos;
@property (nonatomic, strong) NSMutableArray *listadoDatos_Imagenes;

@property (nonatomic, readwrite) BOOL reloadDataAtAppear;

@end
