//
//  Ofertas_MisComprasDetalleVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 08/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ofertas_MisComprasDetalleVC : UIViewController

@property (nonatomic, readwrite) int estadoCupon;

@property (nonatomic, strong) NSMutableDictionary *itemOferta;
@property (nonatomic, strong) NSMutableDictionary *itemImagen;
@property (nonatomic, strong) NSMutableDictionary *itemImagenEmpresa;

@end
