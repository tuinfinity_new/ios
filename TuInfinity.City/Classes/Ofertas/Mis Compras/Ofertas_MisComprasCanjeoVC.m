//
//  Ofertas_MisComprasCanjeoVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 08/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "ZXingObjC.h"

#import "Globales.h"

#import "Ofertas_MisComprasDetalleCell.h"

#import "Ofertas_MisComprasCanjeoVC.h"

@interface Ofertas_MisComprasCanjeoVC ()

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) Ofertas_MisComprasDetalleCell *prototypeCellOferta;

@property (nonatomic, readwrite) BOOL btnCodigoQRActivo;
@property (nonatomic, readwrite) BOOL btnCodigoEANActivo;

@end

@implementation Ofertas_MisComprasCanjeoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _btnCodigoQRActivo = NO;
    _btnCodigoEANActivo = NO;
    
    [self configuraNavigationBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Prototype cells

- (Ofertas_MisComprasDetalleCell *)prototypeCellOferta
{
    if ( !_prototypeCellOferta )
        _prototypeCellOferta = [_tableView dequeueReusableCellWithIdentifier:@"CellOfertas_MisComprasCanjeo"];
    return _prototypeCellOferta;
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnCodigoQR_Pressed:(id)sender
{
    _btnCodigoQRActivo = YES;
    _btnCodigoEANActivo = NO;
    
    [_tableView reloadData];
}

- (IBAction)btnCodigoEAN_Pressed:(id)sender
{
    _btnCodigoQRActivo = NO;
    _btnCodigoEANActivo = YES;
    
    [_tableView reloadData];
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Ofertas_MisComprasDetalleCell class]] )
    {
        Ofertas_MisComprasDetalleCell *cellToConfigure = (Ofertas_MisComprasDetalleCell *)cell;
        
        cellToConfigure.lblTitulo.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:20];
        cellToConfigure.lblTituloCodigo.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:24];
        cellToConfigure.lblCodigo.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:25];
        
        cellToConfigure.btnCodigoQR.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
        cellToConfigure.btnCodigoEAN.titleLabel.font = [UIFont fontWithName:@"Franklin Gothic Medium Cond" size:14];
        
        cellToConfigure.lblTitulo.text = [[_itemOferta objectForKey:@"oferta"] objectForKey:@"titulo"];
        
        NSDateFormatter *formatoFecha1 = [[NSDateFormatter alloc] init];
        NSDateFormatter *formatoFecha2 = [[NSDateFormatter alloc] init];
        NSDateFormatter *formatoFecha3 = [[NSDateFormatter alloc] init];
        [formatoFecha1 setDateFormat:@"dd MM yyyy HH:mm:ss"];
        [formatoFecha2 setDateStyle:NSDateFormatterMediumStyle];
        [formatoFecha3 setDateFormat:@"HH:mm"];
        
        cellToConfigure.lblFechaCaducidad.text = [NSString stringWithFormat:NSLocalizedString(@"MISCUPONES_FECHACADUCIDAD", @""), [formatoFecha2 stringFromDate:[formatoFecha1 dateFromString:[[_itemOferta objectForKey:@"oferta"] objectForKey:@"fecha_caducidad"]]], [formatoFecha3 stringFromDate:[formatoFecha1 dateFromString:[[_itemOferta objectForKey:@"oferta"] objectForKey:@"fecha_caducidad"]]]];
        
        cellToConfigure.lblCodigo.text = [_itemOferta objectForKey:@"codigo"];
        
        cellToConfigure.btnCodigoQR.selected = _btnCodigoQRActivo;
        cellToConfigure.btnCodigoEAN.selected = _btnCodigoEANActivo;
        
        if ( !_btnCodigoQRActivo )
            cellToConfigure.imgCodigoQR.image = [UIImage imageNamed:@"Img_FondoTransparente"];
        else
        {
            ZXMultiFormatWriter *zxWriter = [[ZXMultiFormatWriter alloc] init];
            ZXBitMatrix *zxResult = [zxWriter encode:[_itemOferta objectForKey:@"codigo"]
                                              format:kBarcodeFormatQRCode
                                               width:(int)cellToConfigure.imgCodigoQR.frame.size.width
                                              height:(int)cellToConfigure.imgCodigoQR.frame.size.height
                                               error:nil];
            
            if ( !zxResult )
                cellToConfigure.imgCodigoQR.image = [UIImage imageNamed:@"Img_FondoTransparente"];
            else
            {
                ZXImage *zxImage = [ZXImage imageWithMatrix:zxResult];
                cellToConfigure.imgCodigoQR.image = [UIImage imageWithCGImage:zxImage.cgimage];
            }
        }
        
        if ( !_btnCodigoEANActivo )
            cellToConfigure.imgCodigoEAN.image = [UIImage imageNamed:@"Img_FondoTransparente"];
        else
        {
            ZXMultiFormatWriter *zxWriter = [[ZXMultiFormatWriter alloc] init];
            ZXBitMatrix *zxResult = [zxWriter encode:[_itemOferta objectForKey:@"codigo"]
                                              format:kBarcodeFormatEan8
                                               width:(int)cellToConfigure.imgCodigoEAN.frame.size.width
                                              height:(int)cellToConfigure.imgCodigoEAN.frame.size.height
                                               error:nil];
            
            if ( !zxResult )
                cellToConfigure.imgCodigoEAN.image = [UIImage imageNamed:@"Img_FondoTransparente"];
            else
            {
                ZXImage *zxImage = [ZXImage imageWithMatrix:zxResult];
                cellToConfigure.imgCodigoEAN.image = [UIImage imageWithCGImage:zxImage.cgimage];
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        [self configureCell:self.prototypeCellOferta inTableView:tableView forRowAtIndexPath:indexPath];
        
        _prototypeCellOferta.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.bounds), CGRectGetHeight(_prototypeCellOferta.bounds));
        [_prototypeCellOferta layoutIfNeeded];
        
        CGSize size = [_prototypeCellOferta.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height + 1;
    }
    else if ( indexPath.section == 1 )
        return ( _btnCodigoQRActivo ) ? 170 : 0;
    else if ( indexPath.section == 2 )
        return ( _btnCodigoEANActivo ) ? 120 : 0;
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        Ofertas_MisComprasDetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellOfertas_MisComprasCanjeo"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    else if ( indexPath.section == 1 )
    {
        Ofertas_MisComprasDetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellOfertas_MisComprasCanjeoCodigoQR"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    else if ( indexPath.section == 2 )
    {
        Ofertas_MisComprasDetalleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellOfertas_MisComprasCanjeoCodigoEAN"];
        [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
        return cell;
    }
    
    return nil;
}

@end