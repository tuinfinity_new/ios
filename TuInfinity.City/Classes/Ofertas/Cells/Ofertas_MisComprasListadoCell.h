//
//  Ofertas_MisComprasListadoCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 08/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ofertas_MisComprasListadoCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIView *viewCupon;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;
@property (nonatomic, weak) IBOutlet UILabel *lblUltimaHora;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecio;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecioOld;
@property (nonatomic, weak) IBOutlet UILabel *lblFechaCaducidad1;
@property (nonatomic, weak) IBOutlet UILabel *lblFechaCaducidad2;

@property (nonatomic, weak) IBOutlet UIImageView *imgFoto;
@property (nonatomic, weak) IBOutlet UIImageView *imgUltimaHora;
@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaExclusiva;
@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaRegalo;
@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaRA1;
@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaRA2;
@property (nonatomic, weak) IBOutlet UIImageView *imgFechaCaducidad;

@end
