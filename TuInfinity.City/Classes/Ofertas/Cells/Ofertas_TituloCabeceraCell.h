//
//  Ofertas_TituloCabeceraCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 03/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Ofertas_TituloCabeceraCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;

@end
