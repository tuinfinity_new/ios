//
//  Ofertas_MisComprasDetalleCell.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 08/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

@interface Ofertas_MisComprasDetalleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, weak) IBOutlet UILabel *lblTituloDescripcion;
@property (nonatomic, weak) IBOutlet UILabel *lblTituloCondiciones;
@property (nonatomic, weak) IBOutlet UILabel *lblTituloLocalizacion;
@property (nonatomic, weak) IBOutlet UILabel *lblTituloCodigo;

@property (nonatomic, weak) IBOutlet UILabel *lblTitulo;
@property (nonatomic, weak) IBOutlet UILabel *lblDescripcionTipo;
@property (nonatomic, weak) IBOutlet UILabel *lblUltimaHora;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecio;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecioOld;
@property (nonatomic, weak) IBOutlet UILabel *lblDistancia;
@property (nonatomic, weak) IBOutlet UILabel *lblDisponibilidad1;
@property (nonatomic, weak) IBOutlet UILabel *lblDisponibilidad2;
@property (nonatomic, weak) IBOutlet UILabel *lblDescripcion;
@property (nonatomic, weak) IBOutlet UILabel *lblCondiciones;
@property (nonatomic, weak) IBOutlet UILabel *lblNombre;
@property (nonatomic, weak) IBOutlet UILabel *lblDireccion;
@property (nonatomic, weak) IBOutlet UILabel *lblFechaCaducidad;
@property (nonatomic, weak) IBOutlet UILabel *lblCodigo;

@property (nonatomic, weak) IBOutlet UIImageView *imgFoto;
@property (nonatomic, weak) IBOutlet UIImageView *imgDescripcionTipo;
@property (nonatomic, weak) IBOutlet UIImageView *imgUltimaHora;
@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaExclusiva;
@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaRegalo;
@property (nonatomic, weak) IBOutlet UIImageView *imgLogo;
@property (nonatomic, weak) IBOutlet UIImageView *imgWifi;
@property (nonatomic, weak) IBOutlet UIImageView *imgCodigoQR;
@property (nonatomic, weak) IBOutlet UIImageView *imgCodigoEAN;

@property (nonatomic, weak) IBOutlet MKMapView *mapView;

@property (nonatomic, weak) IBOutlet UIImageView *imgBtnRuta;

@property (nonatomic, weak) IBOutlet UILabel *lblBtnRuta;

@property (nonatomic, weak) IBOutlet UILabel *lblBtnComoLlegar;

@property (nonatomic, weak) IBOutlet UIButton *btnCodigoQR;
@property (nonatomic, weak) IBOutlet UIButton *btnCodigoEAN;

@end
