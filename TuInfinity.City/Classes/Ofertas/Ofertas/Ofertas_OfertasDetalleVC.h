//
//  Ofertas_OfertasDetalleVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 03/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Ofertas_OfertasListadoVC.h"
#import "Empresas_DetalleVC.h"

@interface Ofertas_OfertasDetalleVC : UIViewController

@property (nonatomic, strong) NSMutableDictionary *itemOferta;
@property (nonatomic, strong) NSMutableDictionary *itemImagen;
@property (nonatomic, strong) NSMutableDictionary *itemImagenEmpresa;

@property (nonatomic, readwrite) int typeOriginView;

@property (nonatomic, weak) IBOutlet Ofertas_OfertasListadoVC *originViewOfertas;
@property (nonatomic, weak) IBOutlet Empresas_DetalleVC *originViewEmpresas;

- (void)compraConfirmada;
- (void)compraPagoOk;
- (void)compraPagoError;

@end