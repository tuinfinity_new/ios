//
//  Ofertas_OfertasPagoPayPalVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "Globales.h"

#import "Ofertas_OfertasPagoPayPalVC.h"

@interface Ofertas_OfertasPagoPayPalVC ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation Ofertas_OfertasPagoPayPalVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    _webView.scrollView.bounces = NO;
    
    [self configuraNavigationBar];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    NSMutableString *httpBody = [[NSMutableString alloc] init];
    [httpBody appendFormat:@"cmd=%@", @"_xclick"];
    [httpBody appendFormat:@"&item_name=%@", [_itemOferta objectForKey:@"titulo"]];
    [httpBody appendFormat:@"&business=%@", PAYPAL_BUSSINESCODE];
    [httpBody appendFormat:@"&shopping_url=%@", @"http://www.tuinfinity.com"];
    [httpBody appendFormat:@"&return=%@", @"http://tuinfinity.com/paypal/check_payment.php"];
    [httpBody appendFormat:@"&notify_url=%@", @"http://tuinfinity.com/paypal/paypal-ipn.php"];
    [httpBody appendFormat:@"&currency_code=%@", @"EUR"];
    [httpBody appendFormat:@"&amount=%@", [_itemOferta objectForKey:@"precio"]];
    [httpBody appendFormat:@"&rm=%@", @"2"];
    [httpBody appendFormat:@"&no_shipping=%@", @"1"];
    [httpBody appendFormat:@"&custom=%@", [NSString stringWithFormat:@"{\"id_oferta\":%@,\"id_usuario\":%@}",[_itemOferta objectForKeyedSubscript:@"id_oferta"],[userDefaults objectForKey:@"usrIdUsuario"]]];
    [httpBody appendFormat:@"&image_url=%@", @"http://www.tuinfinity.com/img/paypal/logo_paypal.png"];
    [httpBody appendFormat:@"&cpp_logo_image=%@", @"http://www.tuinfinity.com/img/paypal/logo_paypal.png"];
    [httpBody appendFormat:@"&cpp_cart_border_color=%@", @"4798b0"];

    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:PAYPAL_URL]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setHTTPBody:[httpBody dataUsingEncoding:NSUTF8StringEncoding]];
    [_webView loadRequest:urlRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark -
#pragma mark UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [_activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [_activityIndicator stopAnimating];

    NSString *currentURL =  webView.request.URL.absoluteString;

    if ( [currentURL isEqualToString:PAYPAL_URLRETURN_ERR] )
        [_originView compraPagoError];
    else if ( [currentURL isEqualToString:PAYPAL_URLRETURN_OK] )
        [_originView compraPagoOk];
}

@end