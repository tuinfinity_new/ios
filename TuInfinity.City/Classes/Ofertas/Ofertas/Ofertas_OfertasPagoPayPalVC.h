//
//  Ofertas_OfertasPagoPayPalVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 07/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Ofertas_OfertasDetalleVC.h"

@interface Ofertas_OfertasPagoPayPalVC : UIViewController

@property (nonatomic, strong) Ofertas_OfertasDetalleVC *originView;

@property (nonatomic, strong) NSMutableDictionary *itemOferta;

@end
