//
//  Ofertas_OfertasDetallePagoVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 06/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "UIImageView+WebCache.h"

#import "Globales.h"

#import "Ofertas_OfertasDetallePagoVC.h"

@interface Ofertas_OfertasDetallePagoVC ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *viewOpcionesOferta;

@property (weak, nonatomic) IBOutlet UIImageView *imgFoto;

@property (weak, nonatomic) IBOutlet UILabel *lblTituloOferta;

@property (weak, nonatomic) IBOutlet UILabel *lblTituloPrecio;
@property (weak, nonatomic) IBOutlet UILabel *lblTituloPago;

@property (weak, nonatomic) IBOutlet UILabel *lblPrecio1;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecio2;
@property (weak, nonatomic) IBOutlet UILabel *lblPrecio3;

@property (weak, nonatomic) IBOutlet UIImageView *imgFormaPago1;
@property (weak, nonatomic) IBOutlet UIImageView *imgFormaPago2;

@property (weak, nonatomic) IBOutlet UIButton *btnFormaPago1;
@property (weak, nonatomic) IBOutlet UIButton *btnFormaPago2;

@property (nonatomic, weak) IBOutlet UIImageView *imgFinOferta;
@property (nonatomic, weak) IBOutlet UILabel *lblFinOferta;

@property (nonatomic, weak) IBOutlet UILabel *lblPrecio;
@property (nonatomic, weak) IBOutlet UILabel *lblPrecioOld;

@property (nonatomic, weak) IBOutlet UIImageView *imgOfertaRegalo;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation Ofertas_OfertasDetallePagoVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self configuraNavigationBar];

    _btnFormaPago1.selected = NO;
    _btnFormaPago2.selected = NO;
    
    _lblTituloOferta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:14];

    _lblTituloPrecio.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:14];
    _lblTituloPago.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:14];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *precioAct, *precioOld;
    NSString *descripcionTipo;

    if ( [[_itemOferta objectForKey:@"precio"] doubleValue] - floor([[_itemOferta objectForKey:@"precio"] doubleValue]) == 0.0 )
        precioAct = [NSString stringWithFormat:@"%d€", [[_itemOferta objectForKey:@"precio"] intValue]];
    else
        precioAct = [NSString stringWithFormat:@"%.2f€", [[_itemOferta objectForKey:@"precio"] doubleValue]];
    
    if ( [[_itemOferta objectForKey:@"precio_anterior"] doubleValue] - floor([[_itemOferta objectForKey:@"precio_anterior"] doubleValue]) == 0.0 )
        precioOld = [NSString stringWithFormat:@"%d€", [[_itemOferta objectForKey:@"precio_anterior"] intValue]];
    else
        precioOld = [NSString stringWithFormat:@"%.2f€", [[_itemOferta objectForKey:@"precio_anterior"] doubleValue]];

    switch ( [[_itemOferta objectForKey:@"tipo"] intValue] )
    {
        case 0:
            descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO0", ""), [_itemOferta objectForKey:@"param1"], [_itemOferta objectForKey:@"param2"]];
            _imgOfertaRegalo.hidden = YES;
            break;
        case 1:
            descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO1", ""), [_itemOferta objectForKey:@"param1"], [_itemOferta objectForKey:@"param2"]];
            _imgOfertaRegalo.hidden = YES;
            break;
        case 2:
            descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO2", ""), [_itemOferta objectForKey:@"param1"]];
            _imgOfertaRegalo.hidden = YES;
            break;
        case 3:
            descripcionTipo = @"";
            _imgOfertaRegalo.hidden = YES;
            break;
        case 4:
            descripcionTipo = @"";
            _imgOfertaRegalo.hidden = NO;
            break;
    }

    _lblTituloOferta.text = [_itemOferta objectForKey:@"titulo"];
    _lblPrecio1.text = precioOld;
    _lblPrecio2.text = descripcionTipo;
    _lblPrecio3.text = precioAct;
    
    [_imgFormaPago1 setImage:[UIImage imageNamed:( _btnFormaPago1.selected ) ? @"Img_Seleccion2ON" : @"Img_Seleccion2OFF"]];
    [_imgFormaPago2 setImage:[UIImage imageNamed:( _btnFormaPago2.selected ) ? @"Img_Seleccion2ON" : @"Img_Seleccion2OFF"]];
    
    if ( [_itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
        _imgFoto.image = [UIImage imageWithData:[_itemImagen objectForKey:@"fotoImgLista"]];
    else
    {
        _imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
        
        NSString *urlImagenComprimida = [[NSString stringWithFormat:@"https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=%d&refresh=31536000&url=%@", (int)self.view.frame.size.width, [[_itemOferta objectForKey:@"foto"] objectForKey:@"foto_tmedium"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [_activityIndicator startAnimating];
        [_imgFoto sd_setImageWithURL:[NSURL URLWithString:urlImagenComprimida]
                           completed:^(UIImage *downloadedImage, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                               [_activityIndicator stopAnimating];
                               [_itemImagen setValue:UIImagePNGRepresentation(downloadedImage) forKey:@"fotoImgLista"];
                           }];
    }
    
    if ( [[_itemOferta objectForKey:@"max_unidades"] intValue] > 0 )
    {
        _imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Cantidad"];
        _lblFinOferta.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_UNIDADESDISPONIBLES", @""), ([[_itemOferta objectForKey:@"max_unidades"] intValue] - [[_itemOferta objectForKey:@"unidades_vendidas"] intValue])];
    }
    else
    {
        NSDateFormatter *formatoFecha = [[NSDateFormatter alloc] init];
        [formatoFecha setDateFormat:@"dd MM yyyy HH:mm:ss"];
        
        _imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Caducidad"];
        _lblFinOferta.text = [Globales showDateDifferenceBetween:[NSDate date] and:[formatoFecha dateFromString:[_itemOferta objectForKey:@"fecha_caducidad"]]];
    }
    
    NSDictionary *attributedStringSettings = @{ NSStrikethroughStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle] };

    _lblPrecio.text = precioAct;
    _lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:precioOld attributes:attributedStringSettings];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Navigation Bar

- (void)configuraNavigationBar
{
    UIButton *btnL1 = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btnL1 setFrame:CGRectMake(0, 0, 22, 22)];
    [btnL1 setImage:[UIImage imageNamed:@"Btn_Volver"] forState:UIControlStateNormal];
    [btnL1 addTarget:self action:@selector(btnVolver_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItems = [[NSArray alloc]initWithObjects:[[UIBarButtonItem alloc]initWithCustomView:btnL1], nil];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.title = NSLocalizedString(@"APP_TITULO", @"");
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnVolver_Pressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)btnFormaPago1_Pressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@""
                                message:NSLocalizedString(@"OPCION_PROXIMAMENTEDISPONIBLE", @"")
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

- (IBAction)btnFormaPago2_Pressed:(id)sender
{
    _btnFormaPago1.selected = NO;
    _btnFormaPago2.selected = YES;
    
    [_imgFormaPago1 setImage:[UIImage imageNamed:( _btnFormaPago1.selected ) ? @"Img_Seleccion2ON" : @"Img_Seleccion2OFF"]];
    [_imgFormaPago2 setImage:[UIImage imageNamed:( _btnFormaPago2.selected ) ? @"Img_Seleccion2ON" : @"Img_Seleccion2OFF"]];
}

- (IBAction)btnConfirmarCompra_Pressed:(id)sender
{
    if ( _btnFormaPago1.selected )
    {
    }
    else if ( _btnFormaPago2.selected )
        [_originView compraConfirmada];
    else
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:NSLocalizedString(@"OFERTA_NOFORMAPAGO", @"")
                                   delegate:self
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
}

@end