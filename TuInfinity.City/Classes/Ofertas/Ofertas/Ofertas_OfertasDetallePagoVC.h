//
//  Ofertas_OfertasDetallePagoVC.h
//  TuInfinity.City
//
//  Created by Miquel Masip on 06/10/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Ofertas_OfertasDetalleVC.h"

@interface Ofertas_OfertasDetallePagoVC : UIViewController

@property (nonatomic, strong) Ofertas_OfertasDetalleVC *originView;

@property (nonatomic, strong) NSMutableDictionary *itemOferta;
@property (nonatomic, strong) NSMutableDictionary *itemImagen;

@end
