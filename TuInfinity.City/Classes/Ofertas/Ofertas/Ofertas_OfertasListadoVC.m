//
//  Ofertas_OfertasListadoVC.m
//  TuInfinity.City
//
//  Created by Miquel Masip on 18/09/14.
//  Copyright (c) 2014 TuInfinity. All rights reserved.
//

#import "AFNetworking.h"
#import "UIAlertView+Blocks.h"
#import "UIImageView+WebCache.h"

#import "DatosAplicacionSingleton.h"
#import "Globales.h"
#import "MapViewAnnotation.h"
#import "UIImage_Misc.h"

#import "Ofertas_OfertasListadoCell.h"

#import "Ofertas_OfertasDetalleVC.h"
#import "Empresas_DetalleVC.h"

#import "Ofertas_OfertasListadoVC.h"

@interface Ofertas_OfertasListadoVC () <UINavigationControllerDelegate, UIScrollViewDelegate, MKMapViewDelegate, ARLocationDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) DatosAplicacionSingleton *datosAplicacion;

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, weak) IBOutlet UIView *containerView1;
@property (nonatomic, weak) IBOutlet UIView *containerView2;
@property (nonatomic, weak) IBOutlet UIView *containerView3;

@property (nonatomic, strong) Ofertas_OfertasListadoCell *prototypeCellOferta;

@property (nonatomic, readwrite) float scrollOfsset;

@property (nonatomic, readwrite) NSUInteger mapLastZoomLevel;

@property (nonatomic, weak) IBOutlet UILabel *lblNoResultados;

@end

@implementation Ofertas_OfertasListadoVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _originView.navigationController.delegate = self;
    
    _datosAplicacion = [DatosAplicacionSingleton sharedSingleton];
    
    _listadoDatos = [NSMutableArray array];
    _listadoDatos_Imagenes = [NSMutableArray array];
    _listadoDatos_Empresas = [NSMutableArray array];
    _listadoDatos_Empresas_Imagenes = [NSMutableArray array];
    _listadoDatos_CoordenadasMapa = [NSMutableArray array];
    _listadoDatos_CoordenadasAR = [NSMutableArray array];
    
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width * 3, _scrollView.frame.size.height);
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"TABLEVIEW_REFRESCO", @"")
                                                                     attributes:[NSDictionary dictionaryWithObject:[UIFont fontWithName:@"Arial" size:20] forKey:NSFontAttributeName]];
    [refreshControl addTarget:self action:@selector(reloadDataTableview:) forControlEvents:UIControlEventValueChanged];
    
    [_tableView addSubview:refreshControl];
    
    _mapView.showsUserLocation = YES;
    _mapView.clusteringEnabled = YES;
    
    _previousSelectedView = 0;
    _selectedView = 1;
    
    _opcionesBusqueda_Parametros = @"";
    _reloadDataAtAppear = YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    _originView.navigationController.delegate = self;
    
    if ( _reloadDataAtAppear )
    {
        _originView.lastTextoBusqueda = @"";
        _originView.lastCategoria = 0;
        
        [_listadoDatos removeAllObjects];
        [_listadoDatos_Imagenes removeAllObjects];
        [_listadoDatos_Empresas removeAllObjects];
        [_listadoDatos_Empresas_Imagenes removeAllObjects];
        [_listadoDatos_CoordenadasMapa removeAllObjects];
        [_listadoDatos_CoordenadasAR removeAllObjects];
        
        [_tableView reloadData];
        
        _itemsInicio = 0;
        _itemsNum = 20;
        _itemsDownload = NO;
        
        [self showSelectedView];
        [self downloadData];
    }
    else
        _reloadDataAtAppear = YES;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    _scrollView.frame = self.view.frame;
    _tableView.frame = self.view.frame;
    _mapView.frame = self.view.frame;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    _opcionesBusqueda_Parametros = @"";
    
    [_activityIndicator stopAnimating];
    [self arView_Oculta];
    [[SDWebImageManager sharedManager] cancelAll];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Prototype cells

- (Ofertas_OfertasListadoCell *)prototypeCellOferta
{
    if ( !_prototypeCellOferta )
        _prototypeCellOferta = [_tableView dequeueReusableCellWithIdentifier:@"CellOfertas_OfertasListado"];
    return _prototypeCellOferta;
}

#pragma mark -
#pragma mark - Misc

- (void)showSelectedView
{
    [_activityIndicator stopAnimating];
    
    if ( _previousSelectedView == 3 )
        [self arView_Oculta];
    
    switch ( _selectedView )
    {
        case 1:
            [_originView configuraNavigationBarWithBtnModoVista:YES andBtnFiltro:YES andBtnGeolocalizacion:YES];
            [_scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
            break;
        case 2:
            [_originView configuraNavigationBarWithBtnModoVista:YES andBtnFiltro:YES andBtnGeolocalizacion:YES];
            [_mapView setRegion:[_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(_datosAplicacion.latitud, _datosAplicacion.longitud), 4000, 4000)]];
            [self mapView_CargaCoordenadas];
            [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width, 0) animated:NO];
            break;
        case 3:
            [_originView configuraNavigationBarWithBtnModoVista:YES andBtnFiltro:NO andBtnGeolocalizacion:NO];
            [self arView_Muestra];
            [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width * 2, 0) animated:NO];
            break;
    }
}

- (void)downloadData
{
    [_activityIndicator startAnimating];
    _lblNoResultados.hidden = YES;
    
    _itemsDownload = YES;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSURL *url = ( [[userDefaults objectForKey:@"usrInvitado"] boolValue] ) ? [NSURL URLWithString:URL_SERVIDOR_HTTP] : [NSURL URLWithString:URL_SERVIDOR_HTTPS];
    
    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager.securityPolicy setAllowInvalidCertificates:YES];
    [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
    [operationManager GET:[NSString stringWithFormat:@"ofertaPago/%f/%f/%d/%d%@", _datosAplicacion.latitud, _datosAplicacion.longitud, _itemsInicio, _itemsNum, [_opcionesBusqueda_Parametros stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
               parameters:nil
                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                      NSMutableArray *receivedData = [[NSJSONSerialization JSONObjectWithData:[[operation responseString] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] mutableCopy];
                      
                      if ( [receivedData count] > 0 )
                      {
                          for ( int numItem = 0; numItem < [receivedData count]; numItem++ )
                          {
                              NSMutableDictionary *itemOferta = [[receivedData objectAtIndex:numItem] mutableCopy];
                              [_listadoDatos addObject:itemOferta];
                              
                              NSMutableDictionary *itemImagen = [[NSMutableDictionary alloc] init];
                              [itemImagen setValue:[NSNull null] forKey:@"fotoImgLista"];
                              [itemImagen setValue:[NSNumber numberWithBool:NO] forKey:@"loading"];
                              [_listadoDatos_Imagenes addObject:itemImagen];
                          }

                          [_activityIndicator stopAnimating];
                          
                          _itemsInicio += _itemsNum;
                          [_tableView reloadData];
                          
                          for ( int numItem = 0; numItem < [receivedData count]; numItem++ )
                          {
                              NSMutableDictionary *itemOferta = [[receivedData objectAtIndex:numItem] mutableCopy];
                              
                              if ( [[_listadoDatos_Empresas filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id_empresa==%d", [[[itemOferta objectForKey:@"empresa"] objectForKey:@"id_empresa"] intValue]]] count] == 0 )
                              {
                                  NSMutableDictionary *itemEmpresa = [[itemOferta objectForKey:@"empresa"] mutableCopy];
                                  [itemEmpresa setValue:[NSNumber numberWithInt:1] forKey:@"numCupones"];
                                  [_listadoDatos_Empresas addObject:itemEmpresa];
                                  
                                  NSMutableDictionary *itemImagenEmpresa = [[NSMutableDictionary alloc] init];
                                  
                                  [itemImagenEmpresa setValue:[NSNull null] forKey:@"fotoImgLogo"];
                                  [itemImagenEmpresa setValue:[NSNull null] forKey:@"fotoImgMapa"];
                                  [itemImagenEmpresa setValue:[NSNull null] forKey:@"fotoImgRA"];
                                  [itemImagenEmpresa setValue:[NSNumber numberWithBool:NO] forKey:@"loading"];
                                  [_listadoDatos_Empresas_Imagenes addObject:itemImagenEmpresa];
                                  
                                  MapViewAnnotation *annotation = [[MapViewAnnotation alloc] initWithTitle:[itemEmpresa objectForKey:@"nombre"]
                                                                                             andCoordinate:CLLocationCoordinate2DMake([[itemEmpresa objectForKey:@"latitud"] doubleValue], [[itemEmpresa objectForKey:@"longitud"] doubleValue])];
                                  annotation.tag = (int)[_listadoDatos_Empresas indexOfObject:itemEmpresa];
                                  annotation.imagePinData = UIImagePNGRepresentation([UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[[UIImage imageNamed:@"Img_NoFotoEmpresa"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] atPositionX:12 andPositionY:6]);
                                  [_listadoDatos_CoordenadasMapa addObject:annotation];
                                  
                                  NSDictionary *point = @{
                                                          @"id" : @([_listadoDatos_Empresas indexOfObject:itemEmpresa]),
                                                          @"title" : [itemEmpresa objectForKey:@"nombre"],
                                                          @"lat" : @([[itemEmpresa objectForKey:@"latitud"] doubleValue]),
                                                          @"lon" : @([[itemEmpresa objectForKey:@"longitud"] doubleValue]),
                                                          @"dist" : @([_datosAplicacion.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[[itemEmpresa objectForKey:@"latitud"] doubleValue] longitude:[[itemEmpresa objectForKey:@"longitud"] doubleValue]]]),
                                                          @"image" : UIImagePNGRepresentation([UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroRA"] withFront:[UIImage roundedImage:[UIImage imageWithImage:[UIImage imageNamed:@"Img_NoFotoEmpresa"] scaledToSize:CGSizeMake(84, 83)] radius:180] atPositionX:24 andPositionY:14])
                                                          };
                                  [_listadoDatos_CoordenadasAR addObject:point];
                                  
                                  if ( [itemEmpresa objectForKey:@"id_logo"] != nil )
                                  {
                                      dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0ul);
                                      dispatch_async(queue, ^{
                                          UIImage *imgFoto = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[itemEmpresa objectForKey:@"id_logo"] objectForKey:@"foto_medium"]]]];
                                          
                                          dispatch_sync(dispatch_get_main_queue(), ^{
                                              UIImage *imgNumCupones1 = [UIImage ImageWithBurnedText:[NSString stringWithFormat:@"%d",[[itemEmpresa objectForKey:@"numCupones"] intValue]]
                                                                                        withFontSize:20
                                                                                        andFontColor:COLOR_BLANCO
                                                                                             atImage:[UIImage imageWithImage:[UIImage imageNamed:@"Img_FondoTransparente"] scaledToSize:CGSizeMake(35, 35)]];
                                              UIImage *imgNumCupones2 = [UIImage roundedImage:[UIImage imageWithImage:[UIImage imageNamed:@"Img_FondoAzul1"] scaledToSize:CGSizeMake(35, 35)] radius:180];
                                              
                                              UIImage *imgNumCupones = [UIImage imageByCombiningBack:imgNumCupones2
                                                                                           withFront:imgNumCupones1
                                                                                         atPositionX:1
                                                                                        andPositionY:3];
                                              
                                              UIImage *imgFotoLogo = imgFoto;
                                              UIImage *imgFotoMapa = [UIImage imageByCombiningBack:[UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[UIImage imageWithImage:imgFoto scaledToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6]
                                                                                         withFront:[UIImage imageWithImage:imgNumCupones scaledToSize:CGSizeMake(20, 20)]
                                                                                       atPositionX:3
                                                                                      andPositionY:1];
                                              UIImage *imgFotoRA = [UIImage imageByCombiningBack:[UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroRA"] withFront:[UIImage roundedImage:[UIImage imageWithImage:imgFoto scaledToSize:CGSizeMake(84, 83)] radius:180] atPositionX:24 andPositionY:14]
                                                                                       withFront:[UIImage imageWithImage:imgNumCupones scaledToSize:CGSizeMake(35, 35)]
                                                                                     atPositionX:10
                                                                                    andPositionY:2];
                                              
                                              int numItemImagen = (int)[_listadoDatos_Empresas indexOfObject:itemEmpresa];
                                              if ( numItemImagen < [_listadoDatos_Empresas count] )
                                              {
                                                  [[_listadoDatos_Empresas_Imagenes objectAtIndex:numItemImagen] setObject:UIImagePNGRepresentation(imgFotoLogo) forKey:@"fotoImgLogo"];
                                                  [[_listadoDatos_Empresas_Imagenes objectAtIndex:numItemImagen] setObject:UIImagePNGRepresentation(imgFotoMapa) forKey:@"fotoImgMapa"];
                                                  [[_listadoDatos_Empresas_Imagenes objectAtIndex:numItemImagen] setObject:UIImagePNGRepresentation(imgFotoRA) forKey:@"fotoImgRA"];
                                                  
                                                  MapViewAnnotation *annotation = [_listadoDatos_CoordenadasMapa objectAtIndex:numItemImagen];
                                                  annotation.imagePinData = UIImagePNGRepresentation(imgFotoMapa);
                                                  [_listadoDatos_CoordenadasMapa replaceObjectAtIndex:numItemImagen withObject:annotation];
                                                  
                                                  if ( _selectedView == 2 )
                                                  {
                                                      [_mapView removeAnnotations:_mapView.annotations];
                                                      [_mapView addAnnotations:_listadoDatos_CoordenadasMapa];
                                                  }
                                                  
                                                  NSMutableDictionary *point = [[_listadoDatos_CoordenadasAR objectAtIndex:numItemImagen] mutableCopy];
                                                  [point setValue:UIImagePNGRepresentation(imgFotoRA) forKeyPath:@"image"];
                                                  [_listadoDatos_CoordenadasAR replaceObjectAtIndex:numItemImagen withObject:point];
                                              }
                                          });
                                      });
                                  }
                              }
                              else
                              {
                                  int numItem = [_listadoDatos_Empresas indexOfObject:[[_listadoDatos_Empresas filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id_empresa==%d", [[[itemOferta objectForKey:@"empresa"] objectForKey:@"id_empresa"] intValue]]] objectAtIndex:0]];
                                  
                                  if ( numItem < [_listadoDatos_Empresas count] )
                                      [[_listadoDatos_Empresas objectAtIndex:numItem] setValue:[NSNumber numberWithInt:[[[_listadoDatos_Empresas objectAtIndex:numItem] objectForKey:@"numCupones"] intValue] + 1] forKey:@"numCupones"];
                              }
                          }
                      }
                      _itemsDownload = NO;
                      _lblNoResultados.hidden = ( [_listadoDatos count] > 0 ) ? YES : NO;
                      [_activityIndicator stopAnimating];
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error){
                      NSLog(@"%@",[error description]);
                      _itemsDownload = NO;
                      _lblNoResultados.hidden = ( [_listadoDatos count] > 0 ) ? YES : NO;
                      [_activityIndicator stopAnimating];
                  }];
}

- (IBAction)reloadDataTableview:(id)sender
{
    [sender endRefreshing];
    
    if ( !_itemsDownload )
    {
        [_listadoDatos removeAllObjects];
        [_listadoDatos_Imagenes removeAllObjects];
        [_listadoDatos_Empresas removeAllObjects];
        [_listadoDatos_Empresas_Imagenes removeAllObjects];
        [_listadoDatos_CoordenadasMapa removeAllObjects];
        [_listadoDatos_CoordenadasAR removeAllObjects];
        
        [_tableView reloadData];
        
        _itemsInicio = 0;
        _itemsNum = 20;
        
        [self downloadData];
    }
}

#pragma mark -
#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ( viewController == _originView )
        [self viewWillAppear:animated];
}

#pragma mark -
#pragma mark - UIButton

- (IBAction)btnRuta_Pressed:(id)sender
{
    [[[UIAlertView alloc] initWithTitle:@""
                                message:NSLocalizedString(@"OPCION_PROXIMAMENTEDISPONIBLE", @"")
                               delegate:self
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

#pragma mark -
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ( (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height )
    {
        float scrollOfsset = ( scrollView.contentOffset.y + scrollView.frame.size.height ) - scrollView.contentSize.height;
        if ( scrollOfsset > 60.0 )
        {
            UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [activityIndicator startAnimating];
            activityIndicator.frame = CGRectMake(0, 0, 320, 44);
            _tableView.tableFooterView = activityIndicator;
            
            _scrollOfsset = scrollOfsset;
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if ( !_itemsDownload )
    {
        if ( _scrollOfsset > 60.0 )
        {
            if ( [_listadoDatos count] > 0 )
                [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_listadoDatos count] - 1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            
            _tableView.tableFooterView = nil;
            
            [self downloadData];
        }
    }
    _scrollOfsset = 0;
}

#pragma mark -
#pragma mark - TableView

- (void)configureCell:(UITableViewCell *)cell inTableView:(UITableView *)tableView forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [cell isKindOfClass:[Ofertas_OfertasListadoCell class]] )
    {
        Ofertas_OfertasListadoCell *cellToConfigure = (Ofertas_OfertasListadoCell *)cell;
        
        NSMutableDictionary *itemOferta = [_listadoDatos objectAtIndex:indexPath.row];
        
        cellToConfigure.lblBtnRuta.font = [UIFont fontWithName:@"Franklin Gothic Demi Cond" size:10];
        
        cellToConfigure.viewOferta.layer.shadowOffset = CGSizeMake(3,3);
        cellToConfigure.viewOferta.layer.shadowColor = [COLOR_NEGRO CGColor];
        cellToConfigure.viewOferta.layer.shadowRadius = 10.0;
        cellToConfigure.viewOferta.layer.shadowOpacity = 0.5;
        cellToConfigure.viewOferta.layer.shadowPath = [UIBezierPath bezierPathWithRect:cellToConfigure.viewOferta.bounds].CGPath;
        
        cellToConfigure.imgOfertaExclusiva.hidden = YES;
        cellToConfigure.imgUltimaHora.hidden = YES;
        cellToConfigure.lblUltimaHora.hidden = YES;
        
        cellToConfigure.lblTitulo.text = [itemOferta objectForKey:@"titulo"];
        
        if ( [[itemOferta objectForKey:@"precio"] doubleValue] - floor([[itemOferta objectForKey:@"precio"] doubleValue]) == 0.0 )
            cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%d€", [[itemOferta objectForKey:@"precio"] intValue]];
        else
            cellToConfigure.lblPrecio.text = [NSString stringWithFormat:@"%.2f€", [[itemOferta objectForKey:@"precio"] doubleValue]];
        
        NSDictionary *attributedStringSettings = @{ NSStrikethroughStyleAttributeName:[NSNumber numberWithInt:NSUnderlineStyleSingle] };
        
        if ( [[itemOferta objectForKey:@"precio_anterior"] doubleValue] - floor([[itemOferta objectForKey:@"precio_anterior"] doubleValue]) == 0.0 )
            cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %d€ ", [[itemOferta objectForKey:@"precio_anterior"] intValue]] attributes:attributedStringSettings];
        else
            cellToConfigure.lblPrecioOld.attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%.2f€", [[itemOferta objectForKey:@"precio_anterior"] doubleValue]] attributes:attributedStringSettings];
        
        cellToConfigure.imgUltimaHora.hidden = ![[itemOferta objectForKey:@"isFlash"] boolValue];
        cellToConfigure.lblUltimaHora.hidden = ![[itemOferta objectForKey:@"isFlash"] boolValue];
        
        NSString *descripcionTipo;
        switch ( [[itemOferta objectForKey:@"tipo"] intValue] )
        {
            case 0:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO0", ""), [itemOferta objectForKey:@"param1"], [itemOferta objectForKey:@"param2"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 1:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO1", ""), [itemOferta objectForKey:@"param1"], [itemOferta objectForKey:@"param2"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 2:
                descripcionTipo = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_TIPO_DESCUENTO2", ""), [itemOferta objectForKey:@"param1"]];
                cellToConfigure.imgDescripcionTipo.hidden = NO;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 3:
                descripcionTipo = @"";
                cellToConfigure.imgDescripcionTipo.hidden = YES;
                cellToConfigure.lblPrecio.hidden = NO;
                cellToConfigure.lblPrecioOld.hidden = NO;
                cellToConfigure.imgOfertaRegalo.hidden = YES;
                break;
            case 4:
                descripcionTipo = @"";
                cellToConfigure.imgDescripcionTipo.hidden = YES;
                cellToConfigure.lblPrecio.hidden = YES;
                cellToConfigure.lblPrecioOld.hidden = YES;
                cellToConfigure.imgOfertaRegalo.hidden = NO;
                break;
        }
        cellToConfigure.lblDescripcionTipo.text = descripcionTipo;
        
        CLLocationDistance distance = [_datosAplicacion.location distanceFromLocation:[[CLLocation alloc] initWithLatitude:[[[itemOferta objectForKey:@"empresa"] objectForKey:@"latitud"] doubleValue] longitude:[[[itemOferta objectForKey:@"empresa"] objectForKey:@"longitud"] doubleValue]]];
        
        if ( [[[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem] boolValue] )
            cellToConfigure.lblDistancia.text = ( (long)distance < 1000L ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_METROS", @""), (long)distance] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_KILOMETROS", @""), distance / 1000.0];
        else
            cellToConfigure.lblDistancia.text = ( distance < POINT_ONE_MILE_METERS ) ? [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_PIES", @""), distance * METERS_TO_FEET] : [NSString stringWithFormat:NSLocalizedString(@"DISTANCIA_MILLAS", @""), distance * METERS_TO_MILES];
        
        if ( [[itemOferta objectForKey:@"max_unidades"] intValue] > 0 )
        {
            cellToConfigure.imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Cantidad"];
            cellToConfigure.lblFinOferta.text = [NSString stringWithFormat:NSLocalizedString(@"CUPONES_UNIDADESDISPONIBLES", @""), ([[itemOferta objectForKey:@"max_unidades"] intValue] - [[itemOferta objectForKey:@"unidades_vendidas"] intValue])];
        }
        else
        {
            NSDateFormatter *formatoFecha = [[NSDateFormatter alloc] init];
            [formatoFecha setDateFormat:@"dd MM yyyy HH:mm:ss"];
            
            cellToConfigure.imgFinOferta.image = [UIImage imageNamed:@"Img_Oferta_Caducidad"];
            cellToConfigure.lblFinOferta.text = [Globales showDateDifferenceBetween:[NSDate date] and:[formatoFecha dateFromString:[itemOferta objectForKey:@"fecha_caducidad"]]];
        }
        
        [cellToConfigure.btnRuta setBackgroundImage:[UIImage imageNamed:@"Btn_ColorAzul"] forState:UIControlStateNormal];
        cellToConfigure.imgBtnRuta.image = [UIImage imageNamed:@"Btn_Ruta_Add"];
        cellToConfigure.lblBtnRuta.text = NSLocalizedString(@"RUTA_ADD", @"");
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 255;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_listadoDatos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Ofertas_OfertasListadoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellOfertas_OfertasListado"];
    [self configureCell:cell inTableView:tableView forRowAtIndexPath:indexPath];
    
    NSMutableDictionary *itemOferta = [_listadoDatos objectAtIndex:indexPath.row];
    NSMutableDictionary *itemImagen = [_listadoDatos_Imagenes objectAtIndex:indexPath.row];
    
    if ( [itemImagen objectForKey:@"fotoImgLista"] != [NSNull null] )
        cell.imgFoto.image = [UIImage imageWithData:[itemImagen objectForKey:@"fotoImgLista"]];
    else
    {
        if ( [_listadoDatos_Imagenes count] > indexPath.row )
        {
            cell.imgFoto.image = [UIImage imageNamed:@"Img_FondoBlanco"];
            
            NSString *urlImagenComprimida = [[NSString stringWithFormat:@"https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=%d&refresh=31536000&url=%@", (int)self.view.frame.size.width, [[itemOferta objectForKey:@"foto"] objectForKey:@"foto_medium"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            [cell.activityIndicator startAnimating];
            [cell.imgFoto sd_setImageWithURL:[NSURL URLWithString:urlImagenComprimida]
                                   completed:^(UIImage *downloadedImage, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                       [cell.activityIndicator stopAnimating];
                                       if ( [_listadoDatos_Imagenes count] > indexPath.row )
                                           [[_listadoDatos_Imagenes objectAtIndex:indexPath.row] setValue:UIImagePNGRepresentation(downloadedImage) forKey:@"fotoImgLista"];
                                   }];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
    
    _reloadDataAtAppear = NO;
    
    NSMutableDictionary *itemEmpresa = [[_listadoDatos_Empresas filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"id_empresa==%d", [[[[_listadoDatos objectAtIndex:indexPath.row] objectForKey:@"empresa"] objectForKey:@"id_empresa"] intValue]]] objectAtIndex:0];

    Ofertas_OfertasDetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Ofertas_OfertasDetalle"];

    [pantallaDestino setTypeOriginView:PANTALLAORIGEN_OFERTASLISTADO];
    [pantallaDestino setOriginViewOfertas:self];
    [pantallaDestino setItemOferta:[_listadoDatos objectAtIndex:indexPath.row]];
    [pantallaDestino setItemImagen:[_listadoDatos_Imagenes objectAtIndex:indexPath.row]];
    [pantallaDestino setItemImagenEmpresa:[_listadoDatos_Empresas_Imagenes objectAtIndex:[_listadoDatos_Empresas indexOfObject:itemEmpresa]]];
    [_originView.navigationController pushViewController:pantallaDestino animated:NO];
}

#pragma mark -
#pragma mark MapView

- (NSUInteger)zoomLevelForMapRect:(MKMapRect)mRect withMapViewSizeInPixels:(CGSize)viewSizeInPixels
{
    NSUInteger zoomLevel = MAPKIT_MAXZOOMLEVEL;
    
    MKZoomScale zoomScale = mRect.size.width / viewSizeInPixels.width;
    double zoomExponent = log2(zoomScale);
    zoomLevel = (NSUInteger)(MAPKIT_MAXZOOMLEVEL - ceil(zoomExponent));
    
    return zoomLevel;
}

- (void)mapView_CargaCoordenadas
{
    [_mapView setRegion:[_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(_datosAplicacion.latitud, _datosAplicacion.longitud), 1500, 1500)]];
    _mapLastZoomLevel = [self zoomLevelForMapRect:_mapView.visibleMapRect withMapViewSizeInPixels:_mapView.frame.size];
    [_mapView removeAnnotations:_mapView.annotations];
    [_mapView addAnnotations:_listadoDatos_CoordenadasMapa];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ( [annotation isKindOfClass:[OCAnnotation class]] )
    {
        OCAnnotation *clusterAnnotation = (OCAnnotation *)annotation;
        
        MKAnnotationView *pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterView"];
        if ( !pinView )
        {
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterView"];
            
            pinView.canShowCallout = NO;
            pinView.draggable = NO;
        }
        
        pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"]
                                            withFront:[UIImage ImageWithBurnedText:[NSString stringWithFormat:@"%lu", (unsigned long)[clusterAnnotation.annotationsInCluster count]]
                                                                      withFontSize:22
                                                                      andFontColor:COLOR_AZUL1
                                                                           atImage:[[UIImage roundedImage:[UIImage imageNamed:@"Img_FondoTransparente"] radius:180] imageByScalingProportionallyToSize:CGSizeMake(42, 42)]]
                                          atPositionX:12
                                         andPositionY:10];
        
        return pinView;
    }
    else if ( [annotation isKindOfClass:[MapViewAnnotation class]] )
    {
        MapViewAnnotation *mapAnnotation = annotation;
        
        MKPinAnnotationView *pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
        
        if ( !pinView )
        {
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:[NSString stringWithFormat:@"pinView%d", mapAnnotation.tag]];
            
            pinView.animatesDrop = NO;
            pinView.canShowCallout = YES;
            pinView.draggable = NO;
            
            if ( [[_listadoDatos_Empresas_Imagenes objectAtIndex:mapAnnotation.tag] objectForKey:@"fotoImgMapa"] != [NSNull null] )
                pinView.image = [UIImage imageWithData:mapAnnotation.imagePinData];
            else
                pinView.image = [UIImage imageByCombiningBack:[UIImage imageNamed:@"Img_PunteroMapa"] withFront:[UIImage roundedImage:[[UIImage imageNamed:@"Img_NoFotoEmpresa"] imageByScalingProportionallyToSize:CGSizeMake(42, 42)] radius:180] atPositionX:12 andPositionY:6];
            
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [rightButton setTitle:annotation.title forState:UIControlStateNormal];
            [rightButton setTag:mapAnnotation.tag];
            
            pinView.rightCalloutAccessoryView = rightButton;
            pinView.tag = mapAnnotation.tag;
            
        }
        else pinView.annotation = annotation;
        
        return pinView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
    
    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager.securityPolicy setAllowInvalidCertificates:YES];
    [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
    
    [operationManager GET:[NSString stringWithFormat:@"empresa/%d", [[[_listadoDatos_Empresas objectAtIndex:[view tag]] objectForKey:@"id_empresa"] intValue]]
               parameters:nil
                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                      NSMutableDictionary *receivedData = [[NSJSONSerialization JSONObjectWithData:[[operation responseString] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] mutableCopy];
                      
                      UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
                      
                      Empresas_DetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Empresas_Detalle"];
                      [pantallaDestino setItemEmpresa:receivedData];
                      [pantallaDestino setItemImagen:[_listadoDatos_Empresas_Imagenes objectAtIndex:[view tag]]];
                      [_originView.navigationController pushViewController:pantallaDestino animated:NO];
                      
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error){
                      NSLog(@"%@",[error description]);
                      [_activityIndicator stopAnimating];
                  }];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    NSUInteger mapZoomLevel = [self zoomLevelForMapRect:_mapView.visibleMapRect withMapViewSizeInPixels:_mapView.frame.size];
    
    if ( _mapLastZoomLevel != mapZoomLevel )
    {
        _mapLastZoomLevel = mapZoomLevel;
        if ( _mapView.region.span.longitudeDelta < 0.0075 )
            _mapView.clusteringEnabled = NO;
        else
        {
            _mapView.clusteringEnabled = YES;
            _mapView.clusterSize = _mapView.region.span.longitudeDelta * 0.25;
        }
    }
    else
    {
        [_mapView removeAnnotations:_mapView.annotations];
        [_mapView addAnnotations:_listadoDatos_CoordenadasMapa];
    }
    [_mapView doClustering];
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views;
{
}

#pragma mark -
#pragma mark - ARViewController

- (void)arView_Oculta
{
    for ( UIView *viewToRemove in [_containerView3 subviews] )
    {
        [viewToRemove removeFromSuperview];
    }
}

- (void)arView_Muestra
{
    [self arView_Oculta];
    
    _arView = nil;
    _arView = [[ARViewController alloc] initWithDelegate:self andFrame:CGRectMake(0, 0, _containerView3.frame.size.width, _containerView3.frame.size.height)];
    _arView.showsCloseButton = false;
    [_arView setRadarRange:10.0];
    [_arView setScaleViewsBasedOnDistance:YES];
    [_arView setOnlyShowItemsWithinRadarRange:YES];
    [_arView setRadarBackgroundColour:[UIColor colorWithRed:71.0/255.0 green:152.0/255.0 blue:176.0/255.0 alpha:.4]];
    [_arView setRadarViewportColour:[UIColor colorWithRed:71.0/255.0 green:152.0/255.0 blue:176.0/255.0 alpha:.8]];
    [_arView setRadarPointColour:COLOR_BLANCO];
    
    [_containerView3 addSubview:_arView.view];
}

#pragma mark -
#pragma mark - ARLocationDelegate

- (NSMutableArray *)geoLocations
{
    NSMutableArray *locationArray = [[NSMutableArray alloc] init];
    ARGeoCoordinate *arCoordinate;
    
    int numItem = 0;
    
    while ( ( [locationArray count] < 100 ) && ( numItem < [_listadoDatos_CoordenadasAR count] ) )
    {
        NSMutableDictionary *item = [_listadoDatos_CoordenadasAR objectAtIndex:numItem];
        
        arCoordinate = [ARGeoCoordinate coordinateWithLocation:[[CLLocation alloc] initWithLatitude:[[item objectForKey:@"lat"] doubleValue]
                                                                                          longitude:[[item objectForKey:@"lon"] doubleValue]]
                                              locationDistance:[[item objectForKey:@"dist"] doubleValue]
                                                 locationTitle:@""
                                                   locationTag:[[item objectForKey:@"id"] intValue]
                                                     iconImage:[UIImage imageWithData:[item objectForKey:@"image"]]];
        [locationArray addObject:arCoordinate];
        
        numItem++;
    }
    return locationArray;
}

- (void)locationClicked:(ARGeoCoordinate *)coordinate
{
    int idItem = (int)coordinate.tag;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSURL *url = [NSURL URLWithString:URL_SERVIDOR_HTTPS];
    
    AFHTTPRequestOperationManager *operationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [operationManager.securityPolicy setAllowInvalidCertificates:YES];
    [operationManager setRequestSerializer:[AFHTTPRequestSerializer serializer]];
    [operationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:[userDefaults objectForKey:@"usrLogin"] password:[userDefaults objectForKey:@"usrPassword"]];
    
    [operationManager GET:[NSString stringWithFormat:@"empresa/%d", [[[_listadoDatos_Empresas objectAtIndex:idItem] objectForKey:@"id_empresa"] intValue]]
               parameters:nil
                  success:^(AFHTTPRequestOperation *operation, id responseObject){
                      NSMutableDictionary *receivedData = [[NSJSONSerialization JSONObjectWithData:[[operation responseString] dataUsingEncoding:NSUTF8StringEncoding] options:0 error:nil] mutableCopy];
                      
                      UIStoryboard *storyboard = [UIApplication sharedApplication].delegate.window.rootViewController.storyboard;
                      
                      Empresas_DetalleVC *pantallaDestino = [storyboard instantiateViewControllerWithIdentifier:@"Empresas_Detalle"];
                      [pantallaDestino setItemEmpresa:receivedData];
                      [pantallaDestino setItemImagen:[_listadoDatos_Empresas_Imagenes objectAtIndex:idItem]];
                      [_originView.navigationController pushViewController:pantallaDestino animated:NO];
                      
                  }
                  failure:^(AFHTTPRequestOperation *operation, NSError *error){
                      NSLog(@"%@",[error description]);
                      [_activityIndicator stopAnimating];
                  }];
}

@end